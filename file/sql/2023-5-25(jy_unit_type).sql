-- --------------------------------------------------------
-- 主机:                           47.122.18.241
-- 服务器版本:                        5.7.40-log - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.2.0.6576
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 tanlu_db.jy_unit_type 结构
CREATE TABLE IF NOT EXISTS `jy_unit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `base_unit` varchar(255) DEFAULT NULL COMMENT '基准单位名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人id',
  `modify_by` int(11) DEFAULT NULL COMMENT '修改人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='单位类型表';

-- 正在导出表  tanlu_db.jy_unit_type 的数据：~11 rows (大约)
INSERT INTO `jy_unit_type` (`id`, `unit_name`, `base_unit`, `sort`, `state`, `create_by`, `modify_by`, `create_time`, `modify_time`) VALUES
	(1, '质量单位', 't', 1, 1, NULL, NULL, NULL, NULL),
	(2, '长度单位', 'km', 2, 1, NULL, NULL, NULL, NULL),
	(3, '体积单位', 'L', 3, 1, NULL, NULL, NULL, NULL),
	(4, '能量单位', 'kWh', 4, 1, NULL, NULL, NULL, NULL),
	(5, '面积单位\r\n\r\n\r\n\r\n\r\n\r\n', 'km²', 5, 1, NULL, NULL, NULL, NULL),
	(6, '计数单位\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', '1', 6, 1, NULL, NULL, NULL, NULL),
	(7, '时间单位\r\n\r\n\r\n\r\n\r\n', '小时', 7, 1, NULL, NULL, NULL, NULL),
	(8, '货币单位\r\n\r\n\r\n\r\n\r\n\r\n\r\n', '元', 8, 1, NULL, NULL, NULL, NULL),
	(9, '复合单位\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', '1', 9, 1, NULL, NULL, NULL, NULL),
	(10, '存储单位\r\n', 'GB', 10, 1, NULL, NULL, NULL, NULL),
	(11, '功率单位\r\n\r\n', 'W', 11, 1, NULL, NULL, NULL, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
