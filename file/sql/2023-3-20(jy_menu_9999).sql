-- --------------------------------------------------------
-- 主机:                           47.122.18.241
-- 服务器版本:                        5.7.40-log - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.2.0.6576
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 tanlu_db.jy_menu 结构
CREATE TABLE IF NOT EXISTS `jy_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `component` varchar(100) DEFAULT NULL,
  `redirect` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `alwaysShow` tinyint(1) DEFAULT NULL,
  `noCache` tinyint(1) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `admin_user_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `hidden` int(1) DEFAULT NULL COMMENT '菜单是否显示',
  `menu_data_group_id` varchar(255) DEFAULT NULL COMMENT '控制按纽分组',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='菜单表';

-- 正在导出表  tanlu_db.jy_menu 的数据：~75 rows (大约)
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES
	(1, '首页', '/dashboard', '#', '/dashboard/analysis', 'Dashboard', 'ant-design:dashboard-filled', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, '工作台', 'analysis', 'views/Dashboard/Analysis', NULL, 'Analysis', NULL, 0, NULL, 1, NULL, NULL, NULL, '4|13', NULL, NULL),
	(4, '系统设置', '/syset', '#', '/syset/404', 'Syset', 'codicon:settings-gear', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, '用户管理', 'user', 'views/syset/user', NULL, 'User', NULL, 0, NULL, 4, NULL, NULL, NULL, '1|5|6|7|8', NULL, NULL),
	(6, '角色管理', 'role', 'views/syset/role', NULL, 'Role', NULL, 0, NULL, 4, NULL, NULL, NULL, '1|2|3|4', NULL, NULL),
	(7, '组织管理', 'organization', 'views/syset/organization', NULL, 'Organization', NULL, 0, NULL, 4, NULL, NULL, NULL, '1|2|3|4', NULL, NULL),
	(8, '产品碳足迹', '/product', '#', '/product/management', 'Product', 'game-icons:footprint', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, '产品管理', 'management', 'views/product/management', NULL, 'Management', NULL, 0, NULL, 8, NULL, NULL, NULL, '1|2|3|4|5|6|11|12', NULL, NULL),
	(10, '产品核算', 'businessaccoun', 'views/product/businessaccoun', NULL, 'Businessaccoun', NULL, 0, NULL, 8, NULL, NULL, NULL, '4', NULL, NULL),
	(11, '核算报告', 'report', 'views/product/report', NULL, 'Report', NULL, 0, NULL, 8, NULL, NULL, NULL, '4', NULL, NULL),
	(12, '数据审批', 'approval', 'views/product/approval', NULL, 'Approval', NULL, 0, NULL, 8, NULL, NULL, NULL, '4', NULL, NULL),
	(13, '碳标签管理', 'labelmanage', 'views/product/labelmanage', NULL, 'Labelmanage', NULL, 0, NULL, 8, NULL, NULL, NULL, '4', NULL, NULL),
	(14, '审批设置', 'sysetapproval', 'views/syset/sysetapproval', NULL, 'SysetApproval', NULL, 0, NULL, 4, NULL, NULL, NULL, '3', NULL, NULL),
	(15, '操作日志', 'log', 'views/syset/log', NULL, 'Log', NULL, 0, NULL, 4, NULL, NULL, NULL, '9', NULL, NULL),
	(16, '下载管理', 'downs', 'views/syset/downs', NULL, 'Downs', NULL, 0, NULL, 4, NULL, NULL, NULL, '10', NULL, NULL),
	(17, '供应链碳管理', '/supplier', '#', '/supplier/purchase/suppliermanagement', 'Supplier', 'iconoir:ease-curve-control-points', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, '我是采购者', 'purchase', 'views/supplier/purchase', '/supplier/purchase/suppliermanagement', 'Purchase', NULL, 1, NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, '我是供应商', 'index', 'views/supplier/index', NULL, 'SupplierIndex', NULL, 1, NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, '供应商管理', 'suppliermanagement', 'views/supplier/suppliermanagement', NULL, 'SupplierManagement', NULL, 0, NULL, 18, NULL, NULL, NULL, '4', NULL, NULL),
	(21, '供应商产品管理', 'supplierproduct', 'views/supplier/supplierproduct', NULL, 'SupplierProduct', NULL, 0, NULL, 18, NULL, NULL, NULL, '4', NULL, NULL),
	(22, '报送审批', 'supplierapproval', 'views/supplier/supplierapproval', NULL, 'SupplierApproval', NULL, 0, NULL, 18, NULL, NULL, NULL, '4', NULL, NULL),
	(23, '客户管理', 'customer', 'views/supplier/customer', NULL, 'SupplierCustomer', NULL, 0, NULL, 19, NULL, NULL, NULL, '4', NULL, NULL),
	(24, '供应产品管理', 'supplierproductmanagement', 'views/supplier/supplierproductmanagement', NULL, 'SupplierProductManagement', NULL, 0, NULL, 19, NULL, NULL, NULL, '4', NULL, NULL),
	(25, '组织碳核算', '/calculate', '#', '/calculate', 'Calculate', 'material-symbols:calculate-outline', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, '碳核算管理', 'calculatemanagement', 'views/calculate/calculatemanagement', NULL, 'CalculateManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(27, '数据管理', 'datamanagement', 'views/calculate/datamanagement', NULL, 'DataManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(28, '核算报告', 'calculatereport', 'views/calculate/calculatereport', NULL, 'CalculateReport', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(29, '核算模型管理', 'calculatemodel', 'views/calculate/calculatemodel', NULL, 'CalculateModel', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(30, '排放基准管理', 'emissionbasis', 'views/calculate/emissionbasis', NULL, 'EmissionBasis', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(31, '减排场景', 'scene', 'views/calculate/scene', NULL, 'Scene', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(32, '数据质量管理', 'dataquality', 'views/calculate/dataquality', NULL, 'DataQuality', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(33, '设备管理', 'devicemanagement', 'views/calculate/devicemanagement', NULL, 'DeviceManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(34, '审批管理', 'approvalmanagement', 'views/calculate/approvalmanagement', NULL, 'ApprovalManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(35, '碳因子库', '/factor', '#', '/factor', 'Factor', 'carbon:data-check', 1, NULL, 0, NULL, NULL, NULL, '4', NULL, NULL),
	(36, '因子管理', 'factormanagement', 'views/factor/factormanagement', NULL, 'FactorManagement', NULL, 0, NULL, 35, NULL, NULL, NULL, '4', NULL, NULL),
	(37, '因子导入', 'factorimport', 'views/factor/factorimport', NULL, 'FactorImport', NULL, 0, NULL, 35, NULL, NULL, 1, '4', NULL, NULL),
	(38, '碳数据', '/carbondata', '#', '/carbondata', 'CarbonData', 'carbon:factor', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, '组织排放分析', 'organizationemissions', 'views/carbondata/organizationemissions', NULL, 'OrganizationEmissions', NULL, 0, NULL, 38, NULL, NULL, NULL, '4', NULL, NULL),
	(40, '产品排放分析', 'productanalysis', 'views/carbondata/productanalysis', NULL, 'ProductAnalysis', NULL, 0, NULL, 38, NULL, NULL, NULL, '4', NULL, NULL),
	(41, '碳资产', '/carbonassets', '#', '/carbonassets', 'CarbonAssets', 'grommet-icons:strike-through', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, '碳资产概览', 'analysisee', 'views/carbonassets/analysisee', NULL, 'AnalysiSee', NULL, 0, NULL, 41, NULL, NULL, NULL, '4', NULL, NULL),
	(43, '碳价变化', 'carbonprice', 'views/carbonassets/carbonprice', NULL, 'Carbonprice', NULL, 0, NULL, 41, NULL, NULL, NULL, '4', NULL, NULL),
	(44, '交易明细', 'transactiondetails', 'views/carbonassets/transactiondetails', NULL, 'TransactionDetails', NULL, 0, NULL, 41, NULL, NULL, NULL, '4', NULL, NULL),
	(45, '降碳任务', '/carbontask', '#', '/carbontask', 'CarbonTask', 'grommet-icons:task', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, '新增降碳任务', 'task', 'views/carbontask/task', NULL, 'Task', NULL, 0, NULL, 45, NULL, NULL, 1, '4', NULL, NULL),
	(47, '数据维护', 'datamaintenance', 'views/carbontask/datamaintenance', NULL, 'DataMaintenance', NULL, 0, NULL, 45, NULL, NULL, NULL, '4', NULL, NULL),
	(48, '认证服务', '/certservices', '#', '/certservices', 'Certservices', 'simple-line-icons:globe', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, '发起核查申请', 'apply', 'views/certservices/apply', NULL, 'Apply', NULL, 0, NULL, 48, NULL, NULL, NULL, '4', NULL, NULL),
	(50, '补充材料', 'materials', 'views/certservices/materials', NULL, 'Materials', NULL, 0, NULL, 48, NULL, NULL, NULL, '4', NULL, NULL),
	(51, '排放源管理', 'emissionmanagement', 'views/calculate/emissionmanagement', NULL, 'EmissionManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(52, '下载管理', 'downsmanagement', 'views/calculate/downsmanagement', NULL, 'DownsManagement', NULL, 0, NULL, 25, NULL, NULL, NULL, '4', NULL, NULL),
	(53, '数据管理', 'datamanagement', 'views/product/datamanagement', NULL, 'ProductDataManagement', NULL, 0, NULL, 8, NULL, NULL, 1, '4', NULL, NULL),
	(54, '生成报告', 'createreport', 'views/product/createreport', NULL, 'CreateReport', NULL, 0, NULL, 8, NULL, NULL, 1, '4', NULL, NULL),
	(55, '碳标签', 'carbonlabel', 'views/carbonlabel', NULL, 'CarbonLabel', NULL, 0, NULL, NULL, NULL, NULL, 1, '4', NULL, NULL),
	(56, '数据字典', '/dictionary', '#', '/dictionary', 'Dictionary', 'ep:menu', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, '单位管理', 'unitmanagement', 'views/dictionary/unitmanagement', NULL, 'UnitManagement', NULL, 0, NULL, 56, NULL, NULL, NULL, '1|2|3|4|9|10|11', NULL, NULL),
	(58, '字典管理', 'dictionarymanagement', 'views/dictionary/dictionarymanagement', NULL, 'DictionaryManagement', NULL, 0, NULL, 56, NULL, NULL, NULL, '1|2|3|4|9|10|11', NULL, NULL),
	(59, '消息', '/notices', '#', '/notices', 'Notices', 'fe:notice-push', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, '消息管理', 'noticesmanagement', 'views/notices/noticesmanagement', NULL, 'NoticesManagement', 'fe:notice-push', 0, NULL, 59, NULL, NULL, NULL, '1|2|3|4', NULL, NULL),
	(61, '消息详情', 'noticesdetail', 'views/notices/noticesdetail', NULL, 'NoticesDetail', '', 0, NULL, 59, NULL, NULL, 1, '4', NULL, NULL),
	(62, '消息编辑', 'noticesedit', 'views/notices/noticesedit', NULL, 'NoticesEdit', '', 0, NULL, 59, NULL, NULL, 1, '4', NULL, NULL),
	(63, '更多资讯', 'moreinformation', 'views/Dashboard/moreinformation', NULL, 'Moreinformation', NULL, 0, NULL, 1, NULL, NULL, 1, '4', NULL, NULL),
	(64, '资讯详情', 'informationdetail', 'views/Dashboard/informationdetail', NULL, 'InformationDetail', NULL, 0, NULL, 1, NULL, NULL, 1, '4', NULL, NULL),
	(65, '历史通知', 'historynotice', 'views/Dashboard/historynotice', NULL, 'HistoryNotice', NULL, 0, NULL, 1, NULL, NULL, 1, '4', NULL, NULL),
	(66, '通知详情', 'noticedetail', 'views/Dashboard/noticedetail', NULL, 'NoticeDetail', NULL, 0, NULL, 1, NULL, NULL, 1, '4', NULL, NULL),
	(67, '货品报送', 'supplierproductreport', 'views/supplier/supplierproductreport', NULL, 'SupplierProductReport', NULL, 0, NULL, 19, NULL, NULL, NULL, '4', NULL, NULL),
	(68, '编辑供应产品', 'editsupplierproduct', 'views/supplier/editsupplierproduct', NULL, 'EditSupplierProduct', NULL, 0, NULL, 17, NULL, NULL, 1, '4', NULL, NULL),
	(69, '新增供应产品', 'addsupplierproduct', 'views/supplier/addsupplierproduct', NULL, 'AddSupplierProduct', NULL, 0, NULL, 17, NULL, NULL, 1, '4', NULL, NULL),
	(70, '消息新增', 'noticesadd', 'views/notices/noticesadd', NULL, 'NoticesAdd', NULL, 0, NULL, 59, NULL, NULL, 1, '4', NULL, NULL),
	(71, '客户报送', 'customerproductreport', 'views/supplier/customerproductreport', NULL, 'CustomerProductReport', NULL, 0, NULL, 17, NULL, NULL, 1, '4', NULL, NULL),
	(72, '进行报送', 'productreport', 'views/supplier/productreport', NULL, 'ProductReport', NULL, 0, NULL, 17, NULL, NULL, 1, '4', NULL, NULL),
	(73, '产品详情页', 'productdetail', 'views/carbondata/productdetail', NULL, 'ProductDetail', NULL, 0, NULL, 38, NULL, NULL, 1, '4', NULL, NULL),
	(73, '审批编辑', 'sysetapprovaledit', 'views/syset/sysetapprovaledit', NULL, 'SysetApprovalEdit', '', 0, NULL, 4, NULL, NULL, 1, '4', NULL, NULL),
	(74, '产品对照结果图表', 'productchart', 'views/carbondata/productchart', NULL, 'ProductChart', NULL, 0, NULL, 38, NULL, NULL, 1, '4', NULL, NULL),
	(75, '因子操作', 'factoradd', 'views/factor/factoradd', NULL, 'FactorAdd', NULL, 0, NULL, 35, NULL, NULL, 1, NULL, NULL, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
