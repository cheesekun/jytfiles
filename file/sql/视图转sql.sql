SELECT `experiment_sheet`.`id` AS `id`,`experiment_sheet`.`entrust_sheet_id` AS `entrust_sheet_id`,`customer`.`customer_name` AS `customer_name`,`experiment_sheet`.`experiment_no` AS `experiment_no`,`experiment_sheet`.`project_name` AS `project_name`,`experiment_sheet`.`experiment_name` AS `experiment_name`,`experiment_sheet`.`standard` AS `standard`,`experiment_sheet`.`is_epiboly` AS `is_epiboly`,`experiment_sheet`.`factory` AS `factory`,`experiment_sheet`.`status` AS `status`,`experiment_sheet`.`price` AS `price`,`experiment_sheet`.`flow_handler` AS `flow_handler`,`experiment_sheet`.`schedule_start_time` AS `schedule_start_time`,`experiment_sheet`.`schedule_end_time` AS `schedule_end_time`,`experiment_sheet`.`start_time` AS `start_time`,`experiment_sheet`.`end_time` AS `end_time`,`experiment_sheet`.`remark` AS `remark`,`experiment_sheet`.`create_by` AS `create_by`,`experiment_sheet`.`create_time` AS `create_time`,`experiment_sheet`.`update_time` AS `update_time`,`experiment_sheet`.`is_error` AS `is_error`,`experiment_sheet`.`experiment_sample_count` AS `experiment_sample_count`,`experiment_sheet`.`experiment_conclusion_type` AS `experiment_conclusion_type`,`experiment_sheet`.`standard_test_item_id` AS `standard_test_item_id`,`standard_info`.`code` AS `standard_info_code`,`standard_info`.`version` AS `standard_info_version`,`experiment_sheet`.`planned_finish_date` AS `planned_finish_date`,`experiment_sheet_sample`.`sample_id` AS `sample_id`,`experiment_sheet_sample`.`sample_name` AS `sample_name`,`experiment_sheet_sample`.`gain_count` AS `gain_count`,`experiment_sheet_sample`.`receive_count` AS `receive_count`,`entrust_sheet_addition`.`test_finish_time` AS `test_finish_time`,`experiment_sheet`.`laboratory_id` AS `laboratory_id`,if(ISNULL(`experiment_sheet`.`laboratory_name`),`system_auth_department`.`depart_title`,`experiment_sheet`.`laboratory_name`) AS `laboratory_name`,`experiment_sheet`.`experiment_engineer_id` AS `experiment_engineer_id`,`experiment_sheet`.`experiment_engineer_name` AS `experiment_engineer_name`,`experiment_sheet`.`schedule_manager_id` AS `schedule_manager_id`,`experiment_sheet`.`schedule_manager_name` AS `schedule_manager_name`,`experiment_sheet`.`is_result_record` AS `is_result_record`,`entrust_sheet`.`entrust_no` AS `entrust_no`,`experiment_sheet`.`report_group` AS `report_group`,`entrust_sheet_addition`.`sales_assistant_id` AS `sales_assistant_id`,`entrust_sheet_addition`.`salesman_id` AS `salesman_id`,`experiment_sheet_addition`.`subcontract_report_file_id` AS `subcontract_report_file_id`,`experiment_sheet_addition`.`wu_ling_task_id` AS `wu_ling_task_id`,`experiment_sheet_addition`.`quick_pass_type` AS `quick_pass_type`,`experiment_sheet_addition`.`report_no` AS `report_no`
FROM (((((((((`experiment_sheet`
LEFT JOIN `system_auth_department` ON((`system_auth_department`.`id` = `experiment_sheet`.`laboratory_id`)))
LEFT JOIN `standard_test_item` ON((`standard_test_item`.`id` = `experiment_sheet`.`standard_test_item_id`)))
LEFT JOIN `standard_info` ON((`standard_info`.`id` = `standard_test_item`.`standard_id`)))
LEFT JOIN `experiment_sheet_addition` ON((`experiment_sheet`.`id` = CONVERT(`experiment_sheet_addition`.`experiment_sheet_id` USING utf8mb4))))
LEFT JOIN `experiment_sheet_sample` ON((`experiment_sheet`.`id` = `experiment_sheet_sample`.`experiment_sheet_id`)))
LEFT JOIN `experiment_sheet_sample_addition` ON((`experiment_sheet_sample`.`id` = CONVERT(`experiment_sheet_sample_addition`.`experiment_sheet_sample_id` USING utf8mb4))))
LEFT JOIN `entrust_sheet` ON((`entrust_sheet`.`id` = `experiment_sheet`.`entrust_sheet_id`)))
LEFT JOIN `customer` ON((`customer`.`id` = `entrust_sheet`.`customer_id`)))
LEFT JOIN `entrust_sheet_addition` ON((`entrust_sheet`.`id` = `entrust_sheet_addition`.`sheet_id`)))
WHERE (((`entrust_sheet`.`status` >= 5) OR (`experiment_sheet`.`status` > 4) OR ((`entrust_sheet`.`status` < 5) AND (`experiment_sheet`.`status` >= 4))) AND ISNULL(`experiment_sheet`.`delete_time`) AND (`experiment_sheet_addition`.`quick_pass_type` = 1))
GROUP BY `experiment_sheet`.`id`
ORDER BY `experiment_sheet`.`update_time` DESC

SELECT `experiment_sheet`.`id` AS `id`
,`experiment_sheet`.`entrust_sheet_id` AS `entrust_sheet_id`
,`experiment_sheet`.`experiment_no` AS `experiment_no`
,`experiment_sheet`.`project_name` AS `project_name`
,`experiment_sheet`.`experiment_name` AS `experiment_name`
,`experiment_sheet`.`standard` AS `standard`
,`experiment_sheet`.`is_epiboly` AS `is_epiboly`
,`experiment_sheet`.`factory` AS `factory`
,`experiment_sheet`.`status` AS `status`
,`experiment_sheet`.`price` AS `price`
,`experiment_sheet`.`flow_handler` AS `flow_handler`
,`experiment_sheet`.`schedule_start_time` AS `schedule_start_time`
,`experiment_sheet`.`schedule_end_time` AS `schedule_end_time`
,`experiment_sheet`.`start_time` AS `start_time`
,`experiment_sheet`.`end_time` AS `end_time`
,`experiment_sheet`.`remark` AS `remark`
,`experiment_sheet`.`create_by` AS `create_by`
,`experiment_sheet`.`create_time` AS `create_time`
,`experiment_sheet`.`update_time` AS `update_time`
,`experiment_sheet`.`is_error` AS `is_error`
,`experiment_sheet`.`experiment_sample_count` AS `experiment_sample_count`
,`experiment_sheet`.`experiment_conclusion_type` AS `experiment_conclusion_type`
,`experiment_sheet`.`standard_test_item_id` AS `standard_test_item_id`
,`experiment_sheet`.`planned_finish_date` AS `planned_finish_date`
,`experiment_sheet`.`laboratory_id` AS `laboratory_id`
,`experiment_sheet`.`experiment_engineer_id` AS `experiment_engineer_id`
,`experiment_sheet`.`experiment_engineer_name` AS `experiment_engineer_name`
,`experiment_sheet`.`schedule_manager_id` AS `schedule_manager_id`
,`experiment_sheet`.`schedule_manager_name` AS `schedule_manager_name`
,`experiment_sheet`.`is_result_record` AS `is_result_record`
,`experiment_sheet`.`report_group` AS `report_group`
,if(ISNULL(`experiment_sheet`.`laboratory_name`) ,`system_auth_department`.`depart_title` ,`experiment_sheet`.`laboratory_name`) AS `laboratory_name`
,`customer`.`customer_name` AS `customer_name`
,`entrust_sheet`.`entrust_no` AS `entrust_no`
,`standard_info`.`code` AS `standard_info_code`
,`standard_info`.`version` AS `standard_info_version`
,`experiment_sheet_sample`.`sample_id` AS `sample_id`
,`experiment_sheet_sample`.`sample_name` AS `sample_name`
,`experiment_sheet_sample`.`gain_count` AS `gain_count`
,`experiment_sheet_sample`.`receive_count` AS `receive_count`
,`entrust_sheet_addition`.`test_finish_time` AS `test_finish_time`
,`entrust_sheet_addition`.`sales_assistant_id` AS `sales_assistant_id`
,`entrust_sheet_addition`.`salesman_id` AS `salesman_id`
,`experiment_sheet_addition`.`subcontract_report_file_id` AS `subcontract_report_file_id`
,`experiment_sheet_addition`.`wu_ling_task_id` AS `wu_ling_task_id`
,`experiment_sheet_addition`.`quick_pass_type` AS `quick_pass_type`
,`experiment_sheet_addition`.`report_no` AS `report_no`
FROM (((((((((`experiment_sheet`
LEFT JOIN `system_auth_department` ON((`system_auth_department`.`id` = `experiment_sheet`.`laboratory_id`)))
LEFT JOIN `standard_test_item` ON((`standard_test_item`.`id` = `experiment_sheet`.`standard_test_item_id`)))
LEFT JOIN `standard_info` ON((`standard_info`.`id` = `standard_test_item`.`standard_id`)))
LEFT JOIN `experiment_sheet_addition` ON((`experiment_sheet`.`id` = `experiment_sheet_addition`.`experiment_sheet_id`)))
LEFT JOIN `experiment_sheet_sample` ON((`experiment_sheet`.`id` = `experiment_sheet_sample`.`experiment_sheet_id`)))
LEFT JOIN `experiment_sheet_sample_addition` ON((`experiment_sheet_sample`.`id` = `experiment_sheet_sample_addition`.`experiment_sheet_sample_id`)))
LEFT JOIN `entrust_sheet` ON((`entrust_sheet`.`id` = `experiment_sheet`.`entrust_sheet_id`)))
LEFT JOIN `customer` ON((`customer`.`id` = `entrust_sheet`.`customer_id`)))
LEFT JOIN `entrust_sheet_addition` ON((`entrust_sheet`.`id` = `entrust_sheet_addition`.`sheet_id`)))
WHERE (((`entrust_sheet`.`status` >= 5) OR (`experiment_sheet`.`status` > 4) OR ((`entrust_sheet`.`status` < 5) AND (`experiment_sheet`.`status` >= 4))) 
AND ISNULL(`experiment_sheet`.`delete_time`) 
AND (`experiment_sheet_addition`.`quick_pass_type` = 1))
GROUP BY `experiment_sheet`.`id`
ORDER BY `experiment_sheet`.`update_time` DESC


        $where1 = [['ens.status','>=', 5]];
        $where2 = [['es.status','>', 4]];
        // $where3 = [['ens.status','<', 5], ['es.status','>=', 4]];
        $where3 = [['ens.status','<', 5]];
        $where5 = [['es.status','>=', 4]];
        $where4 = 
                [
                'es.delete_time'=>null,
                'esa.quick_pass_type'=>1
                ];
        $list = Db::table('experiment_sheet es')
            ->field('
                es.id,
                es.entrust_sheet_id,
                es.experiment_no,
                es.project_name,
                es.experiment_name,
                es.standard,
                es.is_epiboly,
                es.factory,
                es.status,
                es.price,
                es.flow_handler,
                es.schedule_start_time,
                es.schedule_end_time,
                es.start_time,
                es.end_time,
                es.remark,
                es.create_by,
                es.create_time,
                es.update_time,
                es.is_error,
                es.experiment_sample_count,
                es.experiment_conclusion_type,
                es.standard_test_item_id,
                es.planned_finish_date,
                es.laboratory_id,
                es.experiment_engineer_id,
                es.experiment_engineer_name,
                es.schedule_manager_id,
                es.schedule_manager_name,
                es.is_result_record,
                es.report_group,
                c.customer_name,
                ens.entrust_no,
                si.code standard_info_code,
                si.version standard_info_version,
                ess.sample_id,
                ess.sample_name,
                ess.gain_count,
                ess.receive_count,
                ensa.test_finish_time,
                ensa.sales_assistant_id,
                ensa.salesman_id,
                esa.subcontract_report_file_id,
                esa.wu_ling_task_id,
                esa.quick_pass_type,
                esa.report_no
            ')
            ->fieldRaw('IF(ISNULL(es.laboratory_name), sad.depart_title, es.laboratory_name) laboratory_name')
            ->where($where)
            // ->where(
            //     [
            //     'es.delete_time'=>null,
            //     'esa.quick_pass_type'=>1
            //     ]
            // )
            // ->whereOr([$where1,$where2,$where3])
            
            ->where(function($query) use ($where1,$where2,$where3,$where4,$where5){$query->where($where4)->where(function($query) use ($where1,$where2,$where3,$where5){$query->where($where5)->whereOr([$where1,$where2,$where3]);});})
            ->leftJoin('system_auth_department sad', 'sad.id = es.laboratory_id')
            ->leftJoin('standard_test_item sti', 'sti.id = es.standard_test_item_id')
            ->leftJoin('standard_info si', 'si.id = sti.standard_id')
            ->leftJoin('experiment_sheet_addition esa', 'es.id = esa.experiment_sheet_id')
            ->leftJoin('experiment_sheet_sample ess', 'es.id = ess.experiment_sheet_id')
            ->leftJoin('experiment_sheet_sample_addition essa', 'ess.id = essa.experiment_sheet_sample_id')
            ->leftJoin('entrust_sheet ens', 'ens.id = es.entrust_sheet_id')
            ->leftJoin('customer c', 'c.id = ens.customer_id')
            ->leftJoin('entrust_sheet_addition ensa', 'es.id = ensa.sheet_id')
            ->order(['es.update_time'=>'desc'])
            ->paginate(['list_rows'=>$pageSize, 'page'=>$page])
            ->toArray();