/*
 Navicat Premium Data Transfer

 Source Server         : tanlu_db
 Source Server Type    : MySQL
 Source Server Version : 50739
 Source Host           : 192.168.0.92:3306
 Source Schema         : tanlu_db

 Target Server Type    : MySQL
 Target Server Version : 50739
 File Encoding         : 65001

 Date: 24/11/2022 11:24:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jy_product
-- ----------------------------
DROP TABLE IF EXISTS `jy_product`;
CREATE TABLE `jy_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `product_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品编号',
  `product_spec` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '产品规格',
  `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `state` tinyint(1) NULL DEFAULT 1 COMMENT '\r\n状态\r\n\r\n',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '更新人',
  `addtime` datetime NULL DEFAULT NULL,
  `updatetime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '产品列表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of jy_product
-- ----------------------------
INSERT INTO `jy_product` VALUES (24, '11111', '123456', '6666', 'www.baidu.com', 2, 1, '2022-11-15 11:50:21', '2022-11-23 16:29:41');
INSERT INTO `jy_product` VALUES (25, '实例产品A', 'SLCPA', '初代', 'www.baidu.com', 1, 1, '2022-11-15 12:54:01', NULL);
INSERT INTO `jy_product` VALUES (26, '实例产品C', 'SLCPA', '初代', 'www.baidu.com', 2, 1, '2022-11-15 14:51:26', '2022-11-22 17:57:59');
INSERT INTO `jy_product` VALUES (27, '实例产品C', 'SLCPA', '初代', 'www.baidu.com', 1, 1, '2022-11-15 15:07:58', NULL);
INSERT INTO `jy_product` VALUES (28, '实例产品C', 'SLCPA', '初代', 'www.baidu.com', 2, 1, '2022-11-15 15:07:59', '2022-11-22 17:57:56');
INSERT INTO `jy_product` VALUES (56, 'test', '0232465456', '51231268796', 'www.baidu.1522.com', 1, 1, '2022-11-19 16:25:48', NULL);
INSERT INTO `jy_product` VALUES (57, '11111', '123456', '6666', 'baidu.com#wangyi.com', 2, 1, '2022-11-21 18:29:39', NULL);
INSERT INTO `jy_product` VALUES (58, 'BBB', '123456', '6666', 'file://D:\\Users\\admin\\Desktop\\示例产品.doc', 2, 1, '2022-11-21 10:45:43', '2022-11-23 17:31:43');
INSERT INTO `jy_product` VALUES (59, '11111', '123456', '6666', 'file://C:\\Users\\admin\\Desktop\\示例产品.doc', 1, 1, '2022-11-21 10:49:16', '2022-11-24 09:23:50');
INSERT INTO `jy_product` VALUES (60, '11111', '123456', '6666', 'file://C:\\Users\\admin\\Desktop\\示例产品.doc', 2, 1, '2022-11-21 10:52:19', '2022-11-22 16:33:31');
INSERT INTO `jy_product` VALUES (61, '11111', '123456', '6666', 'file://C:\\Users\\admin\\Desktop\\示例产品.doc', 1, 1, '2022-11-21 10:55:21', NULL);
INSERT INTO `jy_product` VALUES (62, '11111', '123456', '6666', 'products/20221121\\c92abf9e489de3c07a09679e11bae741.png', 2, 1, '2022-11-21 16:42:42', '2022-11-22 17:58:00');
INSERT INTO `jy_product` VALUES (63, '11111', '123456', '6666', 'products/20221121\\a82fed9e62bac27b0cddaa671523c3dd.png', 2, 1, '2022-11-21 16:45:54', '2022-11-22 17:57:51');
INSERT INTO `jy_product` VALUES (64, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\f41f6f501e8efd68acce8552ca5bab87.png', 1, 1, '2022-11-21 16:46:50', NULL);
INSERT INTO `jy_product` VALUES (65, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\1a4e224bc157fc8fe39dcd67c386bc50.png', 2, 1, '2022-11-21 17:00:22', '2022-11-22 17:57:47');
INSERT INTO `jy_product` VALUES (66, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\26ba46331f0bed5a32f9158bea1ca216.png', 1, 1, '2022-11-21 17:01:04', NULL);
INSERT INTO `jy_product` VALUES (67, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\4cb890d527431c8f781e6daab1edbf87.png', 2, 1, '2022-11-21 17:01:36', '2022-11-22 17:57:46');
INSERT INTO `jy_product` VALUES (68, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\9f19e3327a5efbb8a63ae9fcc3aedd7c.png', 1, 1, '2022-11-21 17:02:56', NULL);
INSERT INTO `jy_product` VALUES (69, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\98ff7737ec89188e0b450ff22e10ed48.png', 2, 1, '2022-11-21 17:03:11', '2022-11-22 17:57:44');
INSERT INTO `jy_product` VALUES (70, '实例产品XXX', 'SLCPA', '二代', 'products/20221121\\5203b390ce53e58d4a9b29cf4bf374de.png', 1, 1, '2022-11-21 17:03:24', NULL);
INSERT INTO `jy_product` VALUES (116, '221221qwqwq', '121', '2121', NULL, 1, 1, '2022-11-22 15:14:18', NULL);
INSERT INTO `jy_product` VALUES (117, '32133', '3232', '323323', NULL, 2, 1, '2022-11-22 15:15:22', '2022-11-22 16:39:06');
INSERT INTO `jy_product` VALUES (118, '123456', '121', '2121', NULL, 1, 1, '2022-11-22 15:15:35', '2022-11-22 16:39:08');
INSERT INTO `jy_product` VALUES (119, '21123313', '21', '21', NULL, 2, 1, '2022-11-22 15:16:12', '2022-11-22 16:25:57');
INSERT INTO `jy_product` VALUES (120, '221221qwqwq', '121', '2121', NULL, 1, 1, '2022-11-22 15:16:29', '2022-11-22 17:57:38');
INSERT INTO `jy_product` VALUES (121, '221221qwqwq', '121', '2121', NULL, 2, 1, '2022-11-22 15:41:08', '2022-11-22 16:25:46');
INSERT INTO `jy_product` VALUES (122, '221221qwqwq', '121', '2121', NULL, 2, 1, '2022-11-22 15:41:23', '2022-11-23 14:44:39');
INSERT INTO `jy_product` VALUES (123, '221221qwqwq', '121', '2121', NULL, 1, 1, '2022-11-22 15:42:18', '2022-11-23 14:37:32');
INSERT INTO `jy_product` VALUES (124, '1212sasas', '2121', '2121', NULL, 1, 1, '2022-11-22 15:45:46', '2022-11-22 17:57:33');
INSERT INTO `jy_product` VALUES (125, 'zhanglaoer', '2121', '21211321231', NULL, 1, 1, '2022-11-22 15:50:12', '2022-11-23 13:52:13');
INSERT INTO `jy_product` VALUES (126, 'qwqw', 'wqwq', 'wqwq', NULL, 1, 1, '2022-11-22 16:49:35', '2022-11-22 17:57:31');
INSERT INTO `jy_product` VALUES (127, 'qwqw', '2121', '2121', NULL, 1, 1, '2022-11-22 16:49:51', '2022-11-23 17:19:14');
INSERT INTO `jy_product` VALUES (128, '12131', '13313', '3131', NULL, 1, 1, '2022-11-22 18:06:34', NULL);
INSERT INTO `jy_product` VALUES (129, '13213', '3213213', '3131', NULL, 1, 1, '2022-11-22 18:06:58', NULL);
INSERT INTO `jy_product` VALUES (130, '1212', '2121', '2121', NULL, 1, 1, '2022-11-22 18:12:41', NULL);
INSERT INTO `jy_product` VALUES (131, '24234r', 'wqwwq', 'wqwq', NULL, 1, 1, '2022-11-22 18:13:16', '2022-11-24 09:23:41');
INSERT INTO `jy_product` VALUES (132, '实例产品XXX', 'SLCPA', '二代', NULL, 2, 1, '2022-11-23 17:27:00', '2022-11-24 09:24:00');
INSERT INTO `jy_product` VALUES (133, '21212', '2121', '2121', NULL, 1, 1, '2022-11-24 09:32:05', NULL);
INSERT INTO `jy_product` VALUES (134, '2121', '2121', '2121', NULL, 2, 1, '2022-11-24 09:32:48', '2022-11-24 09:37:45');
INSERT INTO `jy_product` VALUES (135, '2121', '2121', '2121', NULL, 1, 1, '2022-11-24 09:37:39', NULL);
INSERT INTO `jy_product` VALUES (136, '21212', '3131', '3131', NULL, 1, 1, '2022-11-24 09:40:38', NULL);
INSERT INTO `jy_product` VALUES (137, '2121', '212', '2121', NULL, 1, 1, '2022-11-24 09:43:45', NULL);
INSERT INTO `jy_product` VALUES (138, '121', '2121', '2121', NULL, 1, 1, '2022-11-24 09:46:59', NULL);
INSERT INTO `jy_product` VALUES (139, '实例产品XXX', 'SLCPA', '二代', '20221124/67l1VQ_1669254262.doc', 1, 1, '2022-11-24 09:47:21', NULL);
INSERT INTO `jy_product` VALUES (140, '1212', '2121', '212121', NULL, 1, 1, '2022-11-24 09:48:09', NULL);
INSERT INTO `jy_product` VALUES (141, '实例产品XXX', 'SLCPA', '二代', NULL, 1, 1, '2022-11-24 09:48:10', NULL);
INSERT INTO `jy_product` VALUES (142, '2121', '2121', '2121', NULL, 1, 1, '2022-11-24 09:48:25', NULL);
INSERT INTO `jy_product` VALUES (143, 'wqwq', '1qeq', 'dgd', NULL, 1, 1, '2022-11-24 09:49:35', NULL);
INSERT INTO `jy_product` VALUES (144, '2121', '212121', '2121', NULL, 1, 1, '2022-11-24 09:50:36', NULL);
INSERT INTO `jy_product` VALUES (145, '999', '999', '999', NULL, 2, 1, '2022-11-24 09:52:37', '2022-11-24 10:10:34');
INSERT INTO `jy_product` VALUES (146, '2121', '2121', '211221', NULL, 2, 1, '2022-11-24 09:54:34', '2022-11-24 10:04:51');
INSERT INTO `jy_product` VALUES (147, '1221', '2121', '2121', NULL, 2, 1, '2022-11-24 10:05:07', '2022-11-24 10:05:44');
INSERT INTO `jy_product` VALUES (148, '121', '2121', '212', NULL, 2, 1, '2022-11-24 10:05:41', '2022-11-24 10:10:31');
INSERT INTO `jy_product` VALUES (149, '1221', '2121', '2121', NULL, 2, 1, '2022-11-24 10:06:09', '2022-11-24 10:10:17');
INSERT INTO `jy_product` VALUES (150, '212', '2121', '2121', NULL, 2, 1, '2022-11-24 10:09:54', '2022-11-24 10:10:25');
INSERT INTO `jy_product` VALUES (151, '121', '2121', '2121', NULL, 1, 1, '2022-11-24 10:10:08', '2022-11-24 10:42:13');

SET FOREIGN_KEY_CHECKS = 1;
