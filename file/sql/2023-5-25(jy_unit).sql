-- --------------------------------------------------------
-- 主机:                           47.122.18.241
-- 服务器版本:                        5.7.40-log - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.2.0.6576
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 tanlu_db.jy_unit 结构
CREATE TABLE IF NOT EXISTS `jy_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `type_id` int(11) DEFAULT NULL COMMENT '类型，1-质量单位，2-数量单位，3-长度单位，4-时间单位，4-货币单位，4-组合单位，4-面积单位',
  `is_base` tinyint(1) DEFAULT NULL COMMENT '是否为基准单位',
  `conversion_ratio` decimal(60,30) DEFAULT NULL COMMENT '换算比例',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人id',
  `modify_by` int(11) DEFAULT NULL COMMENT '修改人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='单位表';

-- 正在导出表  tanlu_db.jy_unit 的数据：~158 rows (大约)
INSERT INTO `jy_unit` (`id`, `name`, `type_id`, `is_base`, `conversion_ratio`, `sort`, `state`, `create_by`, `modify_by`, `create_time`, `modify_time`) VALUES
	(1, 't', 1, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(2, 'kg', 1, 2, 0.001000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(3, 'g', 1, 2, 0.000001000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(4, '加仑（美制）', 1, 2, 0.003785000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(5, '加仑（英制）', 1, 2, 0.004546000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(6, '短吨', 1, 2, 0.907200000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(7, '磅', 1, 2, 0.000453600000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(8, '盎司', 1, 2, 0.000028350000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(9, 'mg', 1, 2, 0.000000001000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(10, 'μg', 1, 2, 0.000000000001000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(11, '长吨', 1, 2, 1.016000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(12, '美吨', 1, 2, 0.907200000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(13, '万吨', 1, 2, 10000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(14, '兆吨', 1, 2, 1000000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(15, '千吨', 1, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(16, 'km', 2, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(17, 'm', 2, 2, 0.001000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(18, 'cm', 2, 2, 0.000010000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(19, 'mm', 2, 2, 0.000001000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(20, '英里', 2, 2, 1.609300000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(21, '英寸', 2, 2, 0.000025400000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(22, '海里', 2, 2, 1.852000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(23, '光年', 2, 2, 9460730472580.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(24, 'dm', 2, 2, 0.000100000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(25, 'L', 3, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(26, 'm³', 3, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(27, 'ml', 3, 2, 0.001000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(28, '立方英尺', 3, 2, 28.316800000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(29, '万Nm³', 3, 2, 0.000000000000000000010000000000, 1, 1, NULL, NULL, NULL, NULL),
	(30, 'kL', 3, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(31, '亿立方米', 3, 2, 100000000000000000000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(32, 'Nm³', 3, 2, 0.000000000000000000000001000000, 1, 1, NULL, NULL, NULL, NULL),
	(33, '公秉', 3, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(34, '千Nm³', 3, 2, 0.000000000000000000001000000000, 1, 1, NULL, NULL, NULL, NULL),
	(35, '千m³', 3, 2, 1000000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(36, '亿Nm³', 3, 2, 0.000000000000000100000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(37, 'kWh', 4, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(38, 'GJ', 4, 2, 277.777800000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(39, 'kJ', 4, 2, 0.000277800000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(40, 'MJ', 4, 2, 0.277800000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(41, 'TJ', 4, 2, 278000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(42, '卡', 4, 2, 0.000001163000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(43, '千卡', 4, 2, 0.001163000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(44, '吨标煤', 4, 2, 6944.400000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(45, 'MWh', 4, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(46, 'J', 4, 2, 0.000000277800000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(47, '标准煤', 4, 2, 6.944400000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(48, '百万英热', 4, 2, 293.071100000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(49, '度', 4, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(50, '百万kWh', 4, 2, 1000000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(51, 'km²', 5, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(52, '公顷', 5, 2, 0.010000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(53, '㎡', 5, 2, 0.000001000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(54, '亩', 5, 2, 0.000666700000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(55, '英亩', 5, 2, 0.004047000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(56, '万公顷', 5, 2, 100.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(57, '千㎡', 5, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(58, '瓶', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(59, '架', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(60, '张', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(61, '人', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(62, '包', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(63, '台', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(64, '个', 6, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(65, '支', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(66, '封', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(67, '本', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(68, '户', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(69, '座', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(70, '件', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(71, '次', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(72, '颗', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(73, '片', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(74, '盒', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(75, '袋', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(76, '双', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(77, '块', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(78, '桶', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(79, '条', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(80, '粒', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(81, '卷', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(82, '罐', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(83, '组', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(84, '箱', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(85, '晚', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(86, '头', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(87, '部', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(88, 'EA', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(89, '枚', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(90, '杯', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(91, '株', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(92, '对', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(93, '重', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(94, '万本', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(95, '综合业务量', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(96, '把', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(97, '辆', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(98, '扇', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(99, '笔', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(100, '份', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(101, '千个', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(102, '千双', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(103, '千本', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(104, '千袋', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(105, '单元', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(106, '套', 6, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(107, '小时', 7, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(108, '日', 7, 2, 24.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(109, '年', 7, 2, 8760.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(110, '月', 7, 2, 729.600000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(111, '分钟', 7, 2, 0.016670000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(112, '秒', 7, 2, 0.000277800000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(113, '元', 8, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(114, '美元', 8, 2, 7.160700000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(115, '日元', 8, 2, 0.051200000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(116, '英镑', 8, 2, 9.037600000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(117, '欧元', 8, 2, 7.726500000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(118, '千日元', 8, 2, 51.200000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(119, '万元', 8, 2, 10000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(120, '百万日元', 8, 2, 51171.835000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(121, '人次', 9, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(122, 'tkm', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(123, 'm²h', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(124, '延吨公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(125, '延人公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(126, '每房每晚', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(127, '重量箱', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(128, '吨公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(129, 'm³蓄积量', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(130, 'kg玻璃液', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(131, 'kgm', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(132, 'kgkm', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(133, '人公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(134, '万人次', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(135, '万人公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(136, '公顷每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(137, '人英里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(138, '百万换算吨公里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(139, '吨农产品', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(140, '吨英里', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(141, '千克奶制品', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(142, '万头每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(143, '千米核', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(144, '每人每小时', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(145, '每平方米每小时', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(146, '每平方米每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(147, 'TB每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(148, '每头每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(149, '每个每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(150, '每平方米每天', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(151, '每人每年', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(152, '人天', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(153, '千克每天', 9, 2, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(154, 'GB', 10, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(155, 'TB', 10, 2, 1024.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(156, 'W', 11, 1, 1.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(157, 'kW', 11, 2, 1000.000000000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL),
	(158, '米制马力', 11, 2, 735.498800000000000000000000000000, 1, 1, NULL, NULL, NULL, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
