-- --------------------------------------------------------
-- 主机:                           47.122.18.241
-- 服务器版本:                        5.7.40-log - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.2.0.6576
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 导出  表 tanlu_db.jy_approval 结构
CREATE TABLE IF NOT EXISTS `jy_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '业务流程',
  `factory` int(11) DEFAULT NULL COMMENT '是否开启工厂审核，1-开启，2-关闭',
  `branch_company` int(11) DEFAULT NULL COMMENT '是否开启分公司审核，1-开启，2-关闭',
  `parent_company` int(11) DEFAULT NULL COMMENT '是否开启母公司审核，1-开启，2-关闭',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人id',
  `modify_by` int(11) DEFAULT NULL COMMENT '修改人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='审批设置表';

-- 正在导出表  tanlu_db.jy_approval 的数据：~3 rows (大约)
INSERT INTO `jy_approval` (`id`, `name`, `factory`, `branch_company`, `parent_company`, `sort`, `create_by`, `modify_by`, `create_time`, `modify_time`) VALUES
	(1, '组织碳核算数据上报', 2, 1, 1, 1, NULL, 14, NULL, '2023-03-09 09:54:44'),
	(2, '产品碳足迹核算', 1, 1, 1, 2, NULL, 14, NULL, '2023-03-07 09:16:19'),
	(3, '供应产品填报', 1, 1, 1, 3, NULL, 14, NULL, '2023-03-07 09:16:27');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
