#if !defined _DPAPI2_H_I
#define _DPAPI2_H_I

//��������
#define DT_VOID		0	//������
#define DT_DIGITAL	1	//������	,Ҳ��4�ֽ�����		
#define DT_INT32	2	//4�ֽ�����
#define DT_FLOAT32	3	//4�ֽڸ�����
#define DT_INT64	4	//8�ֽ�����
#define DT_FLOAT64	5	//8�ֽڸ�����
#define DT_STRING	6	//�ַ���
#define DT_BLOB		7	//���������ݿ�

// ����������������
#define DBP_QUALITY_GOOD					0x00	// GOOD
#define DBP_QUALITY_BAD						0x01	// ��ֵ����û���ض�ԭ��
#define DBP_QUALITY_BAD_CONFIG_ERROR		0x02	// ���������ô������磬��ǩ�����еķ�����������ɾ����
#define DBP_QUALITY_BAD_NOT_CONNECTED		0x03	// ����Ҫ�����ӵ�����Ŀ�꣬���޷��ɹ����ӡ�������˵���ڸ�ʱ����ϵ�ֵ�����ţ���Ϊ��һ������Դδ�ṩ
#define DBP_QUALITY_BAD_DEVICE_FAIL			0x04	// ��⵽�豸����
#define DBP_QUALITY_BAD_SENSOR_FAIL			0x05	// ��⵽����������
#define DBP_QUALITY_BAD_LAST				0x06	// �豸���������ͨѶʧ�ܣ����ǣ������Чֵ�ǿ��õġ���ע�⣬ʱ���ǵ������һ����Чֵ��ʱ�꣬���������������ʱ��
#define DBP_QUALITY_BAD_COMM_FAIL			0x07	// �豸���������ͨѶʧ�ܣ�����û�������õ���Чֵ
#define DBP_QUALITY_BAD_OUT_OF_SERVICE		0x08	// �ñ�ǩ���鱻����Ϊ�ǻ����ʾ�ñ�ǩ����ĳ��ԭ�򲻱�����������

#define DBP_QUALITY_UNCERTAIN				0xE0	// ֵ��Ч�Բ�ȷ������û���ض�ԭ��
#define DBP_QUALITY_UNCERTAIN_LAST			0xE1	// �Ѿ�ֹͣд������ֵ����ֵӦ����Ϊ����ȥֵ������ע�⣬��״̬��ʾΪ�ڹ涨ʱ���ڱ�ǩδ�����¡���Bad/Last Usable Value��ʾΪ��⵽ͨѶ������
#define DBP_QUALITY_UNCERTAIN_SENSOR		0xE2	// ֵ�������������̷�Χ���ߴ������ڲ����Ϊ������׼ȷ
#define DBP_QUALITY_UNCERTAIN_EU_EXCEEDED	0xE3	// ����ֵ�����˸�ֵ����ķ�Χ����ע�⣬��״̬������ζ�Ÿ�ֵ�ǳ��޵�
#define DBP_QUALITY_UNCERTAIN_SUB_NORMAL	0xE4	// ��ֵ���Զ��Դ��������״̬ΪGOOD������Դ�������ڹ涨ֵ

#define DBP_QUALITY_CALC_OFF				0xF0	// ����ֹͣ

#define DBP_QUALITY_LOLO_LIMIT				0xF9	// �͵ͱ�
#define DBP_QUALITY_LO_LIMIT				0xFA	// �ͱ�
#define DBP_QUALITY_HI_LIMIT				0xFB	// �߱�
#define DBP_QUALITY_HIHI_LIMIT				0xFC	// �߸߱�
#define DBP_QUALITY_NOT_INIT				0xFD	// �ű���������δ����ʼ��
#define DBP_QUALITY_SHUTDOWN				0xFE	// �������ر�
#define DBP_QUALITY_UNKOWN					0xFF	// δ֪����

// ����ʷstr,��ʷblob,SOE��ʱ�������
#define  RST_CUR			1		//��ǰʱ�괦
#define  RST_AFTER			2		//��ǰ֮��һ��
#define  RST_DEFAULT		(RST_CUR | RST_AFTER) //��ǰ��֮��һ��

// DBP_GetMultiPointHisValʹ�ã���ʾ���ָ��ʱ����ֵʱ�����ȡֵ
#define RHV_BEFORE			1		// ȡǰһֵ
#define RHV_AFTER			2		// ȡ��һֵ
#define RHV_INTERP			3		// ȡ��ֵ

//Ȩ��
#define DS_POWERMAN		0x00000001 //�˺Ź���
#define DS_SYSMAN		0x00000002 //ϵͳ����,���ݿ��������ǩ����
#define DS_WRITE		0x00000004 //дȨ��
#define DS_READ			0x00000008 //��Ȩ��

//��ǩ����
#define TAG_FLAG_DDC	0 //�ֳ����ݱ�ǩ(ʵʱ���ǩ) ������"ddctag"
#define TAG_FLAG_DEF	1 //�����ǩ(�ֹ���ǩ)       ������"deftag"
#define TAG_FLAG_PRE	2 //Ԥ������ǩ(Ԥ������ǩ)   ������"pretag"
#define TAG_FLAG_CUR	3 //���߱�ǩ                 ������"curtag"	
#define TAG_FLAG_CALC	4 //�����ǩ				 ������"calctag"

/// ��ǩ������������� 
#ifndef MallocTagNameArray
typedef char(* TagNameArray)[80];
#define MallocTagNameArray(n) (TagNameArray)malloc((n) * 80)
#define FreeTagNameArray(p)	if(p) free(p)
#endif

typedef void* DBPH; //���

typedef void* HDBPRPCSTREAM; // RPC�����

typedef struct t_dbparg  //�������������Ӳ���
{
	char	sip[16];    	//������IP
	char	suser[16];		//��¼�˺�
	char	spass[16];  	//��¼����
	WORD	wport;	//�˿�
	short	snres;	//����
	char	sres[4];		//����
}TDBPARG,*LPDBPARG;	//sizeof() = 56

#ifndef _DEFINE_RDBPROXY_STRUCT
#define _DEFINE_RDBPROXY_STRUCT

typedef struct t_ServerInfo
{
	DWORD dwVersion;			// �����������汾�ţ��汾��V2.5.4.0��ʾΪ0x02050400
	long lNowTime;				// ��ǰʱ��
	long lStartupTime;			// ����ʱ��
	DWORD dwConnectCount;		// ��ǰ������
	DWORD dwTagCount;			// ���б�ǩ��
	float fCPUUsage;			// CPUռ��
	DWORD dwHandleCount;		// �����
	DWORD dwThreadsCount;		// �߳���
	DWORD dwMemoryBytes;		// �ڴ�ʹ��
	DWORD dwMemoryBytesPeak;	// �߷��ڴ�ʹ��
	DWORD dwVirtualBytes;		// �����ڴ��С
	DWORD dwVirtualBytesPeak;	// �߷������ڴ��С
	DWORD dwReserves[52];
}SERVERINFO, *LPSERVERINFO; // sizeof()=256


typedef struct t_tagitem		//������������ǩ����
{
	char	m_sname[80];		//��ǩ��
	char	m_sdbname[16];		//���ݿ�ʵ����
	char	m_sdbtagname[80];	//���ݿ��ǩ��
	char	m_sdes[80];			//����
	char	m_sunit[16];		//��λ
	DWORD   m_dwid;				//��ǩID
	WORD	m_wtype;			//��������
	short   m_errcode;			//�������
	long	m_ltagflag;			//��ǩ��־,
	DWORD   m_dwres;			//��������д0
}TTAGITEM,*LPTAGITEM; //sizeof() = 288

// ������������ǩ��չ����
typedef struct t_tagitemex
{
	float dblLowLimit;			// ��������
	float dblHighLimit;			// ��������
	char szKKS[64];				// �豸KKS��
	char szDeviceSource[128];	// �豸Դ��1#����/��Ԥ����2#����/�ͷ��

	BOOL bScan;                 // �Ƿ񼤻�ɨ���ǩ���־, 1 ����, 0 ������
	long nScanCycle;            // ɨ�����ڣ���λΪ����
	char szTagSource[32];		// �߼��������
	char szSourceAddress[128];  // ��ǩ��ĵ�Դ:��ʵ����������
}TAGITEMEX, *LPTAGITEMEX;

// ���ڽ���ı�ǩ��Ϣ����ṹ��
typedef struct tagTAGITEMINFO
{
	char	szTagName[80];		// ��ǩ��
	char	szDBName[16];		// ���ݿ�ʵ����
	char	szDBTagName[80];	// ���ݿ��ǩ��
	char	szTagDescr[80];		// ��ǩ������Ϣ
	char	szTagUnit[16];		// ��ǩ��λ
	WORD	wDataType;			// ��ǩ��������
	long	nTagFlag;			// ��ǩ��־
	int		bSave;				// �Ƿ����,0���棬1����
	int 	bCompression;		// ѹ����ʽ��0-��ѹ����1-�ٷֱȾ���ѹ����2-����ֵ����ѹ��
	double	dblCompPrecision;	// ѹ������
	short	shErrorCode;		// ������
}TAGITEMINFO, *LPTAGITEMINFO;

typedef struct  t_vval		//����ṹ
{
	DWORD	vt;		//����
	long	lvlen;	//�ɱ䲿�ֳ���,��pstr,pblob,psoe��Ч,������֪���ȵĻ�������������0
					//�ַ������Ȳ�������β��0�ַ�
	union 	
	{
		long  	lval;		//�洢didital��int32
		float  	fval;		//�洢float32
		__int64 llval;		//�洢int64
		double 	dblval;		//�洢float64
		char*	pstr;		//�ַ�����ָ��
		void*	pblob;		//�洢pblob�Ĵ洢���׵�ַ
	};
	long  ltime;			//ʱ��
	short snqa;				//����
	short snerr;			//�������
}TVVAL,*LPVVAL; //sizeof() = 24 


typedef struct  t_vval2		//����ṹ
{
	DWORD	vt;		//����
	long	lvlen;	//�ɱ䲿�ֳ���,��pstr,pblob,psoe��Ч,������֪���ȵĻ�������������0
	//�ַ������Ȳ�������β��0�ַ�
	union 	
	{
		long  	lval;		//�洢didital��int32
		float  	fval;		//�洢float32
		__int64 llval;		//�洢int64
		double 	dblval;		//�洢float64
		char*	pstr;		//�ַ�����ָ��
		void*	pblob;		//�洢pblob�Ĵ洢���׵�ַ
	};
	long  ltime;			//ʱ��
	short snqa;				//����
	short snerr;			//�������
	short ms;				//����
}TVVAL2,*LPVVAL2; //sizeof() = 28 

typedef struct t_tagval		//��ǩֵ
{
	char	m_sname[80];	//��ǩ��
	TVVAL	m_val;			//ֵ
}TTAGVAL,*LPTAGVAL; //sizeof() = 104

#endif // end of _DEFINE_RDBPROXY_STRUCT

typedef struct  t_soe		//�¼��ṹ
{
	long	m_ltime;			//ʱ��
	long	m_lmsec;			//����
	char	m_ssrc[64];			//��Դ
	char	m_sdec[64];			//�����豸
	char	m_sdes[256];		//�¼�����
	char	m_stxtargs[256];	//�ı�����
	long	m_lclsid;			//�¼�����
	char	m_sres[4];			//����
	long	m_lbinarglen;		//��������
	void*	m_pbinargs;			//�¼�����
}TSOE,*LPSOE; //sizof() = 664

// ���ӳ�ʱ��Ĭ��Ϊ5000ms
#define PROPERTY_CONNECT_TIMEOUT		"connect_timeout"
// �����ݳ�ʱ��Ĭ��Ϊ30000ms
#define PROPERTY_READ_TIMEOUT			"read_timeout"
// ��С���ݳ�ʱ��Ĭ��Ϊ4000ms
#define PROPERTY_READ_SMALL_TIMEOUT		"read_small_timeout"
// д���ݳ�ʱ��Ĭ��Ϊ4000ms
#define PROPERTY_WRITE_TIMEOUT			"write_timeout"

//ö�ٺ���һ�δ���һ����ǩ��ԭ���Ǵ���һ����ǩ����������һ������ntags
typedef void (WINAPI *Enum_TagAttr)(LPTAGITEM ptag, int ntags, void* lparam); //ö�ٱ�ǩ����

extern "C"{
	// dbpapi�汾�ţ��汾��V2.5.4.0��ʾΪ0x02050400
	DWORD  __stdcall DBP_GetVersion();

	DBPH  __stdcall DBP_Open( 
		TDBPARG  conargs[],
		int nsize
		);
	DBPH  __stdcall DBP_Open2( 
		const char *szIP, 
		WORD wPort,
		const char *szUserName,
		const char *szPassword,
		BOOL bUseGroup
		);
	DWORD __stdcall DBP_Close( 
		DBPH h
		);
	DWORD __stdcall DBP_Connect( 
		DBPH h
		);
	DWORD __stdcall DBP_DisConnect( 
		DBPH h
		);

	DWORD __stdcall DBP_IsConnect( 
		DBPH h
		);
	DWORD __stdcall DBP_GetTagAttr(
		DBPH  h, 
		TTAGITEM tags[],
		int nsize
		);
	DWORD __stdcall DBP_GetTagAttrEx(
		DBPH  h, 
		TagNameArray sTagNames,
		TAGITEMEX tags[],
		short	errcode[],
		int nsize
		);
	DWORD __stdcall DBP_GetSnapshot(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_GetSnapshotOne(
		DBPH  h, 
		char* tagname,
		TVVAL tvs[]
		);

	DWORD __stdcall DBP_GetHisVal(
		DBPH  h, 
		const char* lpszTagName,
		long lts,
		long lte,
		long linsertsec,
		long lflag,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);
	DWORD __stdcall DBP_GetHisValFilter(
		DBPH  h, 
		const char* lpszTagName,
		long lts,
		long lte,
		long linsertsec,
		long lflag,
		const char *expression,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);

	/* 2020-10-16����API
		DBP_GetHisVal�ĸĽ��汾�����Զ������ַ�����Blob���͵���ʷ��¼��
		�����ַ�����Blob����ʱ��linsertsec��lflag��Ч��ֻ�ɶ���������

		����DBP_GetHistory2()���������lflag==1����ʾ����ֵ����linsertsecΪ����Ĳ�ֵ���

		������ַ�����Blob���͵���ʷ��¼����pvals��Ҫʹ��DBP_FreeValues()���ͷ��ڴ�
	*/
	DWORD __stdcall DBP_GetHistory(DBPH  h,LPCTSTR lpszTagName,long lts,long lte,long linsertsec,long lflag,
		TVVAL pvals[],int nsize,int* pnrecsize);
	/* ֧�ֺ������ݣ���lflag==1����ʷ��ֵʱ��linsertsec��λΪ���� */
	DWORD __stdcall DBP_GetHistory2(DBPH  h,LPCTSTR lpszTagName,long lts, short lts_ms, long lte, short lte_ms, long linsertsec,long lflag,
		TVVAL2 pvals[],int nsize,int* pnrecsize);
	void __stdcall DBP_FreeValues(TVVAL pvals[], int nsize);
	void __stdcall DBP_FreeValues2(TVVAL2 pvals[], int nsize);

	DWORD __stdcall DBP_WriteReal(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_WriteHis(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_WriteRealStr(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteRealBlob(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteHisStr(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteHisBlob(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteSoe(
		DBPH  h, 
		TSOE* psoe
		);

	DWORD __stdcall DBP_DelHis(
		DBPH  h, 
		const char* sName,
		long lts,
		long lte
		);
	DWORD __stdcall DBP_DelHis2(
		DBPH  h,
		TagNameArray sTagNames,
		long ltimes[],
		short snerrs[],
		int nsize
		);
	DWORD __stdcall DBP_DelSoe(
		DBPH  h, 
		long ltimes, 
		long lmsecs, 
		long ltimee,  
		long lmsece, 
		const char* ssrc, 
		const char* sdec, 
		long lclsid       
		);
	DWORD __stdcall DBP_GetSumVal(
		DBPH  h, 
		const char*  sName,
		long lts,
		long lte,
		long ltype,
		long lds,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);
	DWORD __stdcall DBP_GetRealStr(
		DBPH  h, 
		const char*  sName,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetRealBlob(
		DBPH  h, 
		const char*  sName,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetHisStr(
		DBPH  h, 
		const char*  sName,
		long ltime,
		unsigned long dwstyle,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetHisBlob(
		DBPH  h, 
		const char*  sName,
		long ltime,
		unsigned long dwstyle,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetSoe(
		DBPH  h, 
		long lts, 
		long lms, 
		const char* ssrc, 
		const char* sdec, 
		long lclsid,       
		DWORD dwStyle,		
		TSOE** ppsoe
		);

	DWORD __stdcall DBP_QueryTag(
		DBPH  h,
		LPCTSTR sfltname,
		LPCTSTR sfltdes,
		int ntype,
		Enum_TagAttr pfEnum,
		void* lparam
		); //��ѯ��ǩ

	DWORD __stdcall DBP_QueryTag2(
		DBPH  h,
		LPCTSTR sfltname,
		LPCTSTR sfltdes,
		int ntype,
		int ntagtype,
		Enum_TagAttr pfEnum,
		void* lparam); //��ѯ��ǩ

	DWORD __stdcall DBP_GetSvrConInfo(
		DBPH  h,
		LPDBPARG pconarg,
		int* pnNum);//ȡ�������ͻ�����

	DWORD __stdcall DBP_SendIdel(
		DBPH  h,
		long* plsrvoder);//���Ϳ��У����������Ƿ�������

	DWORD __stdcall DBP_InportTags(
		DBPH  h,
		const char* lpsztags,
		int nsize,
		BOOL bEnd); //�����ǩ��dbp������

	DWORD __stdcall DBP_GetServerInfo(
		DBPH h, 
		LPSERVERINFO lpServerInfo
		);

	DWORD __stdcall DBP_GetMultiPointHisVal(
		DBPH h, 
		long	lMode,				//ȡֵģʽ��RHV_BEFORE��RHV_AFTER��RHV_INTERP
		TagNameArray sTagNames, 
		long lTimes[],
		TVVAL tvs[], 
		int nsize
		);

	DWORD __stdcall DBP_GetDiSumVal(
		DBPH  h, 
		LPCTSTR lpszTagName, 
		long lts, 
		long lte, 
		long lds, 
		long lval,
		long lpSumCount[], 
		long lpSumTimes[], 
		int nsize, 
		int* pnrecsize
		);

	DWORD __stdcall DBP_GetGroups(
		DBPH  h,
		TDBPARG  conargs[],
		int *pnsize); //��ȡDBPȺ������

	DWORD __stdcall DBP_SetGroups(
		DBPH  h,
		TDBPARG  conargs[],
		int nsize); //����DBPȺ������

	DWORD __stdcall DBP_TagSearch(DBPH h,
		char* szTagNameMask,		// ��ǩ�����룬*��ʾȫ����?��ʾ����һ���ַ�
		char* szTagDescrMask,		// ��ǩ�������룬*��ʾȫ����?��ʾ����һ���ַ�
		char* szDBNameMask,			// ���ݿ������룬*��ʾȫ����?��ʾ����һ���ַ�
		int nDataType,				// �����������룬-1��ʾȫ��
		int nTagType,				// ��ǩ�������룬-1��ʾȫ��
		LPTAGITEM pTagItems,		// ���ر�ǩ��������ı�ǩ��������
		int* pnSize,				// ����pTagItems�����С�������ر�ǩ����������������������������������Ҫ���������������ʾ�������
		DWORD* pdwSearchSession,	// ���δ���0�����ر�ǩ����Session
		float* pfltPerProgress		// ����������ɰٷ��ʣ���Чֵ0.0-100.0
		);

	DWORD __stdcall DBP_InsertTags(DBPH h,						// �����ǩ
		LPTAGITEMINFO pTagItemInfos,	// ��Ҫ����Ĳ����Ϣ
		int nTagCount,				// ������
		BOOL bUpdate				// ����Ѵ��ڣ��Ƿ����
		);

	DWORD __stdcall DBP_DeleteTags(DBPH h,						// ɾ����ǩ
		TagNameArray szTagNames,		// ��Ҫɾ���Ĳ����������
		int nTagCount,				// ��Ҫɾ���Ĳ�������������
		short *pErrors				// ��Ӧ���ɾ����ִ�н��������ӦΪnTagCount			
		);

	//////////////////////////////////////////////////////////////////////////
	// �������Զ�д�ӿ�
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_get_property(
		DBPH h,							// �������Ӿ��
		const char* szPropertyName,		// ������������
		void* pPropertyData,			// �����õ���������ֵ�洢�ڴ��
		int nSize						// �����õ���������ֵ�洢�ڴ���С
		);

	DWORD __stdcall dbp_set_property(
		DBPH h,							// �������Ӿ��
		const char* szPropertyName,		// ������������ 
		void* pPropertyData,			// ���ڽ��ܷ�����������ֵ�Ĵ洢�ڴ�� 
		int nSize						// ���ڽ��ܷ�����������ֵ�Ĵ洢�ڴ���С
		);

	//////////////////////////////////////////////////////////////////////////
	// ֧�ֱ�ǩ·�ɵ���API�ӿ�
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_get_snapshot(DBPH h, TagNameArray szTagNames, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_write_snapshot(DBPH h, TagNameArray szTagNames, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_get_history(DBPH  h, const char* lpszTagName, long lStartTime, long lEndTime, 
		long lInsertSec, long lFlag, TVVAL* pTagVals, int* pnSize);

	DWORD __stdcall dbp_write_history(DBPH  h, TagNameArray sTagNames, TVVAL* pTagVals,int nSize);

	DWORD __stdcall dbp_write_single_history(DBPH  h, const char* lpszTagName, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_get_history_section(DBPH h, long lMode, TagNameArray szTagNames, long* lTimes, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_delete_history(DBPH  h, const char* lpszTagName, long lStartTime, long lEndTime);

	DWORD __stdcall dbp_delete_history2(DBPH  h, TagNameArray szTagNames, long* pTimes, short* pErrors, int nSize);

	//////////////////////////////////////////////////////////////////////////
	// RPC�ӿ�
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_rpc_create(DBPH h, HDBPRPCSTREAM* pHRPCStream);

	DWORD __stdcall dbp_rpc_destroy(HDBPRPCSTREAM hRPCStream);

	DWORD __stdcall dbp_rpc_call(HDBPRPCSTREAM hRPCStream, const char* lpszRPCFuncName);

	DWORD __stdcall dbp_rpc_clear(HDBPRPCSTREAM hRPCStream);

	DWORD __stdcall dbp_rpc_size(HDBPRPCSTREAM hRPCStream, DWORD* pSize);

	DWORD __stdcall dbp_rpc_position(HDBPRPCSTREAM hRPCStream, DWORD* pPosition);

	DWORD __stdcall dbp_rpc_seek(HDBPRPCSTREAM hRPCStream, DWORD dwOffsetPosition, DWORD dwOriginFlag);

	DWORD __stdcall dbp_rpc_write(HDBPRPCSTREAM hRPCStream, void* pBuffer, DWORD dwSize);

	DWORD __stdcall dbp_rpc_read(HDBPRPCSTREAM hRPCStream, void* pBuffer, DWORD* pSize);
}
#endif //_DPAPI2_H_I