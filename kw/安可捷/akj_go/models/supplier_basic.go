package models

type SupplierApply struct {
	ID                uint   `gorm:"primarykey;" json:"Id"`
	SuppilerName      string `gorm:"column:suppiler_name;DEFAULT NULL" json:"suppiler_name"`
	SupplierId        int    `gorm:"column:supplier_id;"`
	Product           string `gorm:"column:product;DEFAULT NULL"`
	Category          string `gorm:"column:category;DEFAULT NULL"`
	Type              int    `gorm:"column:type;DEFAULT NULL"`
	System            string `gorm:"column:system;DEFAULT NULL"`
	EngageAddress     string `gorm:"column:engage_address;DEFAULT NULL"`
	SupplierAssessUrl string `gorm:"column:supplier_assess_url;DEFAULT NULL"`
	BusinessUrl       string `gorm:"column:business_url;DEFAULT NULL"`
	Appendix          string `gorm:"column:appendix;DEFAULT NULL"`
	TaxNumber         string `gorm:"column:tax_number;DEFAULT NULL"`
	RegisteredAddress int    `gorm:"column:registered_address;DEFAULT NULL"`
	Tel               string `gorm:"column:tel;DEFAULT NULL"`
	ApprovalStatus    int    `gorm:"column:approval_status;DEFAULT NULL"`
	BankName          string `gorm:"column:bank_name;DEFAULT NULL"`
	AccountNo         string `gorm:"column:account_no;DEFAULT NULL"`
	Information       string `gorm:"column:information;DEFAULT NULL"`
}

func (table *SupplierApply) TableName() string {
	return "supplier_apply"
}
