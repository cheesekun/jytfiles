package controller

import (
	"akj_go/models"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func SupplierList(c *gin.Context) {
	size, _ := strconv.Atoi(c.DefaultQuery("size", "5"))
	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	fmt.Println(size)
	if err != nil {
		log.Println("缺少页码")
		return
	}
	page = (page - 1) * size
	var count int64
	keyword := c.Query("keyword")
	SupplierList := make([]*models.SupplierApply, 0)
	if keyword == "" {
		err = models.DB.Model(new(models.SupplierApply)).
			Count(&count).Limit(size).Offset(page).Find(&SupplierList).Error
	} else {
		err = models.DB.Model(new(models.SupplierApply)).Where("suppiler_name like ?", "%"+keyword+"%").
			Count(&count).Limit(size).Offset(page).Find(&SupplierList).Error
	}

	if err != nil {
		log.Println("GetCategoryList Error:", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "获取分类列表失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"data": map[string]interface{}{
			"list":  SupplierList,
			"count": count,
		},
	})

}

func SupplierCreate(c *gin.Context) {
	supplierId, _ := strconv.Atoi(c.PostForm("supplier_id"))
	suppilerName := c.PostForm("suppiler_name")
	supplierApply := &models.SupplierApply{
		SupplierId:   supplierId,
		SuppilerName: suppilerName,
	}
	err := models.DB.Create(supplierApply).Error
	if err != nil {
		log.Println("CategoryCreate Error:", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "创建分类失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "创建成功",
	})
}
func SupplierUpdate(c *gin.Context) {
	id, _ := strconv.Atoi(c.PostForm("id"))
	suppilerName := c.PostForm("suppiler_name")
	supplierApply := &models.SupplierApply{
		SuppilerName: suppilerName,
	}
	err := models.DB.Model(new(models.SupplierApply)).Where("id=?", id).Updates(supplierApply).Error
	if err != nil {
		log.Println("CategoryModify Error:", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "修改分类失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "修改成功",
	})
}

func SupplierDelete(c *gin.Context) {
	id, _ := strconv.Atoi(c.PostForm("id"))
	fmt.Println(id)
	var cate models.SupplierApply
	err := models.DB.Model(new(models.SupplierApply)).Where("id=?", id).Delete(&cate).Error
	if err != nil {
		log.Println("CategoryCreate Error:", err)
		c.JSON(http.StatusOK, gin.H{
			"code": -1,
			"msg":  "创建分类失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": 200,
		"msg":  "删除成功",
	})
}
