package router

import (
	"akj_go/controller"

	"github.com/gin-gonic/gin"
)

func InitRouter() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.String(200, "Hello, Gin")
	})
	r.SetTrustedProxies([]string{"127.0.0.1"})
	r.GET("/supplier-list", controller.SupplierList)
	r.POST("/supplier-del", controller.SupplierDelete)
	r.POST("/supplier-add", controller.SupplierCreate)
	r.POST("/supplier-update", controller.SupplierUpdate)
	r.Run() // listen and serve on 0.0.0.0:8080 修改端口，如参:端口 r.Run(":9090")
}
