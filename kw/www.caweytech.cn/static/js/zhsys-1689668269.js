(function () {
var data = {
infor:{"swiper_type":"pagination","banner_size":{"radio_val":"custom","default":{"width":"100%","height":"42.1%"},"custom":{"width":"100.0%","height":"18.3%"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"banner_set":{"radio_val":"custom","default":{"speed":0.5,"delay":2},"custom":{"speed":0.5,"delay":2}},"video_height":{"radio_val":"custom","default":{"height":"42%"},"custom":{"height":"42%"}},"end_set_height":"18.3%","banner_btn_color":{"radio_val":"custom","default":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"},"custom":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"}},"num_color":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 1)"},"custom":{"color":"rgba(255, 255, 255, 1)"}}},
listArr:{"full_screen_banner_pattern":"banner","video":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/flv.mp4","add_img":"default","poster":"","animateObj":{"value":1,"animate":{"_key":"slide"}}},
banner_type:'private',
type:'pc',
children:[{"name":"layout_column","pid":707800,"widgets_id":707801,"data":{"pc_infor":{"col":12,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":0,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"5g2tol8gvqk000","full_screen_banner":[],"full_screen_banner_pattern":"video"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false},{"name":"layout_column","pid":707800,"widgets_id":707802,"data":{"pc_infor":{"col":12,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":1,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"1s2pvzjnrmtc00","full_screen_banner":{"src":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/12\/9B\/wKj2K2LnamyAOqSiAADblzpkrfQ175.jpg","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}},"full_screen_banner_pattern":"banner"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false}],
animateObj: null,
}

var id= 707800;

var banner = new Full_screen_banner(id,data);
})();(function () {
var data = {
infor:null,
listArr:{"title":"<div style=\"text-align:left;\"><span style=\"color:#326B86;font-size:26px;\">\u667a\u6167\u5b9e\u9a8c\u5ba4<\/span><\/div>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423202;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423202
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:14px;color:#000000;\">\u4ee5\u6570\u5b57\u5316\u7ba1\u7406\u7cfb\u7edf\u4e3a\u652f\u6491\uff0c\u5b9e\u73b0\u4f01\u4e1a\u7cbe\u7ec6\u5316\u7ba1\u7406\uff0c\u63a8\u52a8\u4f01\u4e1a\u7ba1\u7406\u6a21\u5f0f\u7684\u521b\u65b0<\/span><br>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423203;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423203
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:14px;color:#000000;\">\u8fdc\u7a0b\u63a7\u5236\u8bbe\u5907\u548c\u5176\u4ed6\u7528\u7535\u8bbe\u5907<\/span><br>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423204;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423204
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:14px;color:#000000;\">\u7efc\u5408\u5b9e\u9a8c\u7ba1\u7406\u5e73\u53f0<\/span><br>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423205;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423205
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<div style=\"text-align:left;\"><span style=\"color:#326B86;font-size:26px;\">\u53ef\u89c6\u5316\u5c55\u793a\u5e73\u53f0<\/span><\/div>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423207;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423207
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:14px;color:#000000;\">\u6570\u5b57\u5b6a\u751f\u6280\u672f<\/span><br>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423208;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423208
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:14px;color:#000000;\">\u7269\u8054\u7f51<\/span><br>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 423209;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 423209
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:{"swiper_type":"pagination","banner_size":{"radio_val":"custom","default":{"width":"100%","height":"50.1%"},"custom":{"width":"100.0%","height":"26.9%"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"banner_set":{"radio_val":"custom","default":{"speed":0.5,"delay":2},"custom":{"speed":0.5,"delay":2}},"video_height":{"radio_val":"custom","default":{"height":"42%"},"custom":{"height":"42%"}},"end_set_height":"26.7%","banner_btn_color":{"radio_val":"custom","default":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"},"custom":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"}},"num_color":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 1)"},"custom":{"color":"rgba(255, 255, 255, 1)"}}},
listArr:{"full_screen_banner_pattern":"banner","video":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/flv.mp4","add_img":"default","poster":"","animateObj":{"value":1,"animate":{"_key":"slide"}}},
banner_type:'private',
type:'mb',
children:[{"name":"layout_column","pid":707800,"widgets_id":707801,"data":{"pc_infor":{"col":12,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":0,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"5g2tol8gvqk000","full_screen_banner":[],"full_screen_banner_pattern":"video"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false},{"name":"layout_column","pid":707800,"widgets_id":707802,"data":{"pc_infor":{"col":12,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":1,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"1s2pvzjnrmtc00","full_screen_banner":{"src":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/12\/9B\/wKj2K2LnamyAOqSiAADblzpkrfQ175.jpg","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}},"full_screen_banner_pattern":"banner"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false}],
animateObj: null,
}

var id= 143200;

var banner = new Full_screen_banner(id,data);
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/54\/wKj2K2LoyI6Abad1AAem3hjbiF0820.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 238302
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 238302
  var data = {
    animateObj: {"value":2,"animate":{"direction":"right","duration":0.59999999999999998,"delay":0,"trigger":"single","_key":"moveIn"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/54\/wKj2K2LoyI6Af6J8AATMxlwZ9t4503.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 238304
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 238304
  var data = {
    animateObj: {"value":2,"animate":{"direction":"left","duration":0.59999999999999998,"delay":0,"trigger":"single","_key":"moveIn"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/54\/wKj2K2LoyI6APpv8AAIwkaTGkAo836.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 238305
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 238305
  var data = {
    animateObj: {"value":2,"animate":{"direction":"left","duration":0.59999999999999998,"delay":0,"trigger":"single","_key":"moveIn"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
pc_widgets_infor:{"font":{"radio_val":"custom","default":{"fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","fontSize":14,"color":"rgba(102,102,102,1)","fontWeight":"normal","fontStyle":"normal","textDecoration":"normal","selectColor":"rgba(239,65,54,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","fontSize":14,"color":"rgba(102,102,102,1)","fontWeight":"normal","fontStyle":"normal","textDecoration":"normal","selectColor":"rgba(50,107,134,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}}},
listArr:{"moduleStyle":0}
}
var id= 379402;
var message = new Basic_crumbs(id,data);
})();


(function () {
  var id = 379402
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {

window.globalAnimate = fixed
var window_resize = new Window_resize();
var fixed = new Scroll_init({"vip_type":"default","vip_type_footer":"default","theme_color":"rgba(50,107,134,1)","previewModule":"this","header_display":"true","footer_display":"true","body_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"default","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"footer_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"createrMessage":{"createrShow":"true","loginShow":"true"},"fixed_header":"true","fixed_height":"system","fixed_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"fixed_back_top":{"button_style":"default","show_back_top":true,"pcShowBtn":true,"mbShowBtn":true,"button_color":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(50,107,134,1)","iconName":"el-icon-arrow-up","color":"#fff"},"custom":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"customColor":"rgba(0, 0, 0, 0.1)"},"setting":{"title":"\u4e0a\u6d77\u607a\u851a\u79d1\u6280\u6709\u9650\u516c\u53f8","logoTitleRadio":true,"headerNavRadio":true,"returnTopRadio":true,"multiLanguageOpen":false,"footnavOpen":false,"zhLanguageOpen":false,"zhLanguageAdress":"","loginRegister":false},"mb_header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"mb_header_btn_background":{"radio_val":"custom","default":{"color":"rgba(0, 0, 0,1)"},"custom":{"color":"rgba(0, 0, 0,1)"}},"displayContactRadio":"block","theme_color_type":"custom_5gmquce5c6aups","mb_header_nav_position":"left","mb_header_nav_title":{"radio_val":"custom","default":{"fontSize":18},"custom":{"fontSize":18}},"contact_us":{"contact_us_show":false,"contact_us_set":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(55, 85, 239, 1)","iconPosition":"-0 -0"},"customSet":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"listArr":{"link":{"select_val_preview":"","linkType":"fun","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"other_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}},"icp_isShow":"false","adaptWidth":{"isOpen":"true","radioVal":"custom","val":1460,"customVal":1440},"nav_min_width":{"radio_val":"custom","default":{"val":768},"custom":{"val":768}},"custom_theme_color_list":[{"key":"custom_5gmquce5c6aups","color":"rgba(50,107,134,1)"}],"custom_nav_color_list":[],"header_layout_row_setting":"xuanfu","xuanfu_position":"station"});
var common = {"vip_type":"default","vip_type_footer":"default","theme_color":"rgba(50,107,134,1)","previewModule":"this","header_display":"true","footer_display":"true","body_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"default","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"footer_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"createrMessage":{"createrShow":"true","loginShow":"true"},"fixed_header":"true","fixed_height":"system","fixed_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"fixed_back_top":{"button_style":"default","show_back_top":true,"pcShowBtn":true,"mbShowBtn":true,"button_color":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(50,107,134,1)","iconName":"el-icon-arrow-up","color":"#fff"},"custom":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"customColor":"rgba(0, 0, 0, 0.1)"},"setting":{"title":"\u4e0a\u6d77\u607a\u851a\u79d1\u6280\u6709\u9650\u516c\u53f8","logoTitleRadio":true,"headerNavRadio":true,"returnTopRadio":true,"multiLanguageOpen":false,"footnavOpen":false,"zhLanguageOpen":false,"zhLanguageAdress":"","loginRegister":false},"mb_header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"mb_header_btn_background":{"radio_val":"custom","default":{"color":"rgba(0, 0, 0,1)"},"custom":{"color":"rgba(0, 0, 0,1)"}},"displayContactRadio":"block","theme_color_type":"custom_5gmquce5c6aups","mb_header_nav_position":"left","mb_header_nav_title":{"radio_val":"custom","default":{"fontSize":18},"custom":{"fontSize":18}},"contact_us":{"contact_us_show":false,"contact_us_set":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(55, 85, 239, 1)","iconPosition":"-0 -0"},"customSet":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"listArr":{"link":{"select_val_preview":"","linkType":"fun","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"other_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}},"icp_isShow":"false","adaptWidth":{"isOpen":"true","radioVal":"custom","val":1460,"customVal":1440},"nav_min_width":{"radio_val":"custom","default":{"val":768},"custom":{"val":768}},"custom_theme_color_list":[{"key":"custom_5gmquce5c6aups","color":"rgba(50,107,134,1)"}],"custom_nav_color_list":[],"header_layout_row_setting":"xuanfu","xuanfu_position":"station"}
// if (!common.header_display) {
// document.getElementsByClassName('layout_header')[0].style.display = 'none';
// document.getElementsByClassName('rowFixedNext')[0].style.display = 'none';
// }
new Back_top(common);

new Contact_us(common.contact_us);
var scrollType = window.scrollType;
var privateData = {"banner_isShow":true,"banner_type":"private","fullPageTopNavType":"zhanwei","fullPageStyleType":"0","fullPageFooterType":"typeRow1","floatingFrameOpen":false,"floatingFrameStyle":{"data":{"img_effects":{"id":0,"formData":{"effect":0,"border":{"radio_val":"default","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"default","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 1)"}},"direction":"left"}},"pc_widgets_infor":{"width":{"radio_val":"default","default":{"width":200},"custom":{"width":200}},"height":{"radio_val":"default","default":{"height":200},"custom":{"height":200}},"background":{"radio_val":"default","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}},"boxShadow":{"radio_val":"default","default":{"direction":"outset","x":0,"y":0,"spread":0,"blur":0,"color":"rgba(0,0,0,0.3)","seColor":"rgba(0,0,0,0.5)"},"custom":{"direction":"outset","x":0,"y":0,"spread":0,"blur":4,"color":"rgba(0,0,0,0.3)","seColor":"rgba(0,0,0,0.5)"}},"position":"center_center","trigger":"one","playSet":{"delay":1,"space":1}},"mb_widgets_infor":[],"listArr":{"content":"\u8bf7\u8f93\u5165\u5185\u5bb9"}},"name":"basic_floating_frame"},"fixed_background_isOpen":"false","H1_isOpen":false,"ceilingAdvertisingOpen":false,"ceilingAdvertisingStyle":{"data":{"pc_widgets_infor":{"position":"footer","img_url":"","close_img":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/ceiling_advertising\/default.png","botton":{"type":"system","img_id":0},"background":{"radio_val":"default","default":{"color":"rgba(127, 127, 127,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"seColor":"rgba(239,65,54,1)","sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"color":"rgba(127, 127, 127,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"seColor":"rgba(100, 100, 100,1)","sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}}},"mb_widgets_infor":[]},"name":"basic_ceiling_advertising"}};
var data = {
    scrollType: scrollType,
    private: privateData,
    common: common
}
is_mobile()!=='mb' && initFullPage()
window.initFullPage = initFullPage

function initFullPage(){
    if(scrollType === 'row' || scrollType === 'col'){
        var fullPage = new FullPage({
            data: data,
            el: '.mb_body_view',                            // selector of contain
            slideTime: 600,                               // time of slide
            continuous: false,                            // create an infinite feel with no endpoints
            mode: 'wheel,nav:full-page-nav',               // mode of fullpage
            start: 0,                                     // which page will display when install
            easing: '',                                // easing('ease','ease-in','ease-in-out' or use cubic-bezier like [.33, 1.81, 1, 1];
            onTransitionStart: function(index, thisPage){
                fixed.make_animate()
            },
            callback: function (index, thisPage) {       // callback when pageChange
                var backTopVisible = index > 0 && is_mobile() === 'pc'
                backTopVisible ? $('.back_top_wrapper').show() : $('.back_top_wrapper').hide()
                fixed.countup_start()
            }
        });
        setTimeout(function(){
            fixed.make_animate()
        },100)
        window.fullPage = fullPage
        return fullPage
    }
}




})();