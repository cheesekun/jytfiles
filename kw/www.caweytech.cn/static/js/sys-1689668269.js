(function () {
var data = {
infor:{"swiper_type":"pagination","banner_size":{"radio_val":"custom","default":{"width":"100%","height":"42.1%"},"custom":{"width":"100.0%","height":"23.0%"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"banner_set":{"radio_val":"custom","default":{"speed":0.5,"delay":2},"custom":{"speed":0.5,"delay":2}},"video_height":{"radio_val":"custom","default":{"height":"42%"},"custom":{"height":"42.0%"}},"end_set_height":"23.0%","banner_btn_color":{"radio_val":"custom","default":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"},"custom":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"}},"num_color":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 1)"},"custom":{"color":"rgba(255, 255, 255, 1)"}}},
listArr:{"full_screen_banner_pattern":"banner","video":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/flv.mp4","add_img":"default","poster":"","animateObj":{"value":1,"animate":{"_key":"slide"}}},
banner_type:'private',
type:'pc',
children:[{"name":"layout_column","pid":375200,"widgets_id":375201,"data":{"pc_infor":{"col":12,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":0,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"38c9umlfa8y000","full_screen_banner":[],"full_screen_banner_pattern":"video"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false},{"name":"layout_column","pid":375200,"widgets_id":375202,"data":{"pc_infor":{"col":12,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":1,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"2fjeos2ltvdw00","full_screen_banner":{"src":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/56\/11\/rBwBEmRt5waARcLPAALg2lPkSag029.jpg","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}},"full_screen_banner_pattern":"banner"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false}],
animateObj: null,
}

var id= 375200;

var banner = new Full_screen_banner(id,data);
})();(function () {
var data = {
infor:null,
listArr:{"title":"<p style=\"margin:0px;\"><span style=\"color:rgba(51,51,51,0);\">1<\/span><\/p><p style=\"margin:0px;\"><br><\/p><p style=\"margin:0px;\"><br><\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 371502;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 371502
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:{"swiper_type":"pagination","banner_size":{"radio_val":"custom","default":{"width":"100%","height":"50.1%"},"custom":{"width":"100.0%","height":"26.9%"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"banner_set":{"radio_val":"custom","default":{"speed":0.5,"delay":2},"custom":{"speed":0.5,"delay":2}},"video_height":{"radio_val":"custom","default":{"height":"42%"},"custom":{"height":"42%"}},"end_set_height":"26.7%","banner_btn_color":{"radio_val":"custom","default":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"},"custom":{"color":"rgba(0,0,0,0.1)","seColor":"rgba(229, 229, 229, 1)"}},"num_color":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 1)"},"custom":{"color":"rgba(255, 255, 255, 1)"}}},
listArr:{"full_screen_banner_pattern":"banner","video":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/flv.mp4","add_img":"default","poster":"","animateObj":{"value":1,"animate":{"_key":"slide"}}},
banner_type:'private',
type:'mb',
children:[{"name":"layout_column","pid":375200,"widgets_id":375201,"data":{"pc_infor":{"col":12,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":0,"padding_pattern":"default","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"default","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"38c9umlfa8y000","full_screen_banner":[],"full_screen_banner_pattern":"video"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false},{"name":"layout_column","pid":375200,"widgets_id":375202,"data":{"pc_infor":{"col":12,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"mb_infor":{"col":1,"order":1,"padding_pattern":"custom","padding_left":"0%","padding_right":"0%","padding_top":"0%","padding_bottom":"0%","margin_pattern":"custom","margin_top":"0%","margin_bottom":"0%","margin_left":"0%","margin_right":"0%","is_shadow":"false","border_set":{"radio_val":"custom","default":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]},"custom":{"border_width":0,"border_left_width":0,"border_right_width":0,"border_top_width":0,"border_bottom_width":0,"border_color":"rgba(0,0,0,1)","border_style":"solid","border_check_list":[]}},"background_set":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 0)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"col_id":"2fjeos2ltvdw00","full_screen_banner":{"src":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/56\/11\/rBwBEmRt5waARcLPAALg2lPkSag029.jpg","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}},"full_screen_banner_pattern":"banner"},"rowType":"full_screen_banner","children":[],"isShow":true,"isEdit":false}],
animateObj: null,
}

var id= 437700;

var banner = new Full_screen_banner(id,data);
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p style=\"color:#FFFFFF;font-size:14px;line-height:2;padding:110px;\">\n\t<br>\n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:32px;\">CAWEY-LIMS<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\">\u9075\u5faa<\/span><span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\">IEC\/ISO 17025,\u4e13\u6ce8\u4e8e\u8bd5\u9a8c\u9886\u57df,\u5b9e\u73b0\u4eba\uff0c\u673a\uff0c\u6599\uff0c\u6cd5\uff0c\u73af\uff0c\u6d4b\u7684\u5168\u9762\u8d28\u91cf\u7ba1\u7406\uff0c\u4e3a\u5ba2\u6237\u7684\u4fe1\u606f\u5316\u5efa\u8bbe\u63d0\u4f9b\u9ad8\u6548\u65b9\u6848\uff0c\u8ba9\u4f01\u4e1a\u5b9e\u9a8c\u5ba4\u6570\u5b57\u5316\u8f6c\u578b\u7a33\u6b65\u843d\u5730\u3002<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\"><br>\n<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\"><br>\n<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\"><br>\n<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\"><br>\n<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:24px;\"><br>\n<\/span> \n<\/p>\n<span style=\"color:#FFFFFF;font-size:14px;line-height:2;\"><\/span>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"http:\/\/18980303.s142i.faiusr.com\/2\/AI4BCM_7hgkQAhgAIPSW5PEFKJb2iOAHMIAKONAF.jpg","desc":""}
  }

  var id = 557002

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 557002
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/25\/3B\/rBwBEmQ0wiSAXZxhAAo86qkEGn4460.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"summary":""}
  }
  var id = 635802
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 635802
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"\u56e2\u961f\u5185\u4e2d\u9ad8\u5c42\u5305\u542b\u591a\u4f4d\u6765\u81ea\u8bd5\u9a8c\u884c\u4e1a\u4e0a\u4e0b\u6e38\u7684\u8d44\u6df1\u7ba1\u7406\u4eba\u5458\u53caLIMS\u8d44\u6df1\u9500\u552e\uff0c\u66f4\u61c2\u5f97\u5ba2\u6237\u7684\u75db\u70b9\u53ca\u9700\u6c42\uff0c\u4fdd\u8bc1\u8f6f\u4ef6\u66f4\u5f3a\u7684\u843d\u5730\u6027\u53ca\u66f4\u9ad8\u7684\u6027\u4ef7\u6bd4\u3002\u6807\u51c6\u7248\u3001\u5347\u7ea7\u7248\u53ca\u5b9a\u5236\u7248\u6ee1\u8db3\u5ba2\u6237\u4e0d\u540c\u4f53\u91cf\u53ca\u7ba1\u7406\u9700\u6c42\u3002\n\n\n\n\n","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 635804;
var text = new Basic_text(id,data);
})();


(function () {
  var id = 635804
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:20px;color:#FB780E;\"><strong><span style=\"color:#FB780E;font-size:20px;font-family:\u5fae\u8f6f\u96c5\u9ed1;\"><\/span><span style=\"color:#FB780E;font-size:20px;font-family:\u5fae\u8f6f\u96c5\u9ed1;\">\u2014\u2014<\/span><\/strong><\/span>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 177002;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 177002
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p>\n\t<span style=\"color:#CCCCCC;font-size:14px;white-space:normal;\"><span style=\"color:#333333;font-family:Arial, sans-serif;font-size:14px;white-space:normal;background-color:#FFFFFF;\"> <\/span><\/span>\n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"color:#CCCCCC;font-size:14px;\">CLEAR AND EYE-CATCHING&nbsp;<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"color:#CCCCCC;font-size:14px;\">SOFTWARE WORKBENCH DESIGN<\/span> \n<\/p>\n<p>\n\t<span style=\"color:#CCCCCC;font-size:14px;white-space:normal;\"><\/span> \n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 177003

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 177003
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p>\n\t<span class=\"zyc1\" style=\"white-space:normal;font-size:30px;\"><span style=\"color:#FA780E;font-family:\u5fae\u8f6f\u96c5\u9ed1;\">\u9192\u76ee\u6e05\u6670<\/span><span style=\"color:#000000;\">\u7684\u5de5\u4f5c\u53f0\u8bbe\u8ba1<\/span><\/span>\n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 177004

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 177004
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p>\n\t<span class=\"zyc\" style=\"color:#666666;font-family:\u5fae\u8f6f\u96c5\u9ed1;font-size:16px;white-space:normal;\"><span style=\"line-height:2;\"> <\/span><\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-size:18px;color:#666666;font-family:&quot;Microsoft YaHei&quot;;\">\u6309\u804c\u52a1\u5373\u65f6\u63d0\u793a\u5404\u5f85\u529e\u4e8b\u9879\uff0c\u4fdd\u8bc1\u5de5\u4f5c\u65e0\u758f\u6f0f<\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:&quot;Microsoft YaHei&quot;;font-size:18px;color:#666666;\">\u8bbe\u7f6e\u76f8\u5173\u6a21\u5757\u7684\u5feb\u6377\u5165\u53e3<\/span> <span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;\"><span style=\"font-size:18px;color:#666666;font-family:&quot;Microsoft YaHei&quot;;\">\u5feb\u901f<\/span><span style=\"font-size:18px;color:#666666;font-family:&quot;Microsoft YaHei&quot;;\">\u4fbf\u6377\u8fdb\u5165\u76ee\u6807\u6a21\u5757<\/span><\/span> \n<\/p>\n<p class=\"MsoNormal\">\n\t<span style=\"font-family:&quot;Microsoft YaHei&quot;;font-size:18px;color:#666666;\">\u9996\u9875\u5c55\u793a\u5173\u952e\u7edf\u8ba1\u4fe1\u606f<\/span> <span style=\"font-family:\u5fae\u8f6f\u96c5\u9ed1;\"><span style=\"font-size:18px;color:#666666;font-family:&quot;Microsoft YaHei&quot;;\">\u5bf9\u5173<\/span><span style=\"font-size:18px;color:#666666;font-family:&quot;Microsoft YaHei&quot;;\">\u6ce8\u9879\u53d1\u5c55\u8d8b\u52bf\u968f\u65f6\u638c\u63a7<\/span><\/span> \n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 177005

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 177005
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
listArr:{"type":"default","content":"<p><\/p>\n<style>\n@media screen and (max-width: 768px ){\n.zyc1{\nfont-size:25px!important;\n}}\n@media screen and (max-width: 1366px ){\n.zyc1{\nfont-size:23px!important;\n}}\n@media screen and (max-width: 1366px ){\n.zyc{\nfont-size:12px!important;\n}}\n<\/style>","id":177006,"note":"\u4fee\u6539\u4ebaIP\uff1a{\"cip\":\"127.0.0.1\",\"cid\":\"00\",\"cname\":\"\u672a\u77e5\"} \u4fee\u6539\u65f6\u95f4\uff1aTue Apr 11 2023 13:53:21 GMT+0800 (\u4e2d\u56fd\u6807\u51c6\u65f6\u95f4)"}
}
var id= 177006;
new Basic_plugin_code(id,data);
})();

(function () {
  var id = 177006
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/25\/93\/rBwBEmQ0846ANPnWAAN1YkVY2Zo021.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 177008
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 177008
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/25\/DB\/rBwBEmQ1El6AWhuRAAb6JwmK8uY425.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"summary":""}
  }
  var id = 944202
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 944202
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"\u9879\u76ee\u7ba1\u7406","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 944204;
var text = new Basic_text(id,data);
})();


(function () {
  var id = 944204
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"\u5168\u6d41\u7a0b\u6570\u636e\u8bb0\u5f55--\u5b9e\u9a8c\u5ba4\u77e5\u8bc6\u79ef\u7d2f\uff0c\u7ecf\u9a8c\u4f20\u627f\u3002\n\u754c\u9762\u53cb\u597d--\u5404\u9879\u76ee\u72b6\u6001\u4e00\u76ee\u4e86\u7136   \u672a\u5f00\u59cb\u3001\u5df2\u5f00\u59cb\u3001\u5f85\u5206\u914d\u3001\u8fdb\u884c\u4e2d\u3001\u5df2\u7ec8\u6b62\u3001\u5df2\u5b8c\u6210\u3001\u65b0\u4efb\u52a1\u63d0\u9192\uff0c\u5f85\u767b\u8bb0\u3001\u5df2\u7ed3\u675f\n\n","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 944205;
var text = new Basic_text(id,data);
})();


(function () {
  var id = 944205
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"\u4fbf\u6377\u79fb\u52a8\u529e\u516cPDA","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 122502;
var text = new Basic_text(id,data);
})();


(function () {
  var id = 122502
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"\u652f\u6301\u89c6\u9891\u3001\u62cd\u7167\u53ca\u6587\u672c\u8f93\u5165\uff0c\u652f\u6301GPS\u5b9a\u4f4d\n\u4e0e\u7cfb\u7edf\u65e0\u7f1d\u5bf9\u63a5\uff0c\u4fdd\u8bc1\u8bd5\u9a8c\u4fe1\u606f\u4fbf\u6377\u3001\u5b9e\u65f6\u4e0a\u4f20\u7cfb\u7edf\uff0c\u5b9e\u73b0\u573a\u5730\u4eba\u5458\u79fb\u52a8\u529e\u516c\u9700\u6c42\n\u914d\u5907\u626b\u7801\u6280\u672f\uff0c\u5feb\u901f\u8ffd\u6eaf\u76ee\u6807\u6837\u54c1\u6267\u884c\u4fe1\u606f\u3001\u6267\u884c\u72b6\u6001\u7b49\u5168\u6d41\u7a0b\u8bd5\u9a8c\u4fe1\u606f\n","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}}}
}
var id= 122503;
var text = new Basic_text(id,data);
})();


(function () {
  var id = 122503
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/26\/CA\/rBwBEmQ2H22AQEnHADq-Ndx9OCU555.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"summary":""}
  }
  var id = 122505
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 122505
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/30\/1F\/rBwBEmRAsaiACqZfADnyg4hE-LY381.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 633902
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 633902
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
var data = {
infor:null,
listArr:{"title":"<span style=\"font-size:20px;\"><p style=\"text-align:left;margin:0px;\"><span style=\"font-size:28px;color:#232323;\">\u6570\u636e\u53ef\u89c6\u5316<\/span><\/p><p style=\"text-align:left;margin:0px;\"><span style=\"font-size:28px;\"><br><\/span><\/p><p style=\"text-align:right;margin:0px;\"><span style=\"font-size:28px;\"><\/span><\/p><p style=\"margin:0px;\"><span style=\"font-size:18px;color:#808080;\">\u591a\u65b9\u4f4d\u3001\u591a\u89d2\u5ea6\u5c55\u73b0\u5b9e\u9a8c\u5ba4\u7684\u5404\u9879\u6307\u6807<\/span><br><span style=\"font-size:18px;color:#808080;\">\u6570\u636e\u5b9e\u65f6\u76d1\u63a7\uff0c\u4f01\u4e1a\u52a8\u6001\u4e00\u76ee\u4e86\u7136<\/span><br><span style=\"font-size:18px;color:#808080;\">\u4e2a\u6027\u5316\u5b9a\u5236\uff0c\u5e03\u5c40\u7075\u6d3b\uff0c\u6837\u5f0f\u4e30\u5bcc<\/span><\/p><br><\/span><div style=\"text-align:right;\"><\/div>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}
}
var id= 633904;
var text = new Basic_text_rich_type(id,data);
})();


(function () {
  var id = 633904
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p>\n\t<span style=\"font-weight:bold;color:#C0A063;font-size:32px;\"><span style=\"line-height:2;\"><span style=\"line-height:2.5;\"><span style=\"line-height:3;\"><\/span><\/span><\/span>\u62a5\u544a\u7ba1\u7406<\/span> \n<\/p>\n<p>\n\t<span style=\"color:#555555;font-size:16px;\"><span style=\"font-size:18px;line-height:2.5;\">\u652f\u6301\u81ea\u5b9a\u4e49\u591a\u79cd\u62a5\u544a\u6a21\u677f\u5bfc\u5165\u4e0e\u8c03\u7528\uff1b\u81ea\u52a8\u8bfb\u53d6\u5b9e\u9a8c\u4fe1\u606f\u3001\u5ba1\u6279\u4fe1\u606f\uff0c\u8bfb\u53d6\u8bbe\u5907\u53ca\u5176\u6807\u5b9a\u4fe1\u606f\uff0c\u652f\u6301\u7535\u5b50\u7b7e\u540d\u3001\u7535\u5b50\u7b7e\u7ae0\u53ca\u4e8c\u7ef4\u7801\u9632\u4f2a\u6807\u8bc6\uff0c\u9ad8\u6548\u751f\u6210\u5408\u89c4\u62a5\u544a<\/span><span style=\"line-height:2.5;font-size:18px;\">\u3002<\/span><\/span> \n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 667902

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 667902
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr:{"url":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/42\/35\/rBwBEmRYZMSAIRPKAAzdWRc0DJ8176.png","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"summary":"","lockType":"false"}
  }
  var id = 667904
  var picture = new Basic_Picture(id, data)
})();

(function () {
  var id = 667904
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p style=\"text-align:center;\">\n\t<span style=\"color:#D8D8D8;font-size:24px;\">\u2014<\/span> <span style=\"color:#326b86;font-size:28px;\"><strong>\u7cfb\u7edf<\/strong><\/span><strong><span style=\"color:#333;font-size:28px;\">\u4f18\u52bf<\/span><\/strong> <span style=\"color:#D8D8D8;font-size:24px;\">\u2014<\/span> \n<\/p>\n<p style=\"text-align:center;\">\n\t<span style=\"color:#333333;font-size:14px;\">SYSTEM ADVANTAGE<\/span> \n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 561802

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 561802
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {

    var data = {
    img_effects: {"id":1,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    mb_widgets_infor:{"img_size":{"radio_val":"custom","default":{"widthPercent":25,"heightPercent":25},"custom":{"widthPercent":25,"heightPercent":25}},"alt_display":"true","title":{"text":{"isHidden":true,"value":"\u56fe\u6807\u7ec4\u5408"},"font":{"radio_val":"custom","default":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"height":{"radio_val":"custom","default":{"height":50},"custom":{"height":50}},"background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}}},"col_num":{"radio_val":"custom","default":{"value":1},"custom":{"value":1}}},
    pc_widgets_infor:{"img_size":{"radio_val":"custom","default":{"widthPercent":24,"heightPercent":25},"custom":{"widthPercent":20,"heightPercent":25}},"defaultBackgroundColor":{"style_0":"#fff","style_1":"#fff","style_2":"#f2f2f2","style_3":"#eee"},"set_style":{"background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}}},"icon_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 181, 54,1)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 181, 54,1)","img_scale":"scale_cover","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"alt_display":"true","title":{"text":{"isHidden":true,"value":"\u56fe\u6807\u7ec4\u5408"},"font":{"radio_val":"custom","default":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"height":{"radio_val":"custom","default":{"height":50},"custom":{"height":50}},"background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}}},"page":{"radioValue":false,"pageSize":20,"pageStyle":0},"col_num":{"radio_val":"custom","default":{"value":"4"},"custom":{"value":"4"}},"padding":{"radio_val":"custom","default":{"paddingTop":"10%","paddingLeft":"10%","padding":10},"custom":{"paddingTop":"10%","paddingLeft":"10%","padding":10}},"titleFont":{"radio_val":"custom","default":{"fontSize":16,"fontWidth":"normal","fontColor":"rgba(50,50,50,1)","fontSeColor":"rgba(50,50,50,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","pattern":"pure_color","fontStyle":"","fontWeight":"normal","fontItalics":"normal","fontUnderline":"none","linear_gradient":{"start_color":"rgba(0,120,215,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fontSize":15,"fontWidth":"normal","fontColor":"rgba(50,107,134,1)","fontSeColor":"rgba(50,50,50,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","pattern":"pure_color","fontStyle":"","fontWeight":"normal","fontItalics":"normal","fontUnderline":"none","linear_gradient":{"start_color":"rgba(0,120,215,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"summaryFont":{"radio_val":"custom","default":{"fontSize":14,"fontWidth":"normal","fontColor":"rgba(153,153,153,1)","fontSeColor":"rgba(153,153,153,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","pattern":"pure_color","fontStyle":"","fontWeight":"normal","fontItalics":"normal","fontUnderline":"none","linear_gradient":{"start_color":"rgba(153,153,153,1)","end_color":"rgba(20, 20, 20,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fontSize":14,"fontWidth":"normal","fontColor":"rgba(153,153,153,1)","fontSeColor":"rgba(153,153,153,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","pattern":"pure_color","fontStyle":"","fontWeight":"normal","fontItalics":"normal","fontUnderline":"none","linear_gradient":{"start_color":"rgba(153,153,153,1)","end_color":"rgba(20, 20, 20,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"boxShadow":{"radio_val":"custom","default":{"direction":"outset","x":0,"y":0,"spread":0,"blur":15,"color":"rgba(0,0,0,0)","seColor":"rgba(0,0,0,0.1)"},"custom":{"direction":"outset","x":0,"y":0,"spread":0,"blur":15,"color":"rgba(0,0,0,0)","seColor":"rgba(0,0,0,0.1)"}}},
    pc_infor:{"marginTop":"0%","marginLeft":"0%","marginRight":"0%","marginBottom":"0%"},
    listArr:{"type":"custom","module_style":1,"custom":[{"img_type":"custom","album_id":"3j9z8o7zpva000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/33\/wKj2K2LovC-AXhPAAAC9L60Mosk362.png","summary":"\u5b9a\u5236\u624b\u6301\u7ec8\u7aef\u8bbe\u5907\uff0c\u4fdd\u8bc1\u8bd5\u9a8c\u4fe1\u606f\u80fd\u4fbf\u6377\u3001\u5b9e\u65f6\u4e0a\u4f20\u7cfb\u7edf\uff0c\u8ba9\u529e\u516c\u7a81\u7834\u7a7a\u95f4\u9650\u5236","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u79fb\u52a8\u529e\u516c"},{"img_type":"custom","album_id":"510ll9oudxw000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/33\/wKj2K2LovC-ANnYFAABJNxxsBYY128.png","summary":"\u7b80\u7ea6\u800c\u4e0d\u7b80\u5355\u7684\u754c\u9762\u8bbe\u8ba1\uff0c\u7cbe\u51c6\u4f18\u5316\u7684\u903b\u8f91\u987a\u5e8f\uff0c\u8ba9\u64cd\u4f5c\u7b80\u5355\u8f7b\u677e","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u754c\u9762\u53cb\u597d"},{"img_type":"custom","album_id":"1eti9asg49hc00","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/33\/wKj2K2LovC-AQcelAACiZATpLwE925.png","summary":"\u5f3a\u5927\u7684\u5408\u7406\u903b\u8f91\u5b58\u50a8\u53ca\u6570\u636e\u5206\u6d41\uff0c\u968f\u65f6\u56de\u987e\u603b\u7ed3\uff0c\u8ba9\u7ecf\u9a8c\u4f20\u627f","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u5168\u7a0b\u53ef\u6eaf"},{"img_type":"custom","album_id":"33v339m802u000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/33\/wKj2K2LovC-Ad25iAAAwmQNlvWE613.png","summary":"\u81ea\u5b9a\u4e49\u62a5\u544a\u6a21\u677f\u5185\u5bb9\u6a21\u5757\u5316\u3001\u7075\u6d3b\u5316\uff0c\u8ba9\u62a5\u544a\u4e5f\u80fd\u8d70\u4e0a\u81ea\u52a8\u5316\u9ad8\u6027\u80fd\u6d41\u6c34\u7ebf","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u9ad8\u6548\u62a5\u544a"},{"img_type":"custom","album_id":"52ezuwkkau8000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/33\/wKj2K2LovC-AQLbtAABddvNyeR0895.png","summary":"\u552f\u4e00\u6027\u4e8c\u7ef4\u7801\u7ed3\u5408\u626b\u7801\u6280\u672f\uff0c\u8ba9\u6837\u54c1\u7684\u5373\u65f6\u4f4d\u7f6e\u3001\u72b6\u6001\u4e86\u7136\u4e8e\u5fc3","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u6837\u54c1\u4e8c\u7ef4\u7801"},{"img_type":"custom","album_id":"3dy2rkbx1yw000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/34\/wKj2K2LovC-AWZbuAACsDP2XP14511.png","summary":"\u591a\u6837\u6027\u7684\u6570\u636e\u91c7\u96c6\u65b9\u5f0f\uff0c\u6027\u80fd\u4f18\u5f02\u7684\u5de5\u63a7\u8f6f\u4ef6\uff0c\u8ba9\u60a8\u62e5\u6709\u4e00\u53cc\u9ad8\u79d1\u6301\u5343\u91cc\u773c","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u7cbe\u51c6\u7269\u8054"},{"img_type":"custom","album_id":"1h9q7j9m6mdc00","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/34\/wKj2K2LovDCAX8bwAABCvtxKYv0020.png","summary":"\u79d1\u5b66\u9ad8\u7ea7\u7684\u7b97\u6cd5\u690d\u5165\u8ba9\u6392\u671f\u9ad8\u6548\u667a\u80fd\uff0c\u540c\u65f6\u53c8\u5f88\u8f7b\u677e\u9ad8\u6548","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u667a\u80fd\u6392\u671f"},{"img_type":"custom","album_id":"3opuaewmhj2000","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/34\/wKj2K2LovDCABqKCAABviKmXQEA847.png","summary":"\u9884\u7559\u5408\u7406\u7684\u5f00\u53d1\u7aef\u53e3\u4fbf\u4e8e\u4e8c\u6b21\u5f00\u53d1\uff0c\u8ba9\u5176\u4e0e\u60a8\u7684\u4f01\u4e1a\u5171\u540c\u6210\u957f\uff0c\u6536\u653e\u81ea\u5982","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":"","title":"\u9ad8\u6269\u5c55\u6027"}],"atlas_id":0,"iconAlign":"center"},
    }

    var id= 537402;

    var icon = new Basic_icon_combination(id,data)

})();

(function () {
  var id = 537402
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
  var data = {
    img_effects: {"id":0,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
    listArr: {"moduleStyle":2,"richText":"<p style=\"text-align:center;\">\n\t<span style=\"color:#D8D8D8;font-size:24px;\">\u2014<\/span> <span style=\"color:#326b86;font-size:28px;\"><strong>\u5e94\u7528<\/strong><\/span><strong><span style=\"color:#333;font-size:28px;\">\u573a\u666f<\/span><\/strong> <span style=\"color:#D8D8D8;font-size:24px;\">\u2014<\/span> \n<\/p>\n<p style=\"text-align:center;\">\n\t<span style=\"color:#333333;font-size:14px;\">APPLICATION SCENARIOS<\/span> \n<\/p>","link":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}}},"outside_link":{"url":""}},"imgURL":"https:\/\/staticjs.71360.com\/3\/static\/txys-001\/image\/video.jpg","desc":""}
  }

  var id = 667102

  var rich_text = new Basic_Rich_Text(id, data);
})();

(function () {
  var id = 667102
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {
    var data = {
        img_effects: {"id":1,"formData":{"effect":0,"border":{"radio_val":"custom","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"custom","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 0)"}},"direction":"left"}},
        mb_widgets_infor:{"alt_display":"true","title":{"text":{"isHidden":true,"value":"\u5217\u8868\u591a\u56fe"},"font":{"radio_val":"custom","default":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"height":{"radio_val":"custom","default":{"height":50},"custom":{"height":50}},"background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}}},"col_num":{"radio_val":"custom","default":{"value":2},"custom":{"value":2}}},
        pc_widgets_infor:{"img_size":{"radio_val":"custom","default":{"width":"100%","height":"90%"},"custom":{"width":"100%","height":"64.7%"}},"backgroundSize":{"radio_val":"custom","default":{"backgroundSize":"cover"},"custom":{"backgroundSize":"cover"}},"alt_display":"true","title":{"text":{"isHidden":true,"value":"\u5217\u8868\u591a\u56fe"},"font":{"radio_val":"custom","default":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"fsItalics":"normal","fontUnderline":"none","fontSize":16,"fsWeight":"normal","fontColor_globalColor":"rgba(50,107,134,1)","fontFamily":"\u5fae\u8f6f\u96c5\u9ed1","textAlign":"left","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}},"height":{"radio_val":"custom","default":{"height":50},"custom":{"height":50}},"background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}}},"page":{"radioValue":false,"pageSize":20,"pageStyle":0},"col_num":{"radio_val":"custom","default":{"value":"4"},"custom":{"value":3}},"padding":{"radio_val":"custom","default":{"paddingTop":10,"paddingLeft":10},"custom":{"paddingTop":10,"paddingLeft":10}},"slides_group":{"radio_val":"custom","default":{"value":"1"},"custom":{"value":"1"}},"swiper_loop":"false","banner_set":{"radio_val":"custom","default":{"speed":0.5,"delay":2},"custom":{"speed":0.5,"delay":2}},"boxShadow":{"radio_val":"custom","default":{"direction":"outset","x":0,"y":0,"spread":0,"blur":15,"color":"rgba(0,0,0,0)","seColor":"rgba(0,0,0,0.1)"},"custom":{"direction":"outset","x":0,"y":0,"spread":0,"blur":15,"color":"rgba(0,0,0,0)","seColor":"rgba(0,0,0,0.1)"}},"pageColor":{"radio_val":"custom","default":{"color":"rgba(94,96,100,1)"},"custom":{"color":"rgba(94,96,100,1)"}}},
        listArr:{"type":"custom","module_style":"0","custom":[{"img_type":"custom","album_id":"2ebaw28mo5hc00","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/3D\/wKj2K2LowFGABKe0AAnR8dUlZxo891.png","summary":"PDA\u626b\u63cf\u6280\u672f \u6837\u54c1\u7684\u7a7a\u95f4\u4f4d\u7f6e\u53ca\u72b6\u6001 \u5373\u65f6\u5448\u73b0","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":""},{"img_type":"custom","album_id":"qm06sd44gu800","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/3D\/wKj2K2LowFGAQ7QNAAMx76C8pbE150.png","summary":"\u591a\u7ec8\u7aef\u652f\u6301 \u968f\u65f6\u968f\u5730 \u79fb\u52a8\u529e\u516c","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":""},{"img_type":"custom","album_id":"nnkb0hdduu800","img_name":"","img_path":"https:\/\/img01.71360.com\/file\/read\/www2\/M00\/14\/3D\/wKj2K2LowFGAXbb0AAaXxB7VFE0360.png","summary":"\u8fdc\u7a0b\u76d1\u63a7 \u8ba9\u5173\u952e\u5b9e\u9a8c\u968f\u65f6\u638c\u63e1","intro":{"select_val_preview":"","linkType":"inside_link","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]},"svideo":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"cur_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}},"is_jump":0,"jump_url":""}],"atlas_id":0},
    }
    
    
    var id= 977102;
    
    var banner = new Basic_list_photos(id,data);
})();

(function () {
  var id = 977102
  var data = {
    animateObj: {"value":0,"animate":{"_key":"none"}},
    animateType: "module_animation",
    animateCombObj: {"value":0,"animate":{"_key":"none"}}
  }
  new Basic_module(id, data)
})();(function () {

window.globalAnimate = fixed
var window_resize = new Window_resize();
var fixed = new Scroll_init({"vip_type":"default","vip_type_footer":"default","theme_color":"rgba(50,107,134,1)","previewModule":"this","header_display":"true","footer_display":"true","body_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"default","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"footer_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"createrMessage":{"createrShow":"true","loginShow":"true"},"fixed_header":"true","fixed_height":"system","fixed_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"fixed_back_top":{"button_style":"default","show_back_top":true,"pcShowBtn":true,"mbShowBtn":true,"button_color":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(50,107,134,1)","iconName":"el-icon-arrow-up","color":"#fff"},"custom":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"customColor":"rgba(0, 0, 0, 0.1)"},"setting":{"title":"\u4e0a\u6d77\u607a\u851a\u79d1\u6280\u6709\u9650\u516c\u53f8","logoTitleRadio":true,"headerNavRadio":true,"returnTopRadio":true,"multiLanguageOpen":false,"footnavOpen":false,"zhLanguageOpen":false,"zhLanguageAdress":"","loginRegister":false},"mb_header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"mb_header_btn_background":{"radio_val":"custom","default":{"color":"rgba(0, 0, 0,1)"},"custom":{"color":"rgba(0, 0, 0,1)"}},"displayContactRadio":"block","theme_color_type":"custom_5gmquce5c6aups","mb_header_nav_position":"left","mb_header_nav_title":{"radio_val":"custom","default":{"fontSize":18},"custom":{"fontSize":18}},"contact_us":{"contact_us_show":false,"contact_us_set":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(55, 85, 239, 1)","iconPosition":"-0 -0"},"customSet":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"listArr":{"link":{"select_val_preview":"","linkType":"fun","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"other_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}},"icp_isShow":"false","adaptWidth":{"isOpen":"true","radioVal":"custom","val":1460,"customVal":1440},"nav_min_width":{"radio_val":"custom","default":{"val":768},"custom":{"val":768}},"custom_theme_color_list":[{"key":"custom_5gmquce5c6aups","color":"rgba(50,107,134,1)"}],"custom_nav_color_list":[],"header_layout_row_setting":"xuanfu","xuanfu_position":"station"});
var common = {"vip_type":"default","vip_type_footer":"default","theme_color":"rgba(50,107,134,1)","previewModule":"this","header_display":"true","footer_display":"true","body_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"default","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"footer_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"createrMessage":{"createrShow":"true","loginShow":"true"},"fixed_header":"true","fixed_height":"system","fixed_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(0,0,0,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(71,127,152,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"fixed_back_top":{"button_style":"default","show_back_top":true,"pcShowBtn":true,"mbShowBtn":true,"button_color":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(50,107,134,1)","iconName":"el-icon-arrow-up","color":"#fff"},"custom":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"customColor":"rgba(0, 0, 0, 0.1)"},"setting":{"title":"\u4e0a\u6d77\u607a\u851a\u79d1\u6280\u6709\u9650\u516c\u53f8","logoTitleRadio":true,"headerNavRadio":true,"returnTopRadio":true,"multiLanguageOpen":false,"footnavOpen":false,"zhLanguageOpen":false,"zhLanguageAdress":"","loginRegister":false},"mb_header_background":{"radio_val":"custom","default":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255,1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"cover","background_repeat":"no-repeat","background_position":"center center"}},"mb_header_btn_background":{"radio_val":"custom","default":{"color":"rgba(0, 0, 0,1)"},"custom":{"color":"rgba(0, 0, 0,1)"}},"displayContactRadio":"block","theme_color_type":"custom_5gmquce5c6aups","mb_header_nav_position":"left","mb_header_nav_title":{"radio_val":"custom","default":{"fontSize":18},"custom":{"fontSize":18}},"contact_us":{"contact_us_show":false,"contact_us_set":{"radio_val":"default","default":{"id":"default-0","borderRadius":"50%","canChangeColor":true,"backgroundColor":"rgba(55, 85, 239, 1)","iconPosition":"-0 -0"},"customSet":{"url":"https:\/\/preapiconsole.71360.com\/file\/read\/www\/M00\/01\/60\/wKgBbF5F-HSAaMfQAAArKq7c-fY663.jpg"}},"listArr":{"link":{"select_val_preview":"","linkType":"fun","isOpen":[],"none":[],"inside_link":{"type":"section","section":{"value":[]},"product":{"value":[]},"article":{"value":[]},"case":{"value":[]},"help":{"value":[]},"product_type":{"value":[]},"article_type":{"value":[]},"case_type":{"value":[]},"help_type":{"value":[]}},"fun":{"type":"submit_form","submit_form":{"value":""},"customer_service":{"value":""},"telephone_advisory":{"value":""},"information_advisory":{"value":""},"file_download":{"url":"","name":""},"jump_section":{"type":"other_page","cur_page_value":"","other_page_value":"","site_section":[]},"pop_video":{"type":"local_add","local_add":{"url":"","name":""},"common_code":{"url":""},"video_address":{"url":""}},"send_email":{"recipient":"","subject":"","content":""}},"outside_link":{"url":""}}}},"icp_isShow":"false","adaptWidth":{"isOpen":"true","radioVal":"custom","val":1460,"customVal":1440},"nav_min_width":{"radio_val":"custom","default":{"val":768},"custom":{"val":768}},"custom_theme_color_list":[{"key":"custom_5gmquce5c6aups","color":"rgba(50,107,134,1)"}],"custom_nav_color_list":[],"header_layout_row_setting":"xuanfu","xuanfu_position":"station"}
// if (!common.header_display) {
// document.getElementsByClassName('layout_header')[0].style.display = 'none';
// document.getElementsByClassName('rowFixedNext')[0].style.display = 'none';
// }
new Back_top(common);

new Contact_us(common.contact_us);
var scrollType = window.scrollType;
var privateData = {"banner_isShow":true,"banner_type":"private","fullPageTopNavType":"zhanwei","fullPageStyleType":"0","fullPageFooterType":"typeRow1","floatingFrameOpen":false,"floatingFrameStyle":{"data":{"img_effects":{"id":0,"formData":{"effect":0,"border":{"radio_val":"default","default":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"},"custom":{"borderWidth":2,"borderColor":"rgba(0, 0, 0, 1)"}},"background":{"radio_val":"default","default":{"color":"rgba(255, 255, 255, 0)"},"custom":{"color":"rgba(255, 255, 255, 1)"}},"direction":"left"}},"pc_widgets_infor":{"width":{"radio_val":"default","default":{"width":200},"custom":{"width":200}},"height":{"radio_val":"default","default":{"height":200},"custom":{"height":200}},"background":{"radio_val":"default","default":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"},"custom":{"type":"color","img":"","color":"rgba(255, 255, 255, 1)","img_scale":"scale_tiling","tile_effect":"img_default","background_size":"auto","background_repeat":"repeat","background_position":"center center"}},"boxShadow":{"radio_val":"default","default":{"direction":"outset","x":0,"y":0,"spread":0,"blur":0,"color":"rgba(0,0,0,0.3)","seColor":"rgba(0,0,0,0.5)"},"custom":{"direction":"outset","x":0,"y":0,"spread":0,"blur":4,"color":"rgba(0,0,0,0.3)","seColor":"rgba(0,0,0,0.5)"}},"position":"center_center","trigger":"one","playSet":{"delay":1,"space":1}},"mb_widgets_infor":[],"listArr":{"content":"\u8bf7\u8f93\u5165\u5185\u5bb9"}},"name":"basic_floating_frame"},"fixed_background_isOpen":"true","H1_isOpen":false,"ceilingAdvertisingOpen":false,"ceilingAdvertisingStyle":{"data":{"pc_widgets_infor":{"position":"footer","img_url":"","close_img":"https:\/\/sitecdn.71360.com\/static\/txys-001\/image\/ceiling_advertising\/default.png","botton":{"type":"system","img_id":0},"background":{"radio_val":"default","default":{"color":"rgba(127, 127, 127,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"seColor":"rgba(239,65,54,1)","sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}},"custom":{"color":"rgba(127, 127, 127,1)","pattern":"pure_color","linear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90},"seColor":"rgba(100, 100, 100,1)","sePattern":"pure_color","seLinear_gradient":{"start_color":"rgba(156,40,177,1)","end_color":"rgba(0, 0, 0,1)","start_pointer":12,"end_pointer":100,"deg":90}}}},"mb_widgets_infor":[]},"name":"basic_ceiling_advertising"}};
var data = {
    scrollType: scrollType,
    private: privateData,
    common: common
}
is_mobile()!=='mb' && initFullPage()
window.initFullPage = initFullPage

function initFullPage(){
    if(scrollType === 'row' || scrollType === 'col'){
        var fullPage = new FullPage({
            data: data,
            el: '.mb_body_view',                            // selector of contain
            slideTime: 600,                               // time of slide
            continuous: false,                            // create an infinite feel with no endpoints
            mode: 'wheel,nav:full-page-nav',               // mode of fullpage
            start: 0,                                     // which page will display when install
            easing: '',                                // easing('ease','ease-in','ease-in-out' or use cubic-bezier like [.33, 1.81, 1, 1];
            onTransitionStart: function(index, thisPage){
                fixed.make_animate()
            },
            callback: function (index, thisPage) {       // callback when pageChange
                var backTopVisible = index > 0 && is_mobile() === 'pc'
                backTopVisible ? $('.back_top_wrapper').show() : $('.back_top_wrapper').hide()
                fixed.countup_start()
            }
        });
        setTimeout(function(){
            fixed.make_animate()
        },100)
        window.fullPage = fullPage
        return fullPage
    }
}




})();