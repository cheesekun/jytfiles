﻿var dataYearList=[];
var data1={};
$(function () {
    getDataYear()
    
})
function selectDataYear(data_year){
    for(var i=0;i<dataYearList.length;i++){
        if(dataYearList[i].data_year==data_year){
            $('#'+data_year).addClass('t_active')
        }else{
            $('#'+dataYearList[i].data_year).removeClass('t_active')
        }
    }
    $('#data_title').html('当前数据年份'+data_year)
    map(data_year);
    echarts_1(data_year);
    echarts_2(data_year);
    echarts_4(data_year);
    echarts_5(data_year);
    echarts_6(data_year);
    echarts_31(data_year);
    echarts_32(data_year);
    echarts_33(data_year);
}
function toObject(key,val,val1){
    data1[key]=[val,val1]; 
    return data1;
 }
 function map(data_year) {
     $('#map_title').html(data_year+'年中国地级市碳排放')
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('map_1'));
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_carbon_emission",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var data=[]
                var result={}
                if(res.result.data.records.length>0){
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        data.push({
                            name:res.result.data.records[i].city_name,
                            value:res.result.data.records[i].co2
                        })
                        result=toObject(res.result.data.records[i].city_name,res.result.data.records[i].lng,res.result.data.records[i].lat)
                    }
                    var geoCoordMap = result
                    var convertData = function (data) {
                        var res = [];
                        for (var i = 0; i < data.length; i++) {
                            var geoCoord = geoCoordMap[data[i].name];
                            if (geoCoord) {
                                res.push({
                                    name: data[i].name,
                                    value: geoCoord.concat(data[i].value)
                                });
                            }
                        }
                        return res;
                    };
            
                    option = {
                        tooltip: {
                            trigger: 'item',
                            formatter: function (params) {
                                if (typeof (params.value)[2] == "undefined") {
                                    return params.name + ' : ' + params.value+'（百万吨）';
                                } else {
                                    return params.name + ' : ' + params.value[2]+'（百万吨）';
                                }
                            }
                        },
            
                        geo: {
                            map: 'china',
                            label: {
                                emphasis: {
                                    show: false
                                }
                            },
                            roam: false,//禁止其放大缩小
                            itemStyle: {
                                normal: {
                                    areaColor: '#4c60ff',
                                    borderColor: '#002097'
                                },
                                emphasis: {
                                    areaColor: '#293fff'
                                }
                            }
                        },
                        series: [
                            {
                                name: '碳排放量',
                                type: 'scatter',
                                coordinateSystem: 'geo',
                                data: convertData(data),
                                symbolSize: function (val) {
                                    return val[2] / 8;
                                },
                                label: {
                                    normal: {
                                        formatter: '{b}',
                                        position: 'right',
                                        show: false
                                    },
                                    emphasis: {
                                        show: true
                                    }
                                },
                                itemStyle: {
                                    normal: {
                                        color: '#ffeb7b'
                                    }
                                }
                            }
                        ]
                    };
            
                    myChart.setOption(option);
                    window.addEventListener("resize", function () {
                        myChart.resize();
                    });
                }
            }
        }
    })
}
function getDataYear(){
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_data_year",
        data: {getAll:"true"},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var html=''
                dataYearList=res.result.data.records
                if(res.result.data.records.length>0){
                    $('#data_title').html('当前数据年份'+res.result.data.records[0].data_year)
                    map(res.result.data.records[0].data_year);
                    echarts_1(res.result.data.records[0].data_year);
                    echarts_2(res.result.data.records[0].data_year);
                    echarts_4(res.result.data.records[0].data_year);
                    echarts_5(res.result.data.records[0].data_year);
                    echarts_6(res.result.data.records[0].data_year);
                    echarts_31(res.result.data.records[0].data_year);
                    echarts_32(res.result.data.records[0].data_year);
                    echarts_33(res.result.data.records[0].data_year);
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        if(i==0){
                            html+='<li style="width: 62px;" class="t_active" id="'+res.result.data.records[i].data_year+'" onclick="selectDataYear('+res.result.data.records[i].data_year+')">'+res.result.data.records[i].data_year+'</li>'
                        } else{
                            html+='<li style="width: 62px;" id="'+res.result.data.records[i].data_year+'" onclick="selectDataYear('+res.result.data.records[i].data_year+')">'+res.result.data.records[i].data_year+'</li>'
                        }
                    }
                    $('#data_year_list').html(html)
                }
            }
        }
    })
}
function echarts_1(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_pm",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var xAxisData=[]
                var data=[]
                if(res.result.data.records.length>0){
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        xAxisData.push(res.result.data.records[i].city_name)
                        data.push(res.result.data.records[i].pm)
                    }
                }
                 // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart1'));
                option = {
                    //  backgroundColor: '#00265f',
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        },
                    },
                    grid: {
                        left: '3%',
                        top: '28px',
                        right: '0%',
                        bottom: '7%',
                        containLabel: true
                    },
                    xAxis: [{
                        type: 'category',
                        data: xAxisData,
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "rgba(255,255,255,.1)",
                                width: 1,
                                type: "solid"
                            },
                        },
        
                        axisTick: {
                            show: false,
                        },
                        axisLabel: {
                            interval: 0,
                            rotate: 50,
                            show: true,
                            splitNumber: 15,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                    }],
                    yAxis: [{
                        name: '微克每立方米',
                        nameTextStyle: {
                            color: "#fff",
                            fontSize: 12,
                        },
        
                        type: 'value',
                        axisLabel: {
                            //formatter: '{value} %'
                            show: true,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                        axisTick: {
                            show: false,
                        },
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "rgba(255,255,255,.1	)",
                                width: 1,
                                type: "solid"
                            },
                        },
                        splitLine: {
                            lineStyle: {
                                color: "rgba(255,255,255,.1)",
                            }
                        }
                    }],
                    "dataZoom": [{
                        "show": true,
                        "height": 12,
                        "xAxisIndex": [
                            0
                        ],
                        bottom: '0',
                        "start": 0,
                        "end": 5,
                        handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
                        handleSize: '110%',
                        handleStyle: {
                            color: "#d3dee5",
        
                        },
                        textStyle: {
                            color: "#fff"
                        },
                        borderColor: "#90979c"
                    }, {
                        "type": "inside",
                        "show": true,
                        "height": 15,
                        "start": 1,
                        "end": 35
                    }],
                    series: [
                        {
                            type: 'bar',
                            data: data,
                            barWidth: '35%', //柱子宽度
                            barGap: 1, //柱子之间间距
                            itemStyle: {
                                normal: {
                                    color: '#2f89cf',
                                    opacity: 1,
                                    barBorderRadius: 5,
                                }
                            }
                        }
        
                    ]
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
}
function echarts_2(data_year ) {
    // 基于准备好的dom，初始化echarts实例
    

    // option = {
    //     //  backgroundColor: '#00265f',
    //     tooltip: {
    //         trigger: 'axis',
    //         axisPointer: { type: 'shadow' },
    //         formatter: "{b}: {c} (%)",
    //     },
    //     grid: {
    //         left: '3%',
    //         top: '28px',
    //         right: '0%',
    //         bottom: '7%',
    //         containLabel: true
    //     },
    //     xAxis: [{
    //         type: 'category',
    //         data:  ['加格达奇','黑河','海拉尔','齐齐哈尔','伊春','鹤岗','乌兰浩特','绥化','佳木斯','双鸭山','白城','松原','哈尔滨','鸡西','阿勒泰','塔城','博乐','克拉玛依'],
    //         axisLine: {
    //             show: true,
    //             lineStyle: {
    //                 color: "rgba(255,255,255,.1)",
    //                 width: 1,
    //                 type: "solid"
    //             },
    //         },

    //         axisTick: {
    //             show: false,
    //         },
    //         axisLabel: {
    //             interval: 0,
    //             rotate:50,
    //             show: true,
    //             splitNumber: 15,
    //             textStyle: {
    //                 color: "rgba(255,255,255,.6)",
    //                 fontSize: '12',
    //             },
    //         },
    //     }],
    //     yAxis: [{
    //         name:'%',
    //         nameTextStyle:{
    //             color:"#fff", 
    //             fontSize:12,  
    //         },

    //         type: 'value',
    //         axisLabel: {
    //             // formatter: '{value} %',
    //             show: true,
    //             textStyle: {
    //                 color: "rgba(255,255,255,.6)",
    //                 fontSize: '12',
    //             },
    //         },
    //         axisTick: {
    //             show: false,
    //         },
    //         axisLine: {
    //             show: true,
    //             lineStyle: {
    //                 color: "rgba(255,255,255,.1	)",
    //                 width: 1,
    //                 type: "solid"
    //             },
    //         },
    //         splitLine: {
    //             lineStyle: {
    //                 color: "rgba(255,255,255,.1)",
    //             }
    //         }
    //     }],
    //     "dataZoom": [{
    //         "show": true,
    //         "height": 12,
    //         "xAxisIndex": [
    //             0
    //         ],
    //         bottom: '0',
    //         "start": 10,
    //         "end": 90,
    //         handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
    //         handleSize: '110%',
    //         handleStyle: {
    //             color: "#d3dee5",

    //         },
    //         textStyle: {
    //             color: "#fff"
    //         },
    //         borderColor: "#90979c"
    //     }, {
    //         "type": "inside",
    //         "show": true,
    //         "height": 15,
    //         "start": 1,
    //         "end": 35
    //     }],
    //     series: [
    //         {

    //             type: 'bar',
    //             data:[25.59, 41.79, 37.27, 33.48, 26.45, 46.59, 46.31,30.66,9.17,6.61,45.37,45.62, 37.27, 33.48, 26.45, 46.59, 37.27, 33.48 ],
    //             barWidth: '35%', //柱子宽度
    //             // barGap: 1, //柱子之间间距
    //             itemStyle: {
    //                 normal: {
    //                     color: '#27d08a',
    //                     opacity: 1,
    //                     barBorderRadius: 5,
    //                 }
    //             }
    //         }

    //     ]
    // };

    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_green_coverage",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var xAxisData=[]
                var data=[]
                if(res.result.data.records.length>0){
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        xAxisData.push(res.result.data.records[i].city_name)
                        data.push(res.result.data.records[i].green_coverage)
                    }
                }
                var myChart = echarts.init(document.getElementById('echart2'));
                option = {
                    color: ['#27d08a'],
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            lineStyle: {
                                color: '#27d08a'
                            }
                        }
                    },
                    grid: {
                        left: '10',
                        top: '30',
                        right: '10',
                        bottom: '20',
                        containLabel: true
                    },
        
                    xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        axisLabel: {
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: 12,
                            },
                        },
                        axisLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.2)'
                            }
        
                        },
        
                        data: xAxisData
        
                    }, {
        
                        axisPointer: { show: false },
                        axisLine: { show: false },
                        position: 'bottom',
                        offset: 20,
                    }],
        
                    yAxis: [{
                        name: '%',
                        nameTextStyle: {
                            color: "#fff",
                            fontSize: 12,
                        },
                        type: 'value',
                        axisTick: { show: false },
                        axisLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.1)'
                            }
                        },
                        axisLabel: {
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: 12,
                            },
                        },
                        splitLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.1)'
                            }
                        }
                    }],
                    "dataZoom": [{
                        "show": true,
                        "height": 12,
                        "xAxisIndex": [
                            0
                        ],
                        bottom: '0',
                        "start": 0,
                        "end": 9,
                        handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
                        handleSize: '110%',
                        handleStyle: {
                            color: "#d3dee5",
        
                        },
                        textStyle: {
                            color: "#fff"
                        },
                        borderColor: "#90979c"
                    }, {
                        "type": "inside",
                        "show": true,
                        "height": 15,
                        "start": 1,
                        "end": 35
                    }],
                    series: [
                        {
                            name: '绿化覆盖率',
                            type: 'line',
                            smooth: true,
                            symbol: 'circle',
                            symbolSize: 5,
                            showSymbol: false,
                            lineStyle: {
        
                                normal: {
                                    color: '#27d08a',
                                    width: 2
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                        offset: 0,
                                        color: 'rgba(39, 208, 138, 0.4)'
                                    }, {
                                        offset: 0.8,
                                        color: 'rgba(39, 208, 138, 0.1)'
                                    }], false),
                                    shadowColor: 'rgba(0, 0, 0, 0.1)',
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: '#27d08a',
                                    borderColor: 'rgba(221, 220, 107, .1)',
                                    borderWidth: 12
                                }
                            },
                            data: data
                        }
                    ]
        
                };
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
    
}
function echarts_5(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_weather_data",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var xAxisData=[]
                var data=[]
                if(res.result.data.records.length>0){
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        xAxisData.push(res.result.data.records[i].city_name)
                        data.push(res.result.data.records[i].sunshine_hours)
                    }
                }
                // 基于准备好的dom，初始化echarts实例
                 var myChart = echarts.init(document.getElementById('echart5'));
                 option = {
                    //  backgroundColor: '#00265f',
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },
        
                    grid: {
                        left: '3%',
                        top: '28px',
                        right: '0%',
                        bottom: '7%',
                        containLabel: true
                    },
                    xAxis: [{
                        type: 'category',
                        data: xAxisData,
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "rgba(255,255,255,.1)",
                                width: 1,
                                type: "solid"
                            },
                        },
        
                        axisTick: {
                            show: false,
                        },
                        axisLabel: {
                            interval: 0,
                            rotate: 50,
                            show: true,
                            splitNumber: 15,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                    }],
                    yAxis: [{
                        name: '时',
                        nameTextStyle: {
                            color: "#fff",
                            fontSize: 12,
                        },
        
                        type: 'value',
                        axisLabel: {
                            //formatter: '{value} %'
                            show: true,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                        axisTick: {
                            show: false,
                        },
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "rgba(255,255,255,.1	)",
                                width: 1,
                                type: "solid"
                            },
                        },
                        splitLine: {
                            lineStyle: {
                                color: "rgba(255,255,255,.1)",
                            }
                        }
                    }],
                    "dataZoom": [{
                        "show": true,
                        "height": 12,
                        "xAxisIndex": [
                            0
                        ],
                        bottom: '0',
                        "start": 0,
                        "end": 5,
                        handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
                        handleSize: '110%',
                        handleStyle: {
                            color: "#d3dee5",
        
                        },
                        textStyle: {
                            color: "#fff"
                        },
                        borderColor: "#90979c"
                    }, {
                        "type": "inside",
                        "show": true,
                        "height": 15,
                        "start": 1,
                        "end": 35
                    }],
                    series: [{
                        type: 'bar',
                        data: data,
                        barWidth: '35%', //柱子宽度
                        // barGap: 1, //柱子之间间距
                        itemStyle: {
                            normal: {
                                color: '#2f89cf',
                                opacity: 1,
                                barBorderRadius: 5,
                            }
                        }
                    }
                    ]
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
}

function echarts_4(data_year) {
    // 基于准备好的dom，初始化echarts实例
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_weather_data",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var xAxisData=[]
                var data=[]
                if(res.result.data.records.length>0){
                    for (var i = 0; i < res.result.data.records.length; i++) {   
                        xAxisData.push(res.result.data.records[i].city_name)
                        data.push(res.result.data.records[i].average_temperature)
                    }
                }
                var myChart = echarts.init(document.getElementById('echart4'));
                option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            lineStyle: {
                                color: '#dddc6b'
                            }
                        }
                    },
                    // legend: {
                    //     top: '0%',
                    //     data: ['平均气温'],
                    //     textStyle: {
                    //         color: 'rgba(255,255,255,.5)',
                    //         fontSize: '12',
                    //     }
                    // },
                    grid: {
                        left: '10',
                        top: '30',
                        right: '10',
                        bottom: '20',
                        containLabel: true
                    },
        
                    xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        axisLabel: {
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: 12,
                            },
                        },
                        axisLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.2)'
                            }
        
                        },
        
                        data:xAxisData
        
                    }, {
        
                        axisPointer: { show: false },
                        axisLine: { show: false },
                        position: 'bottom',
                        offset: 20,
        
        
        
                    }],
        
                    yAxis: [{
                        name: '℃',
                        nameTextStyle: {
                            color: "#fff",
                            fontSize: 12,
                        },
                        type: 'value',
                        axisTick: { show: false },
                        axisLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.1)'
                            }
                        },
                        axisLabel: {
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: 12,
                            },
                        },
                        splitLine: {
                            lineStyle: {
                                color: 'rgba(255,255,255,.1)'
                            }
                        }
                    }],
                    "dataZoom": [{
                        "show": true,
                        "height": 12,
                        "xAxisIndex": [
                            0
                        ],
                        bottom: '0',
                        "start": 0,
                        "end": 9,
                        handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
                        handleSize: '110%',
                        handleStyle: {
                            color: "#d3dee5",
        
                        },
                        textStyle: {
                            color: "#fff"
                        },
                        borderColor: "#90979c"
                    }, {
                        "type": "inside",
                        "show": true,
                        "height": 15,
                        "start": 1,
                        "end": 35
                    }],
                    series: [
                        {
                            name: '平均气温',
                            type: 'line',
                            smooth: true,
                            symbol: 'circle',
                            symbolSize: 5,
                            showSymbol: false,
                            lineStyle: {
        
                                normal: {
                                    color: '#0184d5',
                                    width: 2
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                        offset: 0,
                                        color: 'rgba(1, 132, 213, 0.4)'
                                    }, {
                                        offset: 0.8,
                                        color: 'rgba(1, 132, 213, 0.1)'
                                    }], false),
                                    shadowColor: 'rgba(0, 0, 0, 0.1)',
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: '#0184d5',
                                    borderColor: 'rgba(221, 220, 107, .1)',
                                    borderWidth: 12
                                }
                            },
                            data:data
        
                        }
                    ]
        
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
}
function echarts_6(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_environmental_indicators",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var data1=[]
                var data2=[]
                var data3=[]
                if(res.result.data.records.length>0){
                    data1.push(
                        {
                            value: res.result.data.records[0].solid_waste,
                            name: '01'
                        }, {
                            value: 100-res.result.data.records[0].solid_waste,
                            name: 'invisible',
                            tooltip: { show: false },
                            itemStyle:  {
                                normal: {
                                    color: 'rgba(255,255,255,.05)',
                                    label: { show: false, },
                                    labelLine: { show: false }
                                },
                                emphasis: {
                                    color: 'rgba(0,0,0,0)'
                                }
                            }
                        }
                    )
                    data2.push(
                        {
                            value: res.result.data.records[0].urban_domestic_sewage,
                            name: '01'
                        }, {
                            value: 100-res.result.data.records[0].urban_domestic_sewage,
                            name: 'invisible',
                            tooltip: { show: false },
                            itemStyle:  {
                                normal: {
                                    color: 'rgba(255,255,255,.05)',
                                    label: { show: false, },
                                    labelLine: { show: false }
                                },
                                emphasis: {
                                    color: 'rgba(0,0,0,0)'
                                }
                            }
                        }
                    )
                    data3.push(
                        {
                            value: res.result.data.records[0].domestic_garbage,
                            name: '01'
                        }, {
                            value: 100-res.result.data.records[0].domestic_garbage,
                            name: 'invisible',
                            tooltip: { show: false },
                            itemStyle:  {
                                normal: {
                                    color: 'rgba(255,255,255,.05)',
                                    label: { show: false, },
                                    labelLine: { show: false }
                                },
                                emphasis: {
                                    color: 'rgba(0,0,0,0)'
                                }
                            }
                        }
                    )
                }
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart6'));
                var dataStyle = {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        },
                        //shadowBlur: 40,
                        //shadowColor: 'rgba(40, 40, 40, 1)',
                    }
                };
                var placeHolderStyle = {
                    normal: {
                        color: 'rgba(255,255,255,.05)',
                        label: { show: false, },
                        labelLine: { show: false }
                    },
                    emphasis: {
                        color: 'rgba(0,0,0,0)'
                    }
                };
                option = {
                    color: ['#0f63d6', '#0f78d6', '#0f8cd6', '#0fa0d6', '#0fb4d6'],
                    tooltip: {
                        show: true,
                        formatter: "{a} : {c} "
                    },
                    legend: {
                        itemWidth: 10,
                        itemHeight: 10,
                        itemGap: 12,
                        bottom: '3%',
        
                        data: ['工业固体废物综合利用率', '城镇生活污水处理率', '生活垃圾无害化处理率'],
                        textStyle: {
                            color: 'rgba(255,255,255,.6)',
                        }
                    },
        
                    series: [
                        {
                            name: '工业固体废物综合利用率',
                            type: 'pie',
                            clockWise: false,
                            center: ['50%', '42%'],
                            radius: ['59%', '70%'],
                            itemStyle: dataStyle,
                            hoverAnimation: false,
                            data: data1
                        },
                        {
                            name: '城镇生活污水处理率',
                            type: 'pie',
                            clockWise: false,
                            center: ['50%', '42%'],
                            radius: ['49%', '60%'],
                            itemStyle: dataStyle,
                            hoverAnimation: false,
                            data:data2
                        },
                        {
                            name: '生活垃圾无害化处理率',
                            type: 'pie',
                            clockWise: false,
                            hoverAnimation: false,
                            center: ['50%', '42%'],
                            radius: ['39%', '50%'],
                            itemStyle: dataStyle,
                            data: data3
                        },
                    ]
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })        
}
function echarts_31(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_environmental_indicators",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var data1=0
                var data2=0
                var data3=0
                if(res.result.data.records.length>0){
                    data1=res.result.data.records[0].pollution_control
                    data2=res.result.data.records[0].infrastructure_construction
                    data3=res.result.data.records[0].three_wastes
                }
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb1'));
                option = {

                    title: [{
                        text: '投资总额',
                        left: 'center',
                        textStyle: {
                            color: '#fff',
                            fontSize: '16'
                        }

                    }],
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c}万元 ({d}%)",
                        position: function (p) {   //其中p为当前鼠标的位置
                            return [p[0] + 10, p[1] - 10];
                        }
                    },
                    legend: {
                        top: '65%',
                        itemWidth: 10,
                        itemHeight: 10,
                        data: ['环境污染治理投资总额', '城市环境设施建设完成投资额', '三废综合利用产品产值'],
                        textStyle: {
                            color: 'rgba(255,255,255,.5)',
                            fontSize: '12',
                        }
                    },
                    series: [
                        {
                            name: '总投资额',
                            type: 'pie',
                            center: ['50%', '42%'],
                            radius: ['40%', '60%'],
                            color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab', '#06b4ab', '#06c8ab', '#06dcab', '#06f0ab'],
                            label: { show: false },
                            labelLine: { show: false },
                            data: [
                                { value: data1, name: '环境污染治理投资总额' },
                                { value: data2, name: '城市环境设施建设完成投资额' },
                                { value: data3, name: '三废综合利用产品产值' },
                            ]
                        }
                    ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
}
function echarts_32(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_environmental_indicators",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var data1=0
                var data2=0
                if(res.result.data.records.length>0){
                    data1=res.result.data.records[0].industrial_waste_water
                    data2=res.result.data.records[0].industrial_wastewater_discharge
                }
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb2'));
                option = {
                    title: [{
                        text: '工业废水',
                        left: 'center',
                        textStyle: {
                            color: '#fff',
                            fontSize: '16'
                        }
        
                    }],
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c} 万吨({d}%)",
                        position: function (p) {   //其中p为当前鼠标的位置
                            return [p[0] + 10, p[1] - 10];
                        }
                    },
                    legend: {
        
                        top: '65%',
                        itemWidth: 10,
                        itemHeight: 10,
                        data: ['工业废水排放量', '工业废水排放达标量'],
                        textStyle: {
                            color: 'rgba(255,255,255,.5)',
                            fontSize: '12',
                        }
                    },
                    series: [
                        {
                            name: '工业废水',
                            type: 'pie',
                            center: ['50%', '42%'],
                            radius: ['40%', '60%'],
                            color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab', '#06b4ab', '#06c8ab', '#06dcab', '#06f0ab'],
                            label: { show: false },
                            labelLine: { show: false },
                            data: [
                                { value: data1, name: '工业废水排放量' },
                                { value: data2, name: '工业废水排放达标量' }
                            ]
                        }
                    ]
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
    
}
function echarts_33(data_year) {
    getAction({
        type: "get",
        url: baseRootURL+"/cms/getColumnsAndData/ql_city_environmental_indicators",
        data: {getAll:"true",self_data_year:data_year.toString()},
        dataType: "json",
        success: function (res) {
            if (res.success) {
                var data1=0
                var data2=0
                var data3=0
                var data4=0
                if(res.result.data.records.length>0){
                    data1=res.result.data.records[0].sulfur_dioxide_removal
                    data2=res.result.data.records[0].sulfur_dioxide_emission
                    data3=res.result.data.records[0].industrial_dust_removal
                    data4=res.result.data.records[0].industrial_smoke_emission
                }
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb3'));
                option = {
                    title: [{
                        text: '工业二氧化硫',
                        left: 'center',
                        textStyle: {
                            color: '#fff',
                            fontSize: '16'
                        }
        
                    }],
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c} 吨({d}%)",
                        position: function (p) {   //其中p为当前鼠标的位置
                            return [p[0] + 10, p[1] - 10];
                        }
                    },
                    legend: {
                        top: '65%',
                        itemWidth: 10,
                        itemHeight: 10,
                        data: ['工业二氧化硫去除量', '工业二氧化硫排放量', '工业烟尘去除量', '工业烟尘排放量'],
                        textStyle: {
                            color: 'rgba(255,255,255,.5)',
                            fontSize: '12',
                        }
                    },
                    series: [
                        {
                            name: '工业二氧化硫',
                            type: 'pie',
                            center: ['50%', '42%'],
                            radius: ['40%', '60%'],
                            color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab', '#06b4ab', '#06c8ab', '#06dcab', '#06f0ab'],
                            label: { show: false },
                            labelLine: { show: false },
                            data: [
                                { value: data1, name: '工业二氧化硫去除量' },
                                { value: data2, name: '工业二氧化硫排放量' },
                                { value: data3, name: '工业烟尘去除量' },
                                { value: data4, name: '工业烟尘排放量' }
                            ]
                        }
                    ]
                };
        
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            }
        }
    })
}









