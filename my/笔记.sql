CREATE TABLE `jy_product_material`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NULL DEFAULT NULL COMMENT '产品id',
  `product_material_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原材料名称',
  `product_material_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原材料编码',
  `product_material_spec` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '原材料规格',
  `material` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '材料列表逗号分割',
  `number` int(11) NULL DEFAULT NULL COMMENT '核算数量',
  `weight` float(10,2) NULL DEFAULT NULL COMMENT '质量（kg）',
  `source` int(1) NULL DEFAULT NULL COMMENT '来源：0-自产，1-外采',
  `supplier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '供应商',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '原材料列表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;




QA: 
1. jy_product中的材质是否显示表jy_product_material中所包含的所有材质
2. 表jy_product_material中材质是否需要逗号分割
3. 字段 “质量（kg）”是否对应单位，还是为具体数值
4. 核算数量：请输入核算数量基于后方单位，请选择字段1，请选择字段2
5. 数据库操作放到model中 启用事务
6. 新增产品（jy_product，jy_product_material），编辑产品（jy_product_material）时，产品规格可编辑，附件可上传（jy_product）
7. 新增产品 下拉框（其他功能）
8. 产品查询
9. 查看详情（产品详情）：原材料清单（jy_product_material），核算概览（jy_adjust_account）
10. 发起核算
11. 启用、禁用
12. 表格导入（文件类型 CSV）

  Import
    文件名称
    总条数
    导入条数（成功）
    导入异常
    状态（js）
    操作人
    导入时间

  Export
    下载模板
    下载源文件
    下载异常数据

    public function getTeamInfoAll($keyWord= [],$page="")
    {
        //多条件搜索
        $where = function ($query)use($keyWord){
            if (isset($keyWord['teamName']) && $keyWord['teamName'] != ''){
                $query->whereor('rt_name','like',trim($keyWord['teamName'])."%");
            }
        };
        $result = self::where('rt_delete',self::IS_NO_DELETE)->where($where)->order('rt_create_time', 'asc')->paginate(['list_rows'=>$page, 'query'=>request()->param()], false);
        return $result;
    }



[2022-11-17]
-- 1. jy_product_data 字段category是否应该移到jy_product_material
2. 添加排放源（运输）特殊字段：运输类型名称、材料数量单位、运输距离
3. 添加排放源（能耗）排放源名称、用量
4. 添加排放源（辅助材料）没有材料和供应商字段
5. 添加排放源（直接逸散）排放源名称、逸散气体类型
6. 数据管理结果预览：用量/逸散量、碳排放因子、排放量、碳排放系数
7. 数据管理（原材料获取）：匹配类型、因子/产品
8. 产品核算详情：用量/逸散量、碳排放因子、排放量、碳排放系数、类别
9. 产品核算：碳排放量(kgCO2e)、单位产品碳排放率
10. 搜索排放因子：发布机构（发改委、生态环境部、IPCC），来源（系统因子库、组织因子库）

TBD 
搜索供应产品、核算产品、排放因子
CSV



[2022-11-18]
1. 搜索供应产品 
2. 搜索核算产品 
3. 搜索排放因子


http://192.168.0.113:9588/product/add




    /**
     * uploadFile 文件上传
     * @param $file 
     * @return void
     */
    public function uploadFile() {
        $file = request()->file();
        $fileSize = 1024*1024*2;

        foreach ($file as $val){

            try {
                validate(['file_url'=>'fileSize:' . $fileSize . '|fileExt:jpg,png,doc,pdf'])
                    ->check(['file_url' => $val]);
                $saveName = Filesystem::putFile('products', $val);

                return $saveName;
            } catch (\think\exception\ValidateException $e) {

                return false;
            }
        }

    }



[2022-11-22]
设计QA
1. 产品添加，页面是否需要添加产品状态（state），默认启用/禁用。
2. 常用功能配置是否限制数量

=======================================DATABASE=======================================
1. 新建常用项目配置表

CREATE TABLE `jy_common_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单id',
  `state` int(1) NULL DEFAULT NULL COMMENT '状态：1-启用，2-禁用',
  `menu_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单url',
  -- `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  -- `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '常用功能配置表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

2. 新建操作日志表
CREATE TABLE `jy_operation_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单id',
  `time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作类型',
  `module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作模块',
  `log` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作日志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

3. 新建资讯表
CREATE TABLE `jy_news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL COMMENT '标题',
  `author` varchar(15) NOT NULL COMMENT '作者',
  `content` mediumtext NOT NULL COMMENT '文章内容',
  `image_url` varchar(255) NULL DEFAULT NULL COMMENT '图片',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '文章是否显示 1是 2否',
  `addtime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资讯表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

4. 新建通知表
CREATE TABLE `jy_notices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL COMMENT '公告标题',
  `author` varchar(15) NOT NULL COMMENT '发布人',
  `content` longtext NOT NULL COMMENT '公告内容',
  `file_url` varchar(255) NULL DEFAULT NULL COMMENT '文件地址',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '公告是否显示 1是 2否',
  `addtime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '公告表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

===============================================================================


[2022-11-24]
针对不同客户选用不同数据库的设置，以及主从复制，数据库config设置（从redis读取登录用户信息，选用不同的数据库）

config->database设置中可设置不同的数据库

// 数据库连接配置信息
'connections'     => [
    'user1' => [],
    'user2' => [],
    'user3' => []
]

Db::connect('user1') ->
Db::connect('user2') ->
Db::connect('user3') ->

主从复制设置：https://blog.csdn.net/that_were_you/article/details/120079949  |  https://www.freesion.com/article/7091144877/
搭建：https://blog.csdn.net/hgb24660/article/details/115351386


Q:
添加原材料时是否需要先关联产品





    /**
     * test
     * 
     * @return void
     */
    public function test() {
        // $list = Db::connect('user1') -> table('jt_product')->select(); // 主库 user1
        // $list2 = Db::table('jy_product')->master(true)->select(); //主库
        $list3 = Db::table('jy_product')->select(); //从库 

        $data['code'] = '200';
        // $data['data']['list'] = $list;
        // $data['data']['list2'] = $list2;
        $data['data']['list3'] = $list3;

        return json($data);
    }

    /**
     * test
     * 
     * @return void
     */
    public function testadd() {
        $data['product_name'] = $_POST['product_name'];
        $list = Db::connect('user1') -> table('jt_product')->insert($data);

        $data['code'] = '200';
        $data['data']['list'] = $list;

        return json($data);
    }



[2022-11-28]
!preg_match('/^1([0-9]{9})/',$mobile



[2022-12-2]

数据库设计建议

1.数据表可以分类"t_xxx”代表实体表，"r_xxx”代表关系表，易于研发区分表作用
  （暂不处理，后面考虑）

2.表字段小写，表和字段备注必须填写
    OK

3.id尽量使用bigint，随着时间跨度增加数据量级会变大，int后面会长度不够，线上出现过这种问题4.表字段由多个单词组成的下划线连接各个单词 例如: tenant id (租户id)
  OK，前缀固定，后面加上租户对应的数据库id（如jy_product_1） 

5.每个数据表的固定字段: (最好都有)
是否删除: delete_flag : 0表示未删除，1表示已经逻辑删除，saas尽量不做物理删除租户ld: tenant_id
创建人:create by创建时间:create time，这个尽量要统一，表里有一部分addtime修改人:modify_by修改时间:modify time
    OK

6.关键表要设计2-4个几余字段，避免后面大表加字段加不上
    OK

7.索引和唯一建暂时没有
  （暂不处理，后面考虑）


1. 原材料删除
product_material_name
pid
url

2. 原材料添加
pid
url

3. 原材料编辑
pid
url

4. 产品添加
pid
url

5. 产品编辑
pid
url

6. 更新产品状态
product_name
pid
url


[2022-12-5]
新建气体表
CREATE TABLE `jy_gases`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NULL DEFAULT NULL COMMENT '温室气体名称',
  `type` varchar(15) NULL DEFAULT NULL COMMENT '温室气体类型',
  `gwp_6` int(11) NULL DEFAULT NULL COMMENT 'GWP数值IPCC第六次评估报告',
  `gwp_5` int(11) NULL DEFAULT NULL COMMENT 'GWP数值IPCC第五次评估报告',
  `gwp_4` int(11) NULL DEFAULT NULL COMMENT 'GWP数值IPCC第四次评估报告',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用 1是 2否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '气体表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

数据管理阶段表
CREATE TABLE `jy_data_stage` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '阶段名称' COLLATE 'utf8mb4_general_ci',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='数据管理阶段表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;

阶段分类表
CREATE TABLE `jy_stage_category` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '分类名称' COLLATE 'utf8mb4_general_ci',
    `stage_id` int(11) NULL DEFAULT NULL COMMENT '阶段id',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='阶段分类表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;



产品核算梳理：

1. 新加数据管理阶段表（jy_data_stage）、阶段分类表（jy_stage_category）。弃用行业分类表（jy_industry）
2. 产品核算列表过滤核算周期 '2022-10-15-2023-1-15'
2. 数据管理列表（jy_product_data主表获取数据）按照字段（data_stage）分为 N（从jy_data_stage中获取）+ 1（固定：结果预览）个tab。（需要分页）
3. 排放源添加（按阶段分类，从jy_stage_category中获取，添加到jy_product_data中）
4. 编辑排放源
5. 删除排放源
6. 搜索供应产品、核算产品、排放因子
7. 结果预览（1. 分为几个部分，是否需要将使用和废弃处置的部分显示 2. 数据量很多之后的展示【分页/滚动条】）


[2022-12-7]
新建核算时选择产品

创建中间件
php think make:middleware Check

[2022-12-08T15:09:55+08:00][sql] CONNECT:[ UseTime:0.036081s ] mysql:host=192.168.0.92;port=3306;dbname=tanlu_db;charset=utf8
[2022-12-08T15:09:55+08:00][sql] SHOW FULL COLUMNS FROM `jy_product_calculate` [ slave|RunTime:0.021727s ]
[2022-12-08T15:09:55+08:00][sql] SELECT COUNT(*) AS think_count FROM `jy_product_calculate` `jpc` LEFT JOIN `jy_product` `jp` ON `jp`.`id`=`jpc`.`product_id` LEFT JOIN `jy_user` `ju` ON `ju`.`id`=`jpc`.`modify_by` WHERE  (  ( `jpc`.`week_start` <= '2022-09-13' )  OR ( `jpc`.`week_end` >= '2023-01-10' ) ) LIMIT 1 [ slave|RunTime:0.041075s ]
[2022-12-08T15:09:55+08:00][sql] SELECT jpc.id, jpc.product_id, jpc.number, jpc.unit, CONCAT_WS("-", DATE_FORMAT(jpc.week_start, "%Y-%m"), DATE_FORMAT(jpc.week_end, "%Y-%m")) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time,`jp`.`product_name`,`jp`.`product_no`,`jp`.`product_spec`,`jp`.`file_url`,`ju`.`username` FROM `jy_product_calculate` `jpc` LEFT JOIN `jy_product` `jp` ON `jp`.`id`=`jpc`.`product_id` LEFT JOIN `jy_user` `ju` ON `ju`.`id`=`jpc`.`modify_by` WHERE  (  ( `jpc`.`week_start` <= '2022-09-13' )  OR ( `jpc`.`week_end` >= '2023-01-10' ) ) ORDER BY `jpc`.`id` DESC LIMIT 0,10 [ slave|RunTime:0.289481s ]


[2022-12-8]

1.需求：查询两个时间段是否有交集
 
2.分析：分为三种情况：
 
入参：查询开始时间，查询结束时间
 
数据库字段：start_date,end_date
 
    2.1 :查询开始时间在start_date与end_date之间，则肯定有交集
 
    2.2 :查询结束时间在start_date与end_date之间，则肯定有交集
 
    2.3：查询开始时间>=start_date，并且，查询结束时间<=end_date，则肯定有交集
 
其余情况则无时间范围的交集了。

select * from jy_product_calculate jpc
where 
((jpc.start_date >= '2019-01-01' and jpc.start_date<= '2019-12-31')
OR (jpc.end_Date >= '2019-01-01' and jpc.end_Date<= '2019-12-31')
OR (jpc.start_date <= '2019-01-01' and jpc.end_Date>= '2019-12-31'))
AND jpc.`status`=3;

即：
select * from jy_product_calculate jpc
where jpc`status`=3
and not (jpc`start_date`>'2019-12-31' or jpc`end_date`<'2019-01-01')

SELECT COUNT(*) AS think_count FROM `jy_product_calculate` `jpc` 
LEFT JOIN `jy_product` `jp` ON `jp`.`id`=`jpc`.`product_id` 
LEFT JOIN `jy_user` `ju` ON `ju`.`id`=`jpc`.`modify_by` 
WHERE  NOT (  ( `jpc`.`week_end` < '2022-09-06' )  OR ( `jpc`.`week_start` > '2023-01-04' ) )


->where($where)
->where(function($query) use ($whereor){$query->whereOr($whereor);})


[2022-12-9]

核算产品添加唯一性校验：
核算产品名称 + 核算周期（2022-6-2022-10） 唯一

核算报告：

从jy_product_calculate 获取已完成的核算产品名称， 核算时间带入所选核算产品的所有核算周期

或者

从核算列表页 只有完成状态才会有生成报告选项 点击跳转 新增核算报告页， 带入所选的核算产品名称及核算周期


修改

1. 数据管理：获取阶段下的类别、材料
2. 产品核算：新增核算产品（产品名称 + 产品编码 + 核算周期）唯一


[2022-12-12]
CREATE TABLE `jy_report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '报告名称' COLLATE 'utf8mb4_general_ci',
    `sort` INT(11) NULL DEFAULT NULL COMMENT '排序',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人id',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='报告表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=18
;


核算报告详情将 核算数量、碳排放量、碳排放系数带入

SHOW FULL COLUMNS FROM `jy_product_calculate` [ slave|RunTime:0.018026s ]
[2022-12-12T13:36:32+08:00][sql] SELECT * FROM `jy_product_calculate` `jpc` WHERE  `jpc`.`id` = '86' [ slave|RunTime:0.007648s ]
[2022-12-12T13:36:32+08:00][sql] SHOW FULL COLUMNS FROM `jy_product_data` [ slave|RunTime:0.009824s ]
[2022-12-12T13:36:32+08:00][sql] SELECT `jpd`.`data_stage`,`jpd`.`name`,jds.name category,`jpd`.`number`,`jpd`.`unit` FROM `jy_product_data` `jpd` LEFT JOIN `jy_data_stage` `jds` ON `jds`.`id`=`jpd`.`category` WHERE  `jpd`.`product_calculate_id` = '86' [ slave|RunTime:0.005323s ]


materials: 
[
    {
        "id": 77,
        "product_id": 28,
        "week_start": "2022-09-06",
        "week_end": "2022-12-06",
        "number": 88.88,
        "unit_type": 1,
        "unit": 3,
        "scope": 3,
        "stage": "1,2,3,4,5",
        "remarks": "备注AAAA",
        "emissions": null,
        "state": 3,
        "create_by": 14,
        "modify_by": 14,
        "create_time": "2022-12-05 13:36:02",
        "modify_time": "2022-12-05 13:36:02"
    }
]

[{"id":0,"name":"铝合金","number":"1","unit":"1"},{"id":1,"name":"其他材料","number":"1","unit":"2"},{"id":3,"name":"其他材料","number":"1","unit":"2"},{"id":4,"name":"其他材料","number":"2","unit":"3"},{"id":5,"name":"铝合金","number":"2","unit":"3"}]

{
    "bb": [
        false,
        false,
        2,
        2,
        1
    ],
    "cc": [
        1000,
        0.002,
        0.002
    ]
}

{
    "product_calculate_id": "5",
    "product_id": "28",
    "data_stage": "2",
    "category": "1",
    "name": "排放源AAA",
    "number": 2001.004,
    "unit": "1",
    "materials": "铝合金,其他材料",
    "numbers": "1.002,1001.002",
    "units": "1,1",
    "unit_type": "1",
    "code": "SPEC",
    "specs": "PPE",
    "source": "1",
    "supplier_id": "1",
    "distance": "200",
    "match_type": "1",
    "create_by": "14",
    "create_time": "2022-12-13 13:18:41"
}



1

 1000

 1000

 0.002

0.002

[{"id":0,"name":"铝合金","number":"1","unit":"1"},{"id":1,"name":"其他材料","number":"1","unit":"2"},{"id":4,"name":"其他材料","number":"2","unit":"3"},{"id":5,"name":"铝合金","number":"2","unit":"3"}]



[2022-12-14]

产品核算列表

编辑中 -> 可编辑可数据管理可提交审核

待审核 -> 可撤回申请

已完成 -> 审核完成后 可生成报告


"products": [
{
    "product_id": 27,
    "name"： "实例产品C-SLCPA",
    "weeks"：
    {
        "2022-09-2024-01",
        "2022-10-2024-01",
        "2022-11-2024-01",
        "2022-12-2024-01",
    }
},
{
    "product_id": 28,
    "name"： "实例产品C-SLCPA",
    "weeks"：
    {
        "2022-09-2024-01",
        "2022-10-2024-01",
        "2022-11-2024-01",
        "2022-12-2024-01",
    }
}


[2022-12-15]
TP6 安装 phpword
composer require phpoffice/phpword

安装时报错：The Process class relies on proc_open, which is not available on your PHP installation

解决：报着个错的原因是：proc_open函数被禁用，只需要开启函数即可。
解决方法：
在php.ini中，找到disable_functions选项，看看后面是否有proc_open函数被禁用了，如果有的话，去掉即可.
其实如果php的文档熟悉的话，你应该马上就能知道proc_open实际上是一个函数，
是php用来和shell交互的函数，一般这种可以直接作用于操作系统的函数是非常不安全的，对于这种不安全的函数，总是要做一些措施来进行保护。
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore

如果安装了宝塔面板，可以在软件管理->php->禁用函数，找到proc_open函数，删除，重载配置，重启php即可。
proc_get_status

安装视图报错
composer require topthink/think-view

composer.json 中追加，解决。
    "repopackagist": {
        "packagist": {
            "type": "composer",
            "url": "https://packagist.phpcomposer.com"
        }
    }



carbon_price

CREATE TABLE `jy_carbon_price` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `city_id` INT(11) NULL DEFAULT NULL COMMENT '城市id',
    `time` DATE NULL DEFAULT NULL COMMENT '时间',
    `trading_avg_price` INT(11) NULL DEFAULT NULL COMMENT '成交均价（元/吨）',
    `turnover` INT(11) NULL DEFAULT NULL COMMENT '成交量（吨）',
    `amount` INT(11) NULL DEFAULT NULL COMMENT '成交额（元）',
    `daily_percent` VARCHAR(50) NULL DEFAULT NULL COMMENT '日涨跌幅',
    `sort` INT(11) NULL DEFAULT NULL COMMENT '排序',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人id',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='碳价管理表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=18
;




calculatereport

carbonlabel


INSERT INTO `jy_product_data` 
SET `product_calculate_id` = 77 , `product_id` = 28 , `data_stage` = 1 , `category` = 6 , `name` = '12312' , `number` = '' , `unit` = '' , `material` = '' , `unit_type` = '' , `code` = '' , `specs` = '' , `source` = '' , `supplier_id` = '' , `distance` = '' , `match_type` = '' , `create_by` = 14 , `create_time` = '2022-12-21 13:25:35'


            $data['role_id'] = $_POST['role_id'];
            // $roles = explode(',', $_POST['role_id']);
            // $result = array_unique($roles);
            // $data['role_id'] = implode(',', $result);


                    if ($role) {
                        $list['data'][$key]['roles'][$roles_key]['id'] = $roles_value;
                        $list['data'][$key]['roles'][$roles_key]['name'] = $role[0]['title'];
                    }

[2022-12-30]
C:/JET/jayee-jyapi/public
C:\project\jyt\public

180.175.228.214


##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|title | string | 因子名称 |
|model | string | 规格/型号 |
|factor_value | decimal | 二氧化碳当量因子数值 |
|unit | string | 单位 |
|year | string | 发布年份 |
|mechanism | string | 发布机构 |
|country | string | 发布国家/组织 |


##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|title | string | 因子名称 |
|factor_id | int | 因子id |
|model | string | 规格/型号 |
|grade | string | 因子评级 |
|uncertainty | string | 不确定性 |
|describtion | string | 场景描述 |
|factor_value | decimal | 二氧化碳当量因子数值 |
|molecule | string | 二氧化碳当量分子单位 |
|denominator | string | 二氧化碳当量分母单位 |
|mechanism | string | 发布机构 |
|year | string | 发布年份 |
|country | string | 发布国家/组织 |
|region | string | 发布地区 |
|file_name | string | 来源文件名称 |

http://192.168.0.45/gitlab-instance-ac000afe/jayee-api 后端
http://192.168.0.45/gitlab-instance-ac000afe/jayee-web 前端


{
    "id": 9,
    "product_id": 28,
    "number": 50,
    "unit_type": 1,
    "unit": 3,
    "scope": 3,
    "stage": "1,2,3,4",
    "week_start": "2022-07-15",
    "week_end": "2022-12-15",
    "week": "2022-07-2022-12",
    "remarks": "备注AAAA",
    "state": 1,
    "modify_time": "2022-12-07",
    "emissions": 806,
    "coefficient": 16.12,
    "product_name": "实例产品C",
    "product_no": "SLCPA",
    "product_spec": "初代",
    "file_url": "www.baidu.com",
    "username": "admin"
}

##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|product_id | int | 产品id |
|number | number | 核算数量,会有小数点 |
|unit_type | string | 单位类型 |
|unit | string | 单位 |
|scope | string | 评价范围 |
|week_start | string | 核算周期开始 |
|week_end | string | 核算周期结束 |
|week | string | 核算周期 |
|remarks | string | 备注 |
|state | string | 阶段 |
|modify_time | string | 修改时间 |
|emissions | string | 排放量 |
|coefficient | string | 排放系数 |
|product_name | string | 产品名称 |
|product_no | string | 产品编号 |
|product_spec | string | 产品规格 |
|file_url | string | 文件地址 |
|username | string | 更新人 |



CREATE TABLE `jy_dictionary_unit` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `unit_name` VARCHAR(255) NULL DEFAULT NULL COMMENT '单位名称' COLLATE 'utf8mb4_general_ci',
    `unit_name_cn` VARCHAR(255) NULL DEFAULT NULL COMMENT '单位名称' COLLATE 'utf8mb4_general_ci',
    `unit_type` INT(11) NULL DEFAULT NULL COMMENT '单位类型，1-质量单位，2-数量单位，3-长度单位，4-时间单位，4-货币单位，4-组合单位，4-面积单位',
    `is_base` TINYINT(1) NULL DEFAULT NULL COMMENT '是否为基准单位',
    `conversion_ratio` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '换算比例',
    `sort` INT(11) NULL DEFAULT NULL COMMENT '排序',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人id',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='单位字典表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=18
;


CREATE TABLE `jy_dictionary_type_classification` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '名称' COLLATE 'utf8mb4_general_ci',
    `sort` INT(11) NULL DEFAULT NULL COMMENT '排序',
    `state` TINYINT(1) NULL DEFAULT NULL COMMENT '状态',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人id',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='字典类型分类表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=18
;



[2023-1-11]
产品核算编辑 unit_type unit


1. 产品核算：排放量、因子系数的计算
2. 文件上传下载删除
3. 原材料名称 + 原材料编码 唯一性校验
4. 数据字典



INSERT INTO jy_factor
      (title,language,title_language,model,describtion,mechanism,mechanism_short,grade,source,year,country,region,file_name,factor_value,molecule,denominator)
VALUE 

本周完成工作：
完成产品管理模块、产品核算模块、产品数据管理模块、核算报告模块、数据审批模块（T.B.D 1. 数据字典->单位类型 2. 核算报告内容完善 3. 文件接口对接）

本周工作总结：
1. 产品管理
    1.1. 产品详情->核算概览
    1.2. 产品列表当前核算数量（当前产品核算数量的条数）修改
    1.3. 产品名称 + 产品编码、原材料名称 + 原材料编码唯一性校验

2. 产品核算
    2.1. 产品核算列表碳排放量（kgCO2)、碳排放系数的计算
    2.2. 数据管理因子/产品的选择
    2.3. 数据管理排放量的计算

3. 核算报告 模块

4. 数据审批 模块

5. 因子库 模块

5. 产品碳足迹测试

6. 修复BUG： 
    6.1. 产品管理当前核算数量的获取 
    6.2. 数据管理阶段运输类型的选择材料 
    6.3. 产品核算原材料获取阶段将产品原材料清单代入

下周工作计划:
1. 数据字典模块开发，产品碳足迹各模块单位的对接及计算
2. 核算报告模块，下载报告内容的完善
3. 文件上传下载接口的对接



##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|name | string | 单位名称 |
|type | string | 单位分类 |
|is_base | int | 是否为基准单位 |
|conversion_ratio | number | 与基准单位换算比了 |
|modify_time | time | 更新时间 |
|username | string | 更新人 |

        "list": [
            {
                "id": 1,
                "unit_type": "质量单位",
                "units": [
                    {
                        "id": 1,
                        "unit_name": "kg"
                    },
                    {
                        "id": 2,
                        "unit_name": "g"
                    },
                    {
                        "id": 3,
                        "unit_name": "t"
                    },
                    {
                        "id": 4,
                        "unit_name": "lb"
                    },
                ]
            },
            {
                "id": 2,
                "unit_type": "长度单位",
                "units": [
                    {
                        "id": 6,
                        "unit_name": "原材料"
                    },
                    {
                        "id": 6,
                        "unit_name": "原材料"
                    },
                    {
                        "id": 6,
                        "unit_name": "原材料"
                    },
                    {
                        "id": 6,
                        "unit_name": "原材料"
                    },
                ]
            }
        ]


单位管理 -> views/dictionary/unitmanagement
字典管理 -> views/dictionary/dictionarymanagement

更多资讯 -> views/Dashboard/moreinformation
资讯详情 -> views/Dashboard/informationdetail
历史通知 -> views/Dashboard/historynotice
通知详情 -> views/Dashboard/noticedetail

1|4|8|17|18|19|25|35|38|41|2#4|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,11,12|10#4|11#4|12#4|13#4|14#3|15#9|16#10|20#4|21#4|22#4|23#4|24#4|26#4|27#4|28#4,10|29#4|30#4|31#4|32#4|33#4|34#4|36#4|37#4|39#4|40#4|42#4|43#4|44#4|51#4|52#4|53#4|54#4|56|57#1,2,3,4,9,10,11|58#1,2,3,4,9,10,11|59|60#1,2,3,4|61#4|62#4|63#4|64#4|65#4|66#4

修改接口 （同api/dict/list）
api/roles/getList
api/auth/list

code '200' 修改为 200
message 去掉

文件上传
product/upload -> file/upload

文件下载
productdownload -> file/download

文件上传 新增字段 module
产品文件 module = 'productfile'
资讯文件 module = 'noticefile'



本周完成工作:
完成数据字典单位管理模块、产品碳足迹各模块单位对接、文件上传接口统一
本周工作总结:
1. 新增数据字典 -> 单位管理

2. 文件上传接口
    2.1. 核算周期DB修改（week_start、week_end）字段类型 DATE -> varchar(7)
    2.2 新增字段 module
        产品文件 module = 'productfile'
        资讯文件 module = 'noticefile'

3. 数据管理
    数据管理运输类验证

4. 产品管理 模块
    4.1 virtual_product_id -> virtual_id
    4.2 新增unit_str
    4.3 新增用户名重复验证
    4.4 修改排放源验证规则

5. 产品碳足迹测试

下周工作计划:
1. 单位导入和下载
2. 核算报告模块，下载报告内容的完善
3. 权限对接

阿里云服务器相关信息
电话号码：18717955715
用户名：aliyun6781136405
密码：Jyte2023


服务器账号密码
47.122.18.241
Jyt202(VNC密码)
Jyte2023(服务器密码)

宝塔面板地址:http://47.122.18.241:8888/4308e7c2
用户名:42rofkdq
密码:@jyt#123

数据库账号密码
tanlu_db
yR42jna366k4aFRS

192.68.0.92
location ^~ /
{
    if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' '*';
            add_header 'Access-Control-Allow-Headers' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
    }
    expires 0;#12h;
    if ($request_uri ~* "(php|jsp|cgi|asp|aspx)")
    {
         expires 0;
    }
    if ($http_module ~ "api"){
        proxy_pass http://127.0.0.1:8001;
    }
    if ($http_module ~ "jy-api") {
            proxy_pass http://192.168.0.37:9588;
    }
    if ($http_module ~ "sass-api"){
        proxy_pass http://192.168.0.30:9588;
    }
    proxy_pass http://127.0.0.1:8001;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header REMOTE-HOST $remote_addr;

    add_header X-Cache $upstream_cache_status;

    proxy_set_header Accept-Encoding "";
    
    sub_filter_once off;

    #proxy_cache cache_one;
    #proxy_cache_key $host$uri$is_args$args;
    #proxy_cache_valid 200 304 301 302 12h;
    
    try_files $uri $uri/ /index.html
    index  index.html index.htm;
}



47.122.18.241

#PROXY-START/

location /
{
    proxy_pass http://47.122.18.241;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header REMOTE-HOST $remote_addr;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_http_version 1.1;
    # proxy_hide_header Upgrade;

    add_header X-Cache $upstream_cache_status;

    #Set Nginx Cache
    
    
    set $static_fileakDeAocg 0;
    if ( $uri ~* "\.(gif|png|jpg|css|js|woff|woff2)$" )
    {
        set $static_fileakDeAocg 1;
        expires 1m;
        }
    if ( $static_fileakDeAocg = 0 )
    {
    add_header Cache-Control no-cache;
    }
}

#PROXY-END/






user  www www;
worker_processes auto;
error_log  /www/wwwlogs/nginx_error.log  crit;
pid        /www/server/nginx/logs/nginx.pid;
worker_rlimit_nofile 51200;

stream {
    log_format tcp_format '$time_local|$remote_addr|$protocol|$status|$bytes_sent|$bytes_received|$session_time|$upstream_addr|$upstream_bytes_sent|$upstream_bytes_received|$upstream_connect_time';
  
    access_log /www/wwwlogs/tcp-access.log tcp_format;
    error_log /www/wwwlogs/tcp-error.log;
    include /www/server/panel/vhost/nginx/tcp/*.conf;
}

events
    {
        use epoll;
        worker_connections 51200;
        multi_accept on;
    }

http
    {
        include       mime.types;
        #include luawaf.conf;

        include proxy.conf;

        default_type  application/octet-stream;

        server_names_hash_bucket_size 512;
        client_header_buffer_size 32k;
        large_client_header_buffers 4 32k;
        client_max_body_size 50m;

        sendfile   on;
        tcp_nopush on;

        keepalive_timeout 60;

        tcp_nodelay on;

        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 4 64k;
        fastcgi_busy_buffers_size 128k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_intercept_errors on;

        gzip on;
        gzip_min_length  1k;
        gzip_buffers     4 16k;
        gzip_http_version 1.1;
        gzip_comp_level 2;
        gzip_types     text/plain application/javascript application/x-javascript text/javascript text/css application/xml;
        gzip_vary on;
        gzip_proxied   expired no-cache no-store private auth;
        gzip_disable   "MSIE [1-6]\.";

        limit_conn_zone $binary_remote_addr zone=perip:10m;
        limit_conn_zone $server_name zone=perserver:10m;

        server_tokens off;
        access_log off;

server
    {
        listen 888;
        server_name phpmyadmin;
        index index.html index.htm index.php;
        root  /www/server/phpmyadmin;

        #error_page   404   /404.html;
        include enable-php.conf;

        location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
        {
            expires      30d;
        }

        location ~ .*\.(js|css)?$
        {
            expires      12h;
        }

        location ~ /\.
        {
            deny all;
        }

        access_log  /www/wwwlogs/access.log;
    }
include /www/server/panel/vhost/nginx/*.conf;
}



第一步：下载RedisDesktopManager

百度网盘: https://pan.baidu.com/s/1kU8sY3P
这个百度一搜就有了，但是现在的版本ssh用不了
建议找可以用的版本，这个百度，懂得都懂。

#第二步：服务器宝塔redis设置

在配置文件将bind 127.0.0.1 注释掉
将protected-mode的yes改为no
daemonize的no改为yes
添加一行 requirepass 你的密码
保存

##注意
此处保存配置后，点击重载配置，然后到终端界面，
输入ps -ef | grep redis
kill -9 pid
杀死进程后，到宝塔界面重启redis才有用。
否则可能出现连接不上redis的情况


连接成功，记得打开你的云服务器安全组中添加对应端口，
宝塔的安全中添加端口6379和22

#一个小问题
Could not connect to Redis at 127.0.0.1:6379:由于目标计算机积极拒绝，无法连接

解决方法
输入命令redis-server.exe redis.windows.conf后，不要关闭，不要关闭
重新打开cmd窗口输入redis-cli.exe-h 127.0.0.1






伪静态


    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
    {
      if (!-e $request_filename){
       rewrite ^(.*)$ /dist/$1 last; break;
      }
    }

    location ~ .*\.(js|css)?$
    {
    if (!-e $request_filename){
     rewrite ^(.*)$ /dist/$1 last; break;
     }
    }

location ~* (runtime|application)/{
    return 403;
}
location / {
    if (!-e $request_filename){
        rewrite  ^(.*)$  /index.php?s=$1  last;   break;
    }
}



/tmp/php-cgi-80.sock




1. 修改逻辑:产品核算时 当前产品校验排放源名称 + 编码唯一 (新增产品核算，产品名 + 编号 + 核算周期 唯一 可以和已删除的核算产品名 + 编号 + 核算周期 相同)

2. 核算产品删除改为修改状态 新增字段is_del(默认为1，1-未删除，2-已删除)
排放源删除改为修改状态 新增字段is_del(默认为1，1-未删除，2-已删除)
合计排放源 合计状态未删除的排放源 修改计算逻辑

3. 数据管理选择 因子或产品时 修改为只能选择已编辑完成的核算产品
选择 因子或产品，调接口/productcalculate 时 传 state = 1
核算列表 不需要传state


src\views\product\management.vue
  99,30:   formdata.append('module', "productfile")
productfile -> 产品碳足迹

src\views\notices\noticesedit.vue
  144,32:     formdata.append('module', "noticefile")
noticefile -> 后台端

DB:
UPDATE jy_file set module = '产品碳足迹' where module = 'productfile'
UPDATE jy_file set module = '后台端' where module = 'noticefile'


逻辑修改：新增产品核算带入原材料阶段的原材料类型的排放源默认匹配类型有产品修改为因子

INSERT INTO jy_factor
(title,language,title_language,model,describtion,mechanism,mechanism_short,grade,year,country,region,file_name,factor_value,molecule,denominator)
VALUES

LOAD DATA LOW_PRIORITY LOCAL INFILE 'C:\\Users\\admin\\Desktop\\文档\\1.csv' INTO TABLE `tanlu_db`.`jy_factor_copy` CHARACTER SET gbk FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES (`title`, `factor_id`, `language`, `title_language`, `model`, `describtion`, `mechanism`, `mechanism_short`, `grade`, `year`, `country`, `region`, `file_name`, `factor_value`, `molecule`, `denominator`);
/* SQL错误（2）：File 'C:\Users\admin\Desktop\文档\1.csv' not found (Errcode: 2) */




golang 
打包成exe go build main.go
在window下打包成linux
在项目main.go同级目录下，逐条执行以下命令

-- set CGO_ENABLED=0
-- set GOOS=linux
-- set GOARCH=amd64
-- go build -o main-linux main.go


1、首先打开cmd窗口，cd到项目主文件main.go所在目录
2、设置 linux 编译配置：
SET GOOS=linux
SET GOARCH=amd64
3、打包（此时会打包成Linux上可运行的二进制文件，不带后缀名的文件）
go build
4、把打包好的项目，部署到Linux上
假如把打包好的项目上传到Linux上的 /opt/mygo文件夹下 文件名假如是 main
5、给打包好的项目文件main赋予执行权限
切换到项目所在目录 cd /opt/mygo 执行命令 chmod 773 main
6、运行项目
切换到项目所在目录 cd /opt/mygo ./main


go: github.com/dgrijalva/jwt-go@v3.2.0+incompatible: Get "https://proxy.golang.org/github.com/dgrijalva/jwt-go/@v/v3.2.0+incompatible.mod": dial tcp 142.251.42.241:443: connectex: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.

执行
go env -w GOPROXY=https://goproxy.cn 即可解决报错


    USERNAME = "root"
    PASSWORD = "265e4f83fbe18386"
    NETWORK  = "tcp"
    SERVER   = "47.122.18.241"
    PORT     = 3306
    DATABASE = "tanlu_db"



本周完成工作:
完成核算报告模板修改、修改产品碳足迹、修改golang登录、权限获取接口、系统设置（文件管理）接口、供应链碳管理交接
本周工作总结:
1. 核算报告模板修改

2. 修改产品碳足迹

3. 修改golang登录、权限获取接口

4. 系统设置（文件管理）接口

5. 供应链碳管理交接
下周工作计划:
1.系统设置（文件管理）接口
2.系统设置
3.供应链碳管理对接
4.碳因子库接口


  git config --global user.email "996842671@qq.com"
  git config --global user.name "wuyinghua"


git stash #封存修改
git pull origin master 
git stash pop #把修改还原



  /file/download -> /filedownload
  /file/index -> /file
sysetapproval

views/carbondata/productdetail     产品详情页
views/carbondata/productcomparison 产品对比
views/carbondata/productchart      产品对照结果图表


创建中间件
 php think make:middleware OperationLog

 use app\model\system\OperationModel;

 // 添加操作日志
$data_log['user_id'] = $data_redis['userid'];
$data_log['module'] = '系统设置';
$data_log['type'] = '功能操作';
$data_log['time'] = date('Y-m-d H:i:s');
$data_log['url'] = $request->pathinfo();
$data_log['log'] = '添加组织：' . $data['name'];

OperationModel::addOperation($data_log);



本周完成工作:
完成服务器代码库部署，下载管理接口，审批设置接口，组织管理接口，用户管理-组织配置接口，操作日志接口
本周工作总结:
1. 服务器代码库部署

2. 下载管理接口

3. 审批设置接口

4. 组织管理接口

5. 用户管理-组织配置接口

6. 操作日志接口
下周工作计划:
1.审批设置接口，组织管理接口，用户管理-组织配置接口的对接
2.审批设置接口，组织管理接口，用户管理-组织配置接口的bug对应
3.供应链碳管理接口对接
4.供应链碳管理接口bug对应
5.操作日志接口各模块埋点的完善

===========================================================================================================================

用户登陆时 通过go向redis中添加main_organization 从中获取$main的信息

注册要分为 
1. 用户注册 会有填写母公司
2. 邀请链接注册供应商 绑定母公司


供应链碳管理-采购端（我是采购者）

供应商管理
供应商名称 为当前用户所在组织的母公司的名称



http://47.122.18.241 线上演示环境

http://47.122.18.241:9999 线上测试环境

http://192.168.0.37:4000 本地开发环境


  // import.meta.env.VITE_USER_NODE_ENV == 'development'
  //   ? 'http://47.122.18.241:9999/'
  //   : 'http://47.122.18.241:80/'
    import.meta.env.VITE_USER_NODE_ENV == 'development'
    ? 'http://192.168.0.92:80/'
    : 'http://192.168.0.37:9588/'


Request $request
$data_redis = $request->middleware('data_redis');
$main_id = $data_redis['main_organization_id'];




CREATE TABLE `jy_supplier_invite` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `main_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '主体id',
    `invite_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '使用者id',
    `invitation_code` VARCHAR(100) NULL DEFAULT NULL COMMENT '邀请码' COLLATE 'utf8mb4_general_ci',
    `is_use` INT(1) NULL DEFAULT NULL COMMENT '是否已使用（注册） 1 未使用 2 已使用',
    `create_time` DATETIME NOT NULL COMMENT '创建时间',
    `modify_time` DATETIME NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='供应商邀请表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=7
;


本周完成工作:
完成审批设置接口，组织管理接口，用户管理-组织配置接口，操作日志接口
本周工作总结:
1.审批设置接口，组织管理接口，用户管理-组织配置接口的对接
2.审批设置接口，组织管理接口，用户管理-组织配置接口的bug对应
3.供应链碳管理接口对接
4.供应链碳管理接口bug对应
5.操作日志接口各模块埋点的完善
下周工作计划:
1.因子库接口
3.供应链碳管理接口对接
4.供应链碳管理接口bug对应

Key: 'RegisterType.Username' Error:Field validation for 'Username' failed on the 'required' tag


http://192.168.0.45/gitlab-instance-ac000afe/jayee-api
http://192.168.0.45/gitlab-instance-ac000afe/jayee-web
http://192.168.0.45/gitlab-instance-ac000afe/ginproject.git

0 + CAST(jpd.coefficient AS CHAR) coefficient


http://localhost:4000/#/supplierInvite?invite_code=YDupuYlgX68FXQJNZyjNEyHilwY
http://192.168.0.25:4000/#/login?invite_code=M2fzu49nVq4NTBVKZifEFC/qlA3q


表格导入QA

我是采购者-供应商产品导入字段
产品名称
产品型号
产品类型
供应商     （导入文件中是id 还是名称，导入不存在的供应商）


我是供应商-供应产品管理
产品名称
产品型号
产品类型
核算周期
核算数量
单位类型（ID还是名称 ）
单位（ID还是名称 ）

第三方认证以及排放源是否需要

template_type
supplier_template
customer_template



本周完成工作:
完成供应链碳管理接口对接、供应链碳管理接口bug对应、供应链碳管理接口开发
本周工作总结:
1.采购端-供应商管理接口bug对应、开发及对接
2.采购端-供应商产品管理接口bug对应、开发及对接
3.采购端-报送审批接口bug对应、开发及对接
4.供应端-客户管理接口bug对应、开发及对接
5.供应端-供应产品管理接口bug对应、开发及对接
6.供应端-货品报送接口bug对应、开发及对接
7.供应链碳管理测试
下周工作计划:
1.供应链碳管理接口bug对应
2.合作端接口开发



match_type == 2 && source == 2 时为搜索供应产品
调supplycustomerproduct 带上参数supplier_id（例： supplycustomerproduct?pageSize=10&pageIndex=1&product_name=&supplier_id=111，搜索条件只有产品名称）


##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|product_name | string | 产品名称 |
|product_no | string | 产品编码 |
|product_spec | string | 产品规格 |
|unit_str | string | 单位 |
|coefficient | int | 产品碳排放系数 |
|emissions | int | 单位碳排放量 |

##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|<font color="#dd0000">overview</font> | <font color="#dd0000">array</font> | <font color="#dd0000">产品对照结果总览</font> |
|`0` | `array` | `产品1碳排放` |
|`1` | `array` | `产品2碳排放` |
|`reduction` | `array` | `减排值` |
|`reduction_rate` | `array` | `减排比例` |
|raw | number | 原材料获取 |
|manufacturing | number | 生产制造 |
|distribution | number | 分销零售 |
|use | number | 使用 |
|scrap | number | 废弃处置 |
|total | number | 总计 |
|`product_data` | `array` | `产品数据` |
|product_name | string | 产品名称 |
|emissions | string | 排放量 |
|reduction | number | 增加的碳排放量 |
|reduction_rate | string | 增加的碳排放比例 |
|<font color="#dd0000">raw</font> | <font color="#dd0000">array</font> | <font color="#dd0000">原材料分析</font> |
|<font color="#dd0000">raw_comon</font> | <font color="#dd0000">array</font> | <font color="#dd0000">共有材料排放分析</font> |
|`raw_table` | `array` | `原材料表格` |
|`raw_chart` | `array` | `原材料图表` |
|id |int   | 原料id  |
|name | string | 原料名称 |
|emissions | number | 排放量 |
|emission_rate_num | number | 排放比例数值 |
|emission_rate | string | 排放比例 |
|<font color="#dd0000">energy</font> | <font color="#dd0000">array</font> | <font color="#dd0000">能耗分析</font> |
|<font color="#dd0000">energy_comon</font> | <font color="#dd0000">array</font> | <font color="#dd0000">共有能耗排放分析</font> |
|`energy_table` | `array` | `能耗表格` |
|`energy_chart` | `array` | `能耗图表` |
|<font color="#dd0000">supplier</font> | <font color="#dd0000">array</font> | <font color="#dd0000">供应商分析</font> |
|`supplier_table` | `array` | `供应商表格` |
|`supplier_chart` | `array` | `供应商图表` |
|id |int   | 原料id  |
|name | string | 供应商名称 |
|count | number | 供应产品数量 |
|emissions | number | 排放量 |
|emission_rate_num | number | 排放比例数值 |
|emission_rate | string | 排放比例 |



|id |int   | 主键id  |
|product_name | string | 产品名称 |
|product_no | string | 产品编码 |
|product_spec | string | 产品规格 |
|unit_str | string | 单位 |
|coefficient | int | 产品碳排放系数 |
|week | string | 核算周期 |
|tree | array | 碳排放信息 |
|name | string | 名称 |
|carbon_value | string | 排放量 |
|carbon_rate | string | 碳排放贡献比 |
|raw_table | array | 原材料分析表格 |
|raw_chart | array | 原材料饼状图 |
|emission_rate_num | number | 排放量百分比数值 |
|emission_rate | string | 排放量百分比 |
|carbon_value | string | 排放量 |
|carbon_rate | string | 碳排放贡献比 |
|energy_table | array | 能耗分析表格 |
|energy_chart | array | 能耗饼状图 |
|supplier_table | array | 供应商分析表格 |
|rsupplier_chart | array | 供应商饼状图 |



本周完成工作:
完成碳数据模块-产品排放分析接口开发
本周工作总结:
1.产品排放分析列表接口开发及联调
2.产品排放分析对比列表接口开发及联调
3.产品排放分析产品详情接口开发及联调
4.产品排放分析接入接口开发及联调
5.产品排放分析产品对比删除接口开发及联调
6.产品排放分析产品数据对比接口开发
下周工作计划:
1.产品排放分析产品数据对比接口联调
2.合作端接口开发






CREATE TABLE `jy_cooperate_user` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NULL DEFAULT NULL COMMENT '用户名' COLLATE 'utf8mb4_general_ci',
    `password` VARCHAR(50) NULL DEFAULT NULL COMMENT '密码' COLLATE 'utf8mb4_general_ci',
    `token` VARCHAR(200) NULL DEFAULT NULL COMMENT '令牌' COLLATE 'utf8mb4_general_ci',
    `state` INT(2) NULL DEFAULT NULL COMMENT '状态',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='合作端用户表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=194
;


//redis

$str = 'value1aaaa';
$expire_time = 1 * 24 * 60 * 60;
$redis = new Redis(Config::get('cache.stores.redis'));
$str = self::encrypt($str, 'E');
$str = self::encrypt($str, 'D');
$redis->set('token', $str, $expire_time);

return json(['code'=>200, 'message'=>$redis->get('token'), 'aa'=>Config::get('cache.stores.redis')]);


                if ($num > 0) {
                    // 产品1相较于产品2的表个数据
                    $list_new['overview'][2][$stage_key] = $list_new['overview'][$key - 1][$stage_key] - $list_new['overview'][$key][$stage_key];
                    $list_new['overview'][2]['total'] = $list_new['overview'][$key - 1]['total'] - $list_new['overview'][$key]['total'];

                    $list_new['overview'][3][$stage_key] = ($list_new['overview'][$key - 1][$stage_key] == 0) ? $list_new['overview'][$key - 1][$stage_key] : $list_new['overview'][2][$stage_key] / $list_new['overview'][$key - 1][$stage_key];
                    $list_new['overview'][3][$stage_key] = $list_new['overview'][3][$stage_key] * 100;
                    $list_new['overview'][3][$stage_key] = $list_new['overview'][3][$stage_key] . '%';

                    $list_new['overview'][3]['total'] = ($list_new['overview'][$key - 1]['total'] == 0) ? $list_new['overview'][$key - 1]['total'] : $list_new['overview'][2]['total'] / $list_new['overview'][$key - 1]['total'];
                    $list_new['overview'][3]['total'] = $list_new['overview'][3]['total'] * 100;

                    // 产品2相较于产品1共增加的碳排放量和增排比例的描述
                    $list_new['overview'][4][$key - 1]['product_name'] = $product[$key - 1];
                    $list_new['overview'][4][$key - 1]['emissions'] = $emissions[$key - 1] . 'kgCO2e';
                    $list_new['overview'][4][$key]['product_name'] = $product[$key];
                    $list_new['overview'][4][$key]['emissions'] = $emissions[$key] . 'kgCO2e';
                    $list_new['overview'][4]['reduction'] = 0 - $list_new['overview'][2]['total'];
                    $list_new['overview'][4]['reduction_rate'] = 0 - $list_new['overview'][3]['total'];

                    $list_new['overview'][3]['total'] = $list_new['overview'][3]['total'] . '%';
                    $list_new['overview'][4]['reduction_rate'] = $list_new['overview'][4]['reduction_rate'] . '%';
                }




##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|organization_name |string   | 组织名称  |
|attestation_type |int   | 认证类型id  |
|attestation_type_name | string | 认证类型名称 |
|product_name | string | 产品名称 |
|product_no | string | 产品编码 |
|status | int | 状态id |
|status_name | string | 状态名称 |
|create_time | string | 创建时间 |
|username | string | 修改人名称 |




ParamModel::STAGE_TYPE_RAW_CATEGORY_TRANSPORT
ParamModel::STAGE_TYPE_MANUFACTURING_CATEGORY_TRANSPORT
ParamModel::STAGE_TYPE_DISTRIBUTION_TRANSPORT
ParamModel::STAGE_TYPE_USE_TRANSPORT
ParamModel::STAGE_TYPE_SCRAP_TRANSPORT


    'compar'：[{
        "id": 17,
        "err_status": 3,
        "err_msg": "不通过"
    }
    {
        "id": 18,
        "err_status": 3,
        "err_msg": "不通过"
    }
    {
        "id": 23,
        "err_status": 2,
        "err_msg": "通过"
    }]


##### 返回参数说明 

|参数名|类型|说明|
|:-----  |:-----|----- |
|id |int   | 主键id  |
|organization_name |string   | 组织名称  |
|attestation_type |int   | 认证类型id  |
|attestation_type_name | string | 认证类型名称 |
|product_name | string | 产品名称 |
|product_no | string | 产品编码 |
|status | int | 状态id |
|status_name | string | 状态名称 |
|create_time | string | 创建时间 |
|username | string | 修改人名称 |
|report_files | array | 报告文件 |
|certificate_files | array | 证书文件 |
|status_select_map | array | 状态选择map |
|auth_select_map | array | 认证类型选择map |


本周完成工作:
完成产品排放分析产品数据对比接口联调、合作端-认证管理开发（80%）
本周工作总结:
1.产品排放分析产品数据对比接口联调
2.合作端-上传下载接口开发
3.合作端-认证管理列表接口开发
4.合作端-认证查看接口开发
5.合作端-认证数据及材料接口开发
6.合作端-线上核查接口开发
6.合作端-现场核查接口开发
6.合作端-派发证书接口开发
6.合作端-预约现场核查接口开发
6.合作端-数据正确错误及批量处理接口开发
下周工作计划:
1.合作端登录接口修改
2.合作端测试联调

1|4|8|17|18|19|25|35|38|41|2#4|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,11,12|10#4|11#4|12#4|13#4|14#3|15#9|16#10|20#4|21#4|22#4|23#4|24#4|26#4|27#4|28#4,10|29#4|30#4|31#4|32#4|33#4|34#4|36#4|37#4|39#4|40#4|42#4|43#4|44#4|51#4|52#4|53#4|54#4|56|57#1,2,3,4,9,10,11|58#1,2,3,4,9,10,11|59|60#1,2,3,4|61#4|62#4|63#4|64#4|65#4|66#4|67#4|68#4|69#4|70#4|71#4|72#4|73#4|74#4|75#4




c.verify_end_time,c.verify_mobile,c.retinue,
u.username,c.verify_name,c.data_from,c.create_time,c.data_from





现场核查 退回补充材料




1 待付款 STATUS_TO_PAY
2 待认证 STATUS_TO_CERTIFIED
3 待线上核查 STATUS_TO_CHECK_ONLINE
4 待现场核查 STATUS_TO_CHECK_ONSITE
5 已驳回 STATUS_REJECTED
6 现场核查中 STATUS_ONSITE_CHECKING
7 认证完成 STATUS_COMPLETED
8 待补充材料 STATUS_TO_SUPPLEMENTED
-- 9 认证失败 STATUS_FAILED
10 证书已派发 STATUS_DISTRIBUTED
-- 11 服务终止 STATUS_SERVICE_END
12 已取消 STATUS_CANCELLED
-- 13 已退款 STATUS_REFUNDED

不可修改 6，7，10

待线上

http://47.122.18.241:18000/certificationdownload?id=1332,1333


http://47.122.18.241:8080/admin_cooperate_uploads/20230330/80b887778f7e00d1b791f34eca33e090.jpg



本周完成工作:
完成合作端-认证管理开发、合作端-认证管理联调（80%）
本周工作总结:
1.合作端-上传下载接口测试联调
2.合作端-认证管理列表接口测试联调
3.合作端-认证查看接口测试联调
4.合作端-认证数据及材料接口测试联调
5.合作端-线上核查接口测试联调
6.合作端-现场核查接口测试联调
7.合作端-派发证书接口开发测试联调
8.合作端-预约现场核查接口开发测试联调
9.合作端-数据正确错误及批量处理接口开发测试联调
下周工作计划:
1.合作端接口bug修改
2.合作端测试联调



1 待付款 STATUS_TO_PAY
2 待认证 STATUS_TO_CERTIFIED
3 待线上核查 STATUS_TO_CHECK_ONLINE
4 待现场核查 STATUS_TO_CHECK_ONSITE
5 已驳回 STATUS_REJECTED
6 现场核查中 STATUS_ONSITE_CHECKING
7 认证完成 STATUS_COMPLETED
8 待补充材料 STATUS_TO_SUPPLEMENTED
-- 9 认证失败 STATUS_FAILED
10 证书已派发 STATUS_DISTRIBUTED
-- 11 服务终止 STATUS_SERVICE_END
12 已取消 STATUS_CANCELLED
-- 13 已退款 STATUS_REFUNDED


20230403 
超级管理员备份
1|4|8|17|18|19|25|35|38|41|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,11,12|10#4|11#4|12#4|13#4|14#3|15#9|16#10|20#4|21#4|22#4|23#4|24#4|26#4|27#4|28#4,10|29#4|30#4|31#4|32#4|33#4|34#4|36#4|37#4|39#4|40#4|42#4|43#4|44#4|51#4|52#4|53#4|54#4|56|57#1,2,3,4,9,10,11|58#1,2,3,4,9,10,11|59|60#1,2,3,4|61#4|62#4|63#4|64#4|65#4|66#4|67#4|68#4|69#4|70#4|71#4|72#4|73#4|74#4|75#4|76#4|77#4|78#4|79#4|80#4|81#4

1|4|8|13|17|18|19|25|35|38|41|45|56|59|76|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17,18|11#1,2,3,4,10|12#19,20,21|14#3|15#9|16#10|20#4,5,6,24,25,37|21#1,3,4,5,6,11,26,27|22#4,19,20,21|23#4,5,6,37|24#1,2,3,4,11,26,28,38|26#4|27#4|28#4|29#4|30#4|31#4|32#4|33#4|34#4|36#1,2,3,4,5,6,9,10,11|37#4|39#4|40#2,4,9,22,23|42#4|43#4|44#4|46#4|47#4|51#4|52#4|53#1,2,3,4|54#4|55#4|57#1,2,3,4|58#1,2,3,4|60#1,2,3,4|61#4|62#3|63#4|64#4|65#4|66#4|67#4,31|68#3|69#1|70#1|71#4,29,30|72#4|73#3,4|74#4|75#1,3,4|77#32|78#2,4,33,34,35,36|79#1|80#3|81#4
市场人员
1|2#4|63#4|64#4|65#4|66#4|4|5#4|6#4|8|17|18|19|38|76|2#4|9#4|10#4|11#4|12#4|13#4|20#4|21#4|22#4|23#4|24#4|40#4|67#4|68#4|69#4|71#4|72#4|73#4|74#4|77#4|78#4|79#4|80#4|81#4|53#4|54#4

{
"AA":"certification",
"BB":"certificaiton"}

    /**
     * 来源类型-自产
     */
    CONST SOURCE_TYPE_SELF = 1;

    /**
     * 来源类型-外采
     */
    CONST SOURCE_TYPE_EXTERNAL = 2;

    /**
     * 匹配类型-因子
     */
    CONST MATCH_TYPE_FACTOR = 1;

    /**
     * 匹配类型-产品
     */
    CONST MATCH_TYPE_PRODUCT = 2;

供应商产品 - source = 2 match_type = 2 
接口：api/supplycustomerproduct
核算产品 - source = 1 match_type = 2 
接口：productcalculate
因子产品 - match_type = 1 
接口：factor

测试人员
1|4|17|18|19|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17|11#4|12#4|13#4|14#3|15#9|16#10|20#4|21#4|22#4|23#4|24#4|53#4|54#4
1|4|8|17|18|19|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17|11#4|12#4|13#4|14#3|15#9|16#10|20#4|21#4|22#4|23#4|24#4|53#4|54#4



本周完成工作:
完成合作端-认证管理联调（100%）、SaaS端按钮权限
本周工作总结:
1.合作端-上传下载接口测试联调
2.SaaS端-产品核算选择因子快照修改
3.SaaS端按钮权限

下周工作计划:
1.合作端接口bug修复
2.SaaS端接口bug修复


组织碳核算QA
1. 下载管理取值范围
2. 减排场景-确定值、区间值 总减排量、单位减排量
3. 排放基准管理-设定类型：单一年份、多年平均
4. 排放源管理


组织碳核算模块
1. 碳核算管理
2. 数据管理
3. 核算报告
4. 设备管理
CREATE TABLE `jy_facility` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `facility_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '产品名称' COLLATE 'utf8mb4_general_ci',
    `facility_no` VARCHAR(100) NULL DEFAULT NULL COMMENT '产品编号' COLLATE 'utf8mb4_general_ci',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `is_del` TINYINT(1) NULL DEFAULT '1' COMMENT '\r\n是否删除，1-未删除2-已删除\r\n\r\n',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='设备表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_gwp` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '温室气体名称' COLLATE 'utf8mb4_general_ci',
    `short_name` VARCHAR(255) NULL DEFAULT NULL COMMENT '温室气体类型' COLLATE 'utf8mb4_general_ci',
    `state` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '是否启用 1是 2否',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='GWP表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

请选择
IPCC第六次评估报告（IPCC WGI Sixth Assessment Report.IPCC.2021）
IPCC第五次评估报告（IPCC WGI Fifth Assessment Report.IPCC.2013）
IPCC第四次评估报告（IPCC WGI Fourth Assessment Report.IPCC.2007）

CREATE TABLE `jy_calculate_model` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `model_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '产品名称' COLLATE 'utf8mb4_general_ci',
    `describtion` VARCHAR(100) NULL DEFAULT NULL COMMENT '产品编号' COLLATE 'utf8mb4_general_ci',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `gwp_id` INT(11) NULL DEFAULT NULL COMMENT 'GWP Id',
    `is_del` TINYINT(1) NULL DEFAULT '1' COMMENT '\r\n是否删除，1-未删除2-已删除\r\n\r\n',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='核算模型表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;


1|4|8|13|17|18|19|25|35|38|41|45|56|59|76|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17,18|11#1,2,3,4,10|12#19,20,21|14#3|15#9|16#10|20#4,5,6,24,25,37|21#1,3,4,5,6,11,26,27|22#4,19,20,21|23#4,5,6,37|24#1,2,3,4,11,26,28,38|26#4|27#4|28#4|29#4|30#4|31#4|32#4|33#4|34#4|36#1,2,3,4,5,6,9,10,11|37#4|39#4|40#2,4,9,22,23|42#4|43#4|44#4|46#4|47#4|51#4|52#4|53#1,2,3,4|54#4|55#4|57#1,2,3,4|58#1,2,3,4|60#1,2,3,4|61#4|62#3|63#4|64#4|65#4|66#4|67#4,31|68#3|69#1|70#1|71#4,29,30|72#4|73#3,4|74#4|75#1,3,4|77#32|78#2,4,33,34,35,36|79#1|80#3|81#4
1|8|13#1,2,4,39|17|18|19|35|38|41|45|56|59|4|25|76|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17,18|11#1,2,3,4,10|12#19,20,21|20#4,5,6,24,25,37|21#1,3,4,5,6,11,26,27|22#4,19,20,21|23#4,5,6,37|24#1,2,3,4,11,26,28,38|26#4|27#4|28#4|29#4|30#4|31#4|32#4|33#4|34#4|36#1,2,3,4,5,6,9,10,11|37#4|39#4|40#2,4,9,22,23|42#4|43#4|44#4|46#4|47#4|51#4|52#4|53#1,2,3,4|54#4|55#4|57#1,2,3,4|58#1,2,3,4|60#1,2,3,4|61#4|62#3|63#4|64#4|65#4|66#4|67#4,31|68#3|69#1|70#1|71#4,29,30|72#4|73#3,4|74#4|75#1,3,4|77#32|78#33|79#1|80#3|81#4


INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (55, '碳标签', 'carbonlabel', 'views/carbonlabel', NULL, 'CarbonLabel', NULL, 0, 1, NULL, 13, NULL, NULL, 1, '4', NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (56, '数据字典', '/dictionary', '#', '/dictionary/unitmanagement', 'Dictionary', 'ep:menu', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (59, '消息', '/notices', '#', '/notices/noticesmanagement', 'Notices', 'fe:notice-push', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);


80 JY_MENU 20230413

INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (41, '碳资产', '/carbonassets', '#', '/carbonassets', 'CarbonAssets', 'grommet-icons:strike-through', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (45, '降碳任务', '/carbontask', '#', '/carbontask', 'CarbonTask', 'grommet-icons:task', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (56, '数据字典', '/dictionary', '#', '/dictionary/unitmanagement', 'Dictionary', 'ep:menu', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (59, '消息', '/notices', '#', '/notices/noticesmanagement', 'Notices', 'fe:notice-push', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (25, '组织碳核算', '/calculate', '#', '/calculate', 'Calculate', 'material-symbols:calculate-outline', 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jy_menu` (`id`, `title`, `path`, `component`, `redirect`, `name`, `icon`, `alwaysShow`, `showInList`, `noCache`, `pid`, `admin_user_id`, `sort`, `hidden`, `menu_data_group_id`, `create_time`, `modify_time`) VALUES (39, '组织排放分析', 'organizationemissions', 'views/carbondata/organizationemissions', NULL, 'OrganizationEmissions', NULL, 1, 1, NULL, 38, NULL, NULL, NULL, '4', NULL, NULL);


CREATE TABLE `jy_calculate_model_emission` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `emission_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '排放源名称' COLLATE 'utf8mb4_general_ci',
    `source_id` VARCHAR(50) NULL DEFAULT NULL COMMENT '排放源id' COLLATE 'utf8mb4_general_ci',
    `active` VARCHAR(100) NULL DEFAULT NULL COMMENT '排放设施/活动' COLLATE 'utf8mb4_general_ci',
    `ghg_cate` VARCHAR(100) NULL DEFAULT NULL COMMENT 'ghg分类' COLLATE 'utf8mb4_general_ci',
    `iso_cate` VARCHAR(100) NULL DEFAULT NULL COMMENT 'iso分类' COLLATE 'utf8mb4_general_ci',
    `calculate_model_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型Id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='核算模型关联排放源表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_emission_source_facility` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `facility_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '设备名称' COLLATE 'utf8mb4_general_ci',
    `facility_no` VARCHAR(100) NULL DEFAULT NULL COMMENT '设备编号' COLLATE 'utf8mb4_general_ci',
    `calculate_model_emission_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型关联排放源Id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='排放源关联设备表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;


1. 核算报告





本周完成工作:
完成系统设置接口修改、组织碳核算模块接口开发（20%）
本周工作总结:
1.修改操作日志按照组织区分
2.修改文件下载按照组织区分
3.修改系统设置按照组织区分
4.组织碳核算-设备管理开发
5.组织碳核算-核算模型管理开发

下周工作计划:
1.组织碳核算-碳核算管理开发
2.组织碳核算-数据管理开发
3.组织碳核算-核算报告开发
4.组织碳核算接口联调测试


CREATE TABLE `jy_organization_calculate` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织id',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `calculate_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '核算名称' COLLATE 'utf8mb4_general_ci',
    `calculate_year` VARCHAR(4) NULL DEFAULT NULL COMMENT '核算年度' COLLATE 'utf8mb4_general_ci',
    `contrast_year` VARCHAR(4) NULL DEFAULT NULL COMMENT '对比年度' COLLATE 'utf8mb4_general_ci',
    `emissions` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放量',
    `state` TINYINT(1) NULL DEFAULT '1' COMMENT '状态，1-核算中，2-已完成',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='组织核算表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_organization_calculate_detail` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `calculate_id` INT(11) NULL DEFAULT NULL COMMENT '核算id',
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织id',
    `parent_organization_id` INT(11) NULL DEFAULT NULL COMMENT '父级组织id',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `calculate_year` VARCHAR(4) NULL DEFAULT NULL COMMENT '核算年' COLLATE 'utf8mb4_general_ci',
    `calculate_month` VARCHAR(2) NULL DEFAULT NULL COMMENT '核算月' COLLATE 'utf8mb4_general_ci',
    `emissions` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放量',
    `state` TINYINT(1) NULL DEFAULT '1' COMMENT '状态，1-待收集2-待提交3-工厂审核中4-分公司审核中5-母公司审核中6-审核通过7-审核不通过',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='组织核算详情表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_organization_calculate_detail_emission` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `organization_calculate_detail_id` INT(11) NULL DEFAULT NULL COMMENT '组织核算详情Id',
    `emission_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '排放源名称' COLLATE 'utf8mb4_general_ci',
    `source_id` VARCHAR(50) NULL DEFAULT NULL COMMENT '排放源id' COLLATE 'utf8mb4_general_ci',
    `active` VARCHAR(100) NULL DEFAULT NULL COMMENT '排放设施/活动' COLLATE 'utf8mb4_general_ci',
    `ghg_cate` INT(11) NULL DEFAULT NULL COMMENT 'ghg分类',
    `iso_cate` INT(11) NULL DEFAULT NULL COMMENT 'iso分类',
    `ghg_type` INT(11) NULL DEFAULT NULL COMMENT 'ghg类别',
    `iso_type` INT(11) NULL DEFAULT NULL COMMENT 'iso类别',
    `active_data` VARCHAR(255) NULL DEFAULT NULL COMMENT '活动数据记录方式' COLLATE 'utf8mb4_general_ci',
    `active_unit` VARCHAR(255) NULL DEFAULT NULL COMMENT '活动数据单位' COLLATE 'utf8mb4_general_ci',
    `active_department` VARCHAR(255) NULL DEFAULT NULL COMMENT '活动数据保存部门' COLLATE 'utf8mb4_general_ci',
    `active_type` INT(11) NULL DEFAULT NULL COMMENT '活动数据类别',
    `active_score` VARCHAR(255) NULL DEFAULT NULL COMMENT '活动数据评分' COLLATE 'utf8mb4_general_ci',
    `unitary_ratio` DECIMAL(30,10) NULL DEFAULT NULL COMMENT '单位换算比',
    `factor_type` INT(11) NULL DEFAULT NULL COMMENT '排放因子类别',
    `factor_score` VARCHAR(255) NULL DEFAULT NULL COMMENT '排放因子评分' COLLATE 'utf8mb4_general_ci',
    `factor_source` VARCHAR(255) NULL DEFAULT NULL COMMENT '排放因子来源' COLLATE 'utf8mb4_general_ci',
    `year` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布年份' COLLATE 'utf8mb4_general_ci',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `calculate_model_id` (`calculate_model_id`) USING BTREE
)
COMMENT='组织核算详情排放源表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_organization_calculate_detail_facility` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `organization_calculate_detail_id` INT(11) NULL DEFAULT NULL COMMENT '组织核算详情Id',
    `organization_calculate_detail_emission_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型关联排放源Id',
    `facility_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '设备名称' COLLATE 'utf8mb4_general_ci',
    `facility_no` VARCHAR(100) NULL DEFAULT NULL COMMENT '设备编号' COLLATE 'utf8mb4_general_ci',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `organization_calculate_detail_id` (`organization_calculate_detail_id`) USING BTREE,
    INDEX `organization_calculate_detail_emission_id` (`organization_calculate_detail_emission_id`) USING BTREE
)
COMMENT='组织核算详情设备表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;

CREATE TABLE `jy_organization_calculate_detail_emission_gas` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `emi_id` INT(10) NULL DEFAULT NULL COMMENT '组织核算详情排放源id',
    `gas` VARCHAR(255) NULL DEFAULT NULL COMMENT '温室气体' COLLATE 'utf8mb4_general_ci',
    `gas_type` VARCHAR(255) NULL DEFAULT NULL COMMENT '气体类型' COLLATE 'utf8mb4_general_ci',
    `factor_value` DECIMAL(30,6) NULL DEFAULT NULL COMMENT '因子数值',
    `molecule` VARCHAR(255) NULL DEFAULT NULL COMMENT '分子单位' COLLATE 'utf8mb4_general_ci',
    `denominator` VARCHAR(255) NULL DEFAULT NULL COMMENT '分母单位' COLLATE 'utf8mb4_general_ci',
    `create_time` DATETIME NULL DEFAULT NULL,
    `create_by` INT(10) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='组织核算详情排放源气体表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;

CREATE TABLE `jy_organization_calculate_report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织id',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `organization_calculate_id` INT(11) NULL DEFAULT NULL COMMENT '组织核算id',
    `data_quality_id` INT(11) NULL DEFAULT NULL COMMENT '数据质量控制计划版本id',
    `reduction_id` VARCHAR(100) NULL DEFAULT NULL COMMENT '减排id' COLLATE 'utf8mb4_general_ci',
    `report_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '报告名称' COLLATE 'utf8mb4_general_ci',
    `username` VARCHAR(100) NULL DEFAULT NULL COMMENT '报告负责人姓名' COLLATE 'utf8mb4_general_ci',
    `telephone` CHAR(11) NULL DEFAULT NULL COMMENT '报告负责人电话' COLLATE 'utf8mb4_general_ci',
    `email` VARCHAR(100) NULL DEFAULT NULL COMMENT '告负责人邮件' COLLATE 'utf8mb4_general_ci',
    `exempte_description` LONGTEXT NULL DEFAULT NULL COMMENT '排放源免除说明' COLLATE 'utf8mb4_general_ci',
    `change_description` LONGTEXT NULL DEFAULT NULL COMMENT '量化方法变更说明' COLLATE 'utf8mb4_general_ci',
    `discharge_description` LONGTEXT NULL DEFAULT NULL COMMENT '生物质相关排放说明' COLLATE 'utf8mb4_general_ci',
    `otherdescription` LONGTEXT NULL DEFAULT NULL COMMENT '其他情况说明' COLLATE 'utf8mb4_general_ci',
    `file_id` INT(11) NULL DEFAULT NULL COMMENT '文件id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='组织核算报告表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;



1|4|8|17|18|19|25|35|38|41|45|76|2#4,13|5#1,5,6,7,8|6#1,2,3,4|7#1,2,3,4|9#1,2,3,4,5,6,10,14|10#1,2,3,4,15,16,17,18|11#1,2,3,4,10|12#19,20,21|13#1,2,4,39|14#3|15#9|16#10|20#4,5,6,24,25,37|21#1,3,4,5,6,11,26,27|22#4,19,20,21|23#4,5,6,37|24#1,2,3,4,11,26,28,38|26#4|27#4|28#4|29#1,2,3,4,41,42|30#4|31#4|32#4|33#1,2,3,4|34#4|36#1,2,3,4,5,6,9,10,11|37#4|39#4|40#2,4,9,22,23|42#4|43#4|44#4|46#4|47#4|51#4|52#4|53#1,2,3,4|54#4|63#4|64#4|65#4|66#4|67#4,31|68#3|69#1|71#4,29,30|72#4|73#3,4|74#4|75#1,3,4|77#32|78#2,4,33,34,35,36|79#1|80#3|81#4|83#1,3,4|84#1,3,4|85#1,2,4,40|86#1,2,3,4|87#4|88#1,2,3,4|89#1,3,4|90#4|91#4



本周完成工作:
完成主页待办事项接口、消息管理接口修改、组织碳核算开发（70%）
本周工作总结:
1.组织碳核算-设备管理开发
2.组织碳核算-核算模型管理开发
3.组织碳核算-碳核算管理开发
4.组织碳核算-数据管理开发（70%）
5.主页待办事项接口开发
6.修改消息管理

下周工作计划:
1.组织碳核算-数据管理开发
2.组织碳核算-核算报告开发
3.组织碳核算接口联调测试



     * @author wuyinghua

CREATE TABLE `jy_organization_calculate_report` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织id',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `organization_calculate_id` INT(11) NULL DEFAULT NULL COMMENT '组织核算id',
    `data_quality_id` INT(11) NULL DEFAULT NULL COMMENT '数据质量控制计划版本id',
    `reduction_id` VARCHAR(100) NULL DEFAULT NULL COMMENT '减排id' COLLATE 'utf8mb4_general_ci',
    `report_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '报告名称' COLLATE 'utf8mb4_general_ci',
    `username` VARCHAR(100) NULL DEFAULT NULL COMMENT '报告负责人姓名' COLLATE 'utf8mb4_general_ci',
    `telephone` CHAR(11) NULL DEFAULT NULL COMMENT '报告负责人电话' COLLATE 'utf8mb4_general_ci',
    `email` VARCHAR(100) NULL DEFAULT NULL COMMENT '告负责人邮件' COLLATE 'utf8mb4_general_ci',
    `exempte_description` LONGTEXT NULL DEFAULT NULL COMMENT '排放源免除说明' COLLATE 'utf8mb4_general_ci',
    `change_description` LONGTEXT NULL DEFAULT NULL COMMENT '量化方法变更说明' COLLATE 'utf8mb4_general_ci',
    `discharge_description` LONGTEXT NULL DEFAULT NULL COMMENT '生物质相关排放说明' COLLATE 'utf8mb4_general_ci',
    `otherdescription` LONGTEXT NULL DEFAULT NULL COMMENT '其他情况说明' COLLATE 'utf8mb4_general_ci',
    `file_id` INT(11) NULL DEFAULT NULL COMMENT '文件id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='组织核算报告表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=2
;


CREATE TABLE `jy_organization_calculate_report_reduction` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `organization_calculate_report_id` INT(11) NULL DEFAULT NULL COMMENT '核算报告id',
    `name` VARCHAR(255) NULL DEFAULT NULL COMMENT '减排场景名称' COLLATE 'utf8mb4_general_ci',
    `type` VARCHAR(12) NULL DEFAULT NULL COMMENT '减排量量化类型' COLLATE 'utf8mb4_general_ci',
    `desc` VARCHAR(255) NULL DEFAULT NULL COMMENT '减排场景描述' COLLATE 'utf8mb4_general_ci',
    `total_desc` VARCHAR(255) NULL DEFAULT NULL COMMENT '总减排量描述' COLLATE 'utf8mb4_general_ci',
    `reduction_type` INT(1) NULL DEFAULT NULL COMMENT '减排量类型',
    `total_reduction_low` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '总减排量低值',
    `total_reduction_high` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '总减排量高值',
    `total_unit` TINYINT(11) NULL DEFAULT NULL COMMENT '总减排量单位',
    `unit_desc` VARCHAR(255) NULL DEFAULT NULL COMMENT '单位减排量描述' COLLATE 'utf8mb4_general_ci',
    `unit_type` INT(1) NULL DEFAULT NULL COMMENT '单位减排量类型',
    `unit_low` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '单位减排量低值',
    `unit_high` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '单位减排量高值',
    `molecule` VARCHAR(255) NULL DEFAULT NULL COMMENT '分子单位' COLLATE 'utf8mb4_general_ci',
    `denominator` VARCHAR(255) NULL DEFAULT NULL COMMENT '分母单位' COLLATE 'utf8mb4_general_ci',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建者id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    `modify_by` INT(10) NULL DEFAULT NULL COMMENT '修改人',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='h核算报告关联减排场景表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=2
;


http://jayee.tpddns.cn:9046/gitlab-instance-ac000afe/jayee-api.git




本周完成工作:
完成系统登录修改、产品碳足迹产品核算修改、组织碳核算接口修改
本周工作总结:
1.系统登录修改
2.产品碳足迹产品核算修改
3.组织碳核算-碳核算管理修改
4.组织碳核算-数据管理开修改
5.组织碳核算-核算报告（GHG、ISO）开发

下周工作计划:
1.组织碳核算-核算报告（GHG、ISO）开发
2.组织碳核算接口联调测试



http://47.122.18.241:81/template/home/捷亦云系统操作手册.pdf


0 + CAST(jd.total AS CHAR) active_value


本周完成工作:
完成产品碳足迹产品核算修改、组织碳核算-核算报告(ISO、GHG) WORD版开发
本周工作总结:
1.组织碳核算-碳核算管理修改
2.组织碳核算-数据管理修改
3.组织碳核算-核算报告（GHG、ISO）开发

下周工作计划:
1.组织碳核算接口联调测试、修改



Redis 安装到本地
打开cmd命令窗口，使用命令进行安装和注册redis到window服务

安装命令：redis-server.exe --service-install redis.windows.conf --loglevel verbose

启动服务命令：redis-server.exe  --service-start

关闭服务命令：redis-server.exe  --service-stop

自测——组织碳核算

1. 复制核算模型，排放源关联的设备没有复制过来
2. 碳核算管理，组织十二个月数据中有一条已经提交后 是否还能全部发起
3. 核算文件中 “加权平均评分总计” 会大于36


CREATE TABLE `jy_calculate_model_emission_aaaa` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `calculate_model_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型Id',
    `emission_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型关联排放源Id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `calculate_model_id` (`calculate_model_id`) USING BTREE
)
COMMENT='核算模型关联排放源表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;


CREATE TABLE `jy_calculate_model_emission_facility_aaaa` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `calculate_model_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型Id',
    `emission_id` INT(11) NULL DEFAULT NULL COMMENT '核算模型关联排放源Id',
    `facility_id` INT(11) NULL DEFAULT NULL COMMENT '排放源关联设备Id',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `emission_id` (`emission_id`) USING BTREE,
    INDEX `calculate_model_id` (`calculate_model_id`) USING BTREE
)
COMMENT='排放源关联设备表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;


http://47.122.18.241:9999/#/calculate/calculatemanagementseedata?id=3010&detailsid=178&types=Nodatamanager&state_name=%E5%BE%85%E6%94%B6%E9%9B%86


本周完成工作:
完成组织碳核算测试修改、合作端修改
本周工作总结:
1.组织碳核算-碳核算管理修改
2.组织碳核算-数据管理修改
3.组织碳核算-核算报告修改

下周工作计划:
1.组织碳核算接口联调测试、修改
2.合作端修改
3.华能系统开发

snapshot

CREATE TABLE `jy_factor_snapshot` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `title` VARCHAR(1024) NULL DEFAULT NULL COMMENT '因子名称' COLLATE 'utf8mb4_general_ci',
    `factor_id` INT(11) NULL DEFAULT NULL COMMENT '因子ID',
    `language` VARCHAR(255) NULL DEFAULT NULL COMMENT '源语言' COLLATE 'utf8mb4_general_ci',
    `title_language` VARCHAR(255) NULL DEFAULT NULL COMMENT '名称（源语言）' COLLATE 'utf8mb4_general_ci',
    `model` VARCHAR(255) NULL DEFAULT NULL COMMENT '规格/型号' COLLATE 'utf8mb4_general_ci',
    `describtion` TEXT NULL DEFAULT NULL COMMENT '场景描述' COLLATE 'utf8mb4_general_ci',
    `mechanism` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布机构' COLLATE 'utf8mb4_general_ci',
    `mechanism_short` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布机构简称' COLLATE 'utf8mb4_general_ci',
    `grade` VARCHAR(255) NULL DEFAULT NULL COMMENT '因子评级' COLLATE 'utf8mb4_general_ci',
    `uncertainty` VARCHAR(255) NULL DEFAULT NULL COMMENT '不确定性' COLLATE 'utf8mb4_general_ci',
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织ID',
    `year` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布年份' COLLATE 'utf8mb4_general_ci',
    `country` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布国家/组织' COLLATE 'utf8mb4_general_ci',
    `region` VARCHAR(255) NULL DEFAULT NULL COMMENT '发布地区' COLLATE 'utf8mb4_general_ci',
    `file_name` TEXT NULL DEFAULT NULL COMMENT '来源文件名称' COLLATE 'utf8mb4_general_ci',
    `factor_value` DECIMAL(30,20) NULL DEFAULT NULL COMMENT '二氧化碳当量因子数值',
    `molecule` VARCHAR(255) NULL DEFAULT NULL COMMENT '二氧化碳当量分子单位' COLLATE 'utf8mb4_general_ci',
    `denominator` VARCHAR(255) NULL DEFAULT NULL COMMENT '二氧化碳当量分母单位' COLLATE 'utf8mb4_general_ci',
    `unit_type` VARCHAR(255) NULL DEFAULT NULL COMMENT '分母单位' COLLATE 'utf8mb4_general_ci',
    `factor_source` CHAR(1) NULL DEFAULT '1' COMMENT '来源:1-系统因子库,2-组织因子库' COLLATE 'utf8mb4_general_ci',
    `sort` INT(11) NULL DEFAULT NULL COMMENT '排序',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人id',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '修改人id',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    `f23` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
    `f24` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
    `status` TINYINT(1) NULL DEFAULT '1' COMMENT '状态1启用 2禁用',
    `is_del` TINYINT(1) NULL DEFAULT NULL COMMENT '是否删除1=>删除',
    `del_time` DATETIME NULL DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='因子快照表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;


CREATE TABLE `jy_supplier_product_snapshot` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `product_name` VARCHAR(100) NULL DEFAULT NULL COMMENT '\r\n产品名称' COLLATE 'utf8mb4_general_ci',
    `product_model` VARCHAR(150) NULL DEFAULT NULL COMMENT '产品型号' COLLATE 'utf8mb4_general_ci',
    `product_type` VARCHAR(150) NULL DEFAULT NULL COMMENT '产品类型' COLLATE 'utf8mb4_general_ci',
    `supplier_id` INT(11) NULL DEFAULT NULL COMMENT '供用商id',
    `auth` INT(1) NULL DEFAULT NULL COMMENT '是否第三方认证 1 有 2 无',
    `state` INT(1) NULL DEFAULT NULL COMMENT '\r\n状态\r\n\r\n',
    `file_url` VARCHAR(255) NULL DEFAULT NULL COMMENT '附件上传' COLLATE 'utf8mb4_general_ci',
    `create_by` INT(11) NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) NULL DEFAULT NULL COMMENT '更新人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '更新时间',
    `audit_state` TINYINT(2) UNSIGNED NULL DEFAULT NULL COMMENT '审核状态',
    `week_start` VARCHAR(7) NULL DEFAULT NULL COMMENT '核算周期开始时间' COLLATE 'utf8mb4_general_ci',
    `week_end` VARCHAR(7) NULL DEFAULT NULL COMMENT '核算周期结束时间' COLLATE 'utf8mb4_general_ci',
    `number` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '核算数量,会有小数点',
    `unit_type` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT '单位类型',
    `unit` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT '单位',
    `customer_id` VARCHAR(255) NULL DEFAULT NULL COMMENT '客户id，可多选用逗号分隔' COLLATE 'utf8mb4_general_ci',
    `is_del` TINYINT(1) UNSIGNED ZEROFILL NULL DEFAULT '0' COMMENT '是否被删除 0 未删除  1已删除',
    `emissions` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放量 ',
    `coefficient` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放因子数值（系数）',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='供用商产品快照表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `jy_supplier_product_discharge_snapshot` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `type_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '所属类型id',
    `name` VARCHAR(255) NULL DEFAULT '' COMMENT '排放源名称' COLLATE 'utf8mb4_general_ci',
    `loss_type` VARCHAR(255) NULL DEFAULT '' COMMENT '逸散类型/产品型号' COLLATE 'utf8mb4_general_ci',
    `value` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '逸散量/用量',
    `unit_type` VARCHAR(255) NULL DEFAULT '' COMMENT '单位类别名称' COLLATE 'utf8mb4_general_ci',
    `unit` VARCHAR(255) NULL DEFAULT '' COMMENT '单位' COLLATE 'utf8mb4_general_ci',
    `factor_name` VARCHAR(255) NULL DEFAULT '' COMMENT '排放因子名称' COLLATE 'utf8mb4_general_ci',
    `year` VARCHAR(255) NULL DEFAULT NULL COMMENT '排放因子发布年份' COLLATE 'utf8mb4_general_ci',
    `coefficient` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放因子数值（系数）',
    `factor_unit` VARCHAR(255) NULL DEFAULT NULL COMMENT '排放因子单位' COLLATE 'utf8mb4_general_ci',
    `factor_source` VARCHAR(255) NULL DEFAULT '' COMMENT '排放因子来源' COLLATE 'utf8mb4_general_ci',
    `supplier_product_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '客户产品id',
    `create_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    `is_del` TINYINT(1) UNSIGNED NULL DEFAULT '0' COMMENT '是否删除  0未删除 1已删除',
    `emissions` DECIMAL(20,6) NULL DEFAULT NULL COMMENT '排放量',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='供应商产品排放源快照表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;


http://jayee.tpddns.cn:9046 ->> http://192.168.1.45



http://47.122.18.241:889/static/%E7%A2%B3%E6%8E%92%E6%94%BE%E5%9B%A0%E5%AD%90%E5%AF%BC%E5%85%A5%E6%A8%A1%E6%9D%BF.xlsx



本周完成工作:
完成合作端修改、系统下载统一修改
本周工作总结:
1.合作端修改
2.系统下载统一修改，文件id 自增 变更为 uuid
3.SaaS端、合作端因子快照修改

下周工作计划:
1.系统下载统一修改
2.合作端修改



TP6 download() 函数输出文件流 header中的中文名乱码问题
【解决方案1】：
使用rawurlencode()根据RFC 3986对文件名进行编码就足够了

所以您需要做的就是将 header() 行更改为：

header("Content-Disposition: attachment; filename*=UTF-8''".rawurlencode($name));
直接回答问题：

格式正确，但是$name里面的文字需要用rawurlencode()编码。
rawurlencode() 成功了。



本周完成工作:
完成SaaS端新增页面、bug修复、华能系统-系统设置开发
本周工作总结:
1.SaaS端配合前端UI修改新增所需页面、修复bug
2.华能系统完成系统设置的开发

下周工作计划:
1.华能系统数据库设计
2.华能系统接口开发

=======================================================================================================================================================================================================================================================================================

CREATE TABLE `jy_forewarn_email` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `eamil` VARCHAR(255) NULL DEFAULT '' COMMENT '邮件' COLLATE 'utf8mb4_general_ci',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织ID',
    `create_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='预警邮件表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;

CREATE TABLE `jy_forewarn_detail` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `content` VARCHAR(255) NULL DEFAULT '' COMMENT '预警内容' COLLATE 'utf8mb4_general_ci',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织ID',
    `combination_id` INT(11) NULL DEFAULT NULL COMMENT '机组ID',
    `create_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='预警详情表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;

CREATE TABLE `jy_monitor_facility` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `facility_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `main_organization_id` INT(11) NULL DEFAULT NULL COMMENT '母公司id',
    `organization_id` INT(11) NULL DEFAULT NULL COMMENT '组织ID',
    `monitor_point_id` INT(11) NULL DEFAULT NULL COMMENT '监测点位ID',
    `ambient_humidness` VARCHAR(255) NULL DEFAULT '' COMMENT '环境温度' COLLATE 'utf8mb4_general_ci',
    `ambient_temperature` VARCHAR(255) NULL DEFAULT '' COMMENT '环境湿度' COLLATE 'utf8mb4_general_ci',
    `pressure` VARCHAR(255) NULL DEFAULT '' COMMENT '气压' COLLATE 'utf8mb4_general_ci',
    `gas_velocity` VARCHAR(255) NULL DEFAULT '' COMMENT '气体流速' COLLATE 'utf8mb4_general_ci',
    `co2_concentration` VARCHAR(255) NULL DEFAULT '' COMMENT 'CO2浓度' COLLATE 'utf8mb4_general_ci',
    `create_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '创建人',
    `modify_by` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT '修改人',
    `create_time` DATETIME NULL DEFAULT NULL COMMENT '创建时间',
    `modify_time` DATETIME NULL DEFAULT NULL COMMENT '修改时间',
    `state` INT(11) NULL DEFAULT NULL COMMENT '设备状态',
    PRIMARY KEY (`id`) USING BTREE
)
COMMENT='设备监测表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=1
;

华能表设计

一、在线监测

1. 在线监测-监测总览

summary

CO2排放量
度电碳排
负荷率
发电量
供热量
供热碳排强度

chart

1) 碳排数据

更新时间
昨日、今日
每个小时的数据

2）总碳排量

总碳排量
月度排放
月度累计排放

3）历史碳排放量

实际碳排
当年配额
预计配额

4）在线预警 

预警等级：蓝色预警、黄色预警、橙色预警
预警类型：实时流量异常预警、碳排浓度激增预警
监测设备
监测点位
监测详情
检测时间

2. 在线监测-在线总览-碳排数据

summary

当前烟气二氧化碳浓度
烟气流速
烟气流量
实时碳排强度
当前烟气二氧化碳浓度的百分比


chart

当日累计排放、当月、当年
小时排放、累计排放


3. 在线监测-在线总览-设备监测

集团->公司->工厂->监测点位

设备总数（正常、异常、离线）

设备列表：
设备名称
设备状态
环境湿度
环境温度
气压
气体流速
CO2e浓度

4. 在线监测-在线总览-预警详情

组织名称、组织类型、核算周期（开始-结束）
预警内容
预警工厂
预警机组
预警时间

5. 在线监测-在线总览-预警详情-预警邮件管理

邮件地址


二、数据分析

按月为维度，实际要统计到天
总碳排量
排放强度
发电量
负荷率
供热量
供热碳排强度

chart （同比、环比）

碳排放量
碳排趋势
发电碳排强度
供热碳排强度
发电量
供热量
负荷率


三、数据大屏

数据分成集团或工厂

1）机组统计

2）预警

3）发电监测

4）实时监测

5）资产配额



INSERT INTO `jy_unit` (`name`, `type_id`, `is_base`, `conversion_ratio`, `sort`, `state`, `create_by`, `modify_by`, `create_time`, `modify_time`) 
VALUES 
('W', 11, 1, 1, 1, 1, NULL, NULL, NULL, NULL),
('kW', 11, 2, 1000, 1, 1, NULL, NULL, NULL, NULL),
('米制马力', 11, 2, 735.4988, 1, 1, NULL, NULL, NULL, NULL);


1.00E-16

http://192.168.1.43:4000/#/calculate/calculatemanagementdatainfo?id=4678&ids=4678&data_type=1&type=details&types=toDataManagersEnterData&backtypes=formToDataManager&state_name=3



http://192.168.1.31/api/supplysupplierproductcustomerrequestfilllist?pageSize=10&pageIndex=1&product_name=&title=

http://192.168.1.31/api/supplysupplierproductcustomerrequestfilllist?pageSize=10&pageIndex=1&product_name=&title=


本周完成工作:
完成SaaS端新增页面、bug修复、华能系统配置
本周工作总结:
1.SaaS端配合前端UI修改新增所需页面、修复bug
2.华能系统-系统设置
3.华能系统-在线监测-预警邮件管理
4.华能系统-数据分析接口数据结构
5.华能系统-模板引擎

下周工作计划:
1.华能系统开发
2.对接系统设置接口


 0 + CAST(jf.ambient_humidness AS CHAR) ambient_humidness,
 0 + CAST(jf.ambient_temperature AS CHAR) ambient_temperature,
 0 + CAST(jf.pressure AS CHAR) pressure,
 0 + CAST(jf.gas_velocity AS CHAR) gas_velocity,
 0 + CAST(jf.co2_concentration AS CHAR) co2_concentration

1.1. 负荷（出力）系数：展示百分比，计算公式：负荷（出力）系数=发电量÷（机组容量*运行小时数）
1.2. 发电排放强度：单位：tCO2/MWh，计算公式：发电排放强度=该机组排放量÷供电量
1.3. 供热排放强度：单位：tCO2/GJ，计算公式：供热排放强度=该机组供热量÷供热量

 数据分析

今年总碳排量  SUM()   tCO2e
同比去年


<script src="https://gofly.vlkf.com/static/js/kefu-front.js"></script>
<script>
KEFU.init({
KEFU URL:"https://gofly. vikf.com"
KEFU KEFU ID:"taoshihan"
KEFU ENT:"5"
KEFU LANG:"cn"
KEFU SHOW TYFES:1.
KEFU AUTO OPEN:false,
})
</script>

<div class="left-msg"><div class="msg-title" id="dateundefined">admin     16:43:29</div><div class="left-msg-content"><div class="left-msg-box"><div>Hi～～

我是您的智能捷亦碳管家




捷亦云是国内最专业的碳管理一站式服务工具，碳盘查、碳足迹、碳管理、碳认证，一体化的双碳综合线上管理，尽在碳管理SaaS系统。

想要了解更多，可直接扫码添加官方客服微信号哦～～～
<img class="huilian-ywkefu-img" src="https://photo.ywkefu.com/reply/af499e523c2244788ad5c1823d90ab4c.png" title="点击查看图片"><br></div></div></div></div>



margin: 0px; padding: 0px; overflow: hidden; position: fixed; z-index: 9999999; box-sizing: border-box; bottom: 50px; right: 100px; border-radius: 6px; box-shadow: rgba(95, 125, 232, 0.36) 0px 0px 25px 0px; display: block; height: 590px !important; width: 420px !important;
margin: 0px; padding: 0px; overflow: hidden; position: fixed; z-index: 9999999; box-sizing: border-box; bottom: 50px; right: 100px; border-radius: 6px; box-shadow: rgba(95, 125, 232, 0.36) 0px 0px 25px 0px; display: none; height: 590px !important; width: 420px !important;




                                                            {if condition="$vo['child'] eq 1"}
                                                            {volist name="$vo.sub" id="v"}
                                                            <li class="navchildLine">
                                                                <div class="navchildData">
                                                                    <a href="{$v.url}"  data-type="" >
                                                                        <span class="navchildLink">{$v.catname}</span>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                            {/volist}
                                                            {/if}

                {if $thisModule.service_detail}
                    {$thisModule.service_detail|raw}
                {else/}
                    <div class="fly-none">没有相关数据</div>
                {/if}

width: 100%;
height: auto;



测试条目
php think make:controller app\Data\controller\StandardTestItemController
php think make:model app\Data\model\StandardTestItemModel
php think make:validate app\Data\validate\StandardTestItemValidate

产品信息
php think make:controller app\Data\controller\StandardProductInfoController
php think make:model app\Data\model\StandardProductInfoModel
php think make:validate app\Data\validate\StandardProductInfoValidate

排程中心
php think make:controller app\Data\controller\SchedulingCenterController
php think make:model app\Data\model\SchedulingCenterModel
php think make:validate app\Data\validate\SchedulingCenterValidate

委外实验
php think make:controller app\Data\controller\SubcontractController
php think make:model app\Data\model\SubcontractModel
php think make:validate app\Data\validate\SubcontractValidate




color:null
createTime:"2023-06-30T03:29:53.000+00:00"
customerName:"2222"
dateInProduced:null
endTime:"2023-08-23T00:00:00.000+00:00"
entrustNo:"230630003"
experimentName:"气味性"
experimentNo:"230630003-2"
experimentSampleCount:1
factory:""
id:"c4be73be2b5245f694645b00da5d6b15"
isEpiboly:1
laboratoryId:"实验室"
laboratoryName:"实验室"
plannedFinishDate:"2023-06-29T16:00:00.000+00:00"
price:0
projectName:"22"
reportGroup:"2"
sampleName:"22"
scheduleEndTime:"2023-08-03T00:00:00.000+00:00"
scheduleManagerId:"79dd3cfd281db347341668bd7ba1ff9e"
scheduleStartTime:"2023-08-02T00:00:00.000+00:00"
standard:"Q/LiA5400012\nQ/LiA5300004"
standardInfoCode:"Q/LiA5400012\nQ/LiA5300004"
standardTestItemId:"0f89a614ab5b4c369d65c86052c05e8f"
startTime:"2023-08-22T00:00:00.000+00:00"
status:9
subcontractReportFileId:"119cbd8e061ff47f5c2e79cf5d8a6fe3"
testFinishTime:"2023-06-29T16:00:00.000+00:00"
texture:null
updateTime:"2023-08-23T01:58:01.000+00:00"



/mobile/news/index/catId/1.html
/mobile/news/index/catId/1.html


select * from v_experiment_sheet 
where 1=1 
-- and is_epiboly = 1 
and experiment_name like concat('%', #{params.experimentName} ,'%') 
and (
    -- concat(',', `create_by`, ',') regexp concat(',',#{params.flowHandler}, ',') 
    or concat(',', `experiment_engineer_id`, ',') regexp concat(',',#{params.flowHandler}, ',') 
    -- or concat(',', `flow_handler`, ',') regexp concat(',',#{params.flowHandler}, ',')
) 
and `customer_name` like concat('%', #{params.customerName},'%') 
and `experiment_no` like concat('%', #{params.experimentNo},'%') 
and `experiment_conclusion_type` = #{params.experimentResult} 
and `laboratory_name` like concat('%', #{params.laboratoryName},'%') 
and `experiment_engineer_id` = #{params.projectEngineer} 
and `project_name` like concat('%', #{params.projectName},'%') 
and `status` = #{params.status} 




-- projectEngineer根据上传的用户名称模糊查询到用户id传入sql查询

列表数据内还有三个数据根据下面sql获取cache字段，从cache字段里获取

select cache from flow_progress where JSON_CONTAINS(cache, '{"id":"#{params.experimentId}"}') order by create_time desc limit 1

usedSamples -> usedSampleItems.size 默认 0
finishOriginalRecord 默认 false
finishDetectionConclusion 默认 false


SELECT 
`experiment_sheet`.`id` AS `id`,
`experiment_sheet`.`entrust_sheet_id` AS `entrust_sheet_id`,
`customer`.`customer_name` AS `customer_name`,
`experiment_sheet`.`experiment_no` AS `experiment_no`,
`experiment_sheet`.`project_name` AS `project_name`,
`experiment_sheet`.`experiment_name` AS `experiment_name`,
`experiment_sheet`.`standard` AS `standard`,
`experiment_sheet`.`is_epiboly` AS `is_epiboly`,
`experiment_sheet`.`factory` AS `factory`,
`experiment_sheet`.`status` AS `status`,
`experiment_sheet`.`price` AS `price`,
`experiment_sheet`.`flow_handler` AS `flow_handler`,
`experiment_sheet`.`schedule_start_time` AS `schedule_start_time`,
`experiment_sheet`.`schedule_end_time` AS `schedule_end_time`,
`experiment_sheet`.`start_time` AS `start_time`,
`experiment_sheet`.`end_time` AS `end_time`,
`experiment_sheet`.`remark` AS `remark`,
`experiment_sheet`.`create_by` AS `create_by`,
`experiment_sheet`.`create_time` AS `create_time`,
`experiment_sheet`.`update_time` AS `update_time`,
`experiment_sheet`.`is_error` AS `is_error`,
`experiment_sheet`.`experiment_sample_count` AS `experiment_sample_count`,
`experiment_sheet`.`experiment_conclusion_type` AS `experiment_conclusion_type`,
`experiment_sheet`.`standard_test_item_id` AS `standard_test_item_id`,
`standard_info`.`code` AS `standard_info_code`,
`standard_info`.`version` AS `standard_info_version`,
`experiment_sheet`.`planned_finish_date` AS `planned_finish_date`,
`experiment_sheet_sample`.`sample_id` AS `sample_id`,
`experiment_sheet_sample`.`sample_name` AS `sample_name`,
`experiment_sheet_sample`.`gain_count` AS `gain_count`,
`experiment_sheet_sample`.`receive_count` AS `receive_count`,
`entrust_sheet_addition`.`test_finish_time` AS `test_finish_time`,
`experiment_sheet`.`laboratory_id` AS `laboratory_id`,
if(ISNULL(`experiment_sheet`.`laboratory_name`),
`system_auth_department`.`depart_title`,
`experiment_sheet`.`laboratory_name`) AS `laboratory_name`,
`experiment_sheet`.`experiment_engineer_id` AS `experiment_engineer_id`,
`experiment_sheet`.`experiment_engineer_name` AS `experiment_engineer_name`,
`experiment_sheet`.`schedule_manager_id` AS `schedule_manager_id`,
`experiment_sheet`.`schedule_manager_name` AS `schedule_manager_name`,
`experiment_sheet`.`is_result_record` AS `is_result_record`,
`entrust_sheet`.`entrust_no` AS `entrust_no`,
`experiment_sheet`.`report_group` AS `report_group`,
`entrust_sheet_addition`.`sales_assistant_id` AS `sales_assistant_id`,
`entrust_sheet_addition`.`salesman_id` AS `salesman_id`,
`experiment_sheet_addition`.`subcontract_report_file_id` AS `subcontract_report_file_id`,
`experiment_sheet_addition`.`wu_ling_task_id` AS `wu_ling_task_id`,
`experiment_sheet_addition`.`quick_pass_type` AS `quick_pass_type`,
`experiment_sheet_addition`.`report_no` AS `report_no`
FROM (((((((((`experiment_sheet`
LEFT JOIN `system_auth_department` ON((`system_auth_department`.`id` = `experiment_sheet`.`laboratory_id`)))
LEFT JOIN `standard_test_item` ON((`standard_test_item`.`id` = `experiment_sheet`.`standard_test_item_id`)))
LEFT JOIN `standard_info` ON((`standard_info`.`id` = `standard_test_item`.`standard_id`)))
LEFT JOIN `experiment_sheet_addition` ON((`experiment_sheet`.`id` = CONVERT(`experiment_sheet_addition`.`experiment_sheet_id` USING utf8mb4))))
LEFT JOIN `experiment_sheet_sample` ON((`experiment_sheet`.`id` = `experiment_sheet_sample`.`experiment_sheet_id`)))
LEFT JOIN `experiment_sheet_sample_addition` ON((`experiment_sheet_sample`.`id` = CONVERT(`experiment_sheet_sample_addition`.`experiment_sheet_sample_id` USING utf8mb4))))
LEFT JOIN `entrust_sheet` ON((`entrust_sheet`.`id` = `experiment_sheet`.`entrust_sheet_id`)))
LEFT JOIN `customer` ON((`customer`.`id` = `entrust_sheet`.`customer_id`)))
LEFT JOIN `entrust_sheet_addition` ON((`entrust_sheet`.`id` = `entrust_sheet_addition`.`sheet_id`)))
WHERE (((`entrust_sheet`.`status` >= 5) OR (`experiment_sheet`.`status` > 4) OR ((`entrust_sheet`.`status` < 5) AND (`experiment_sheet`.`status` >= 4))) AND ISNULL(`experiment_sheet`.`delete_time`) AND (`experiment_sheet_addition`.`quick_pass_type` = 1))
GROUP BY `experiment_sheet`.`id`
ORDER BY `experiment_sheet`.`update_time` DESC




SELECT *
FROM `experiment_sheet` `experiment_sheet`
LEFT JOIN experiment_sheet_addition ON experiment_sheet_addition.experiment_sheet_id = experiment_sheet.id
WHERE is_epiboly = 2 AND experiment_engineer_id IS NOT NULL 
AND schedule_start_time < NOW() 
AND schedule_end_time > NOW() 
AND STATUS = 2 
AND (`experiment_sheet_addition`.parent_id IS NULL OR (`experiment_sheet_addition`.parent_id IS NOT NULL 
AND (
SELECT COUNT(*)
FROM sample_information_item_details
LEFT JOIN sample_information_item ON sample_information_item.id = sample_information_item_details.sample_id
WHERE sample_information_item.experiment_no = `experiment_sheet`.experiment_no 
AND sample_information_item_details.return_time IS NOT NULL) = (
    SELECT quantity
    FROM experiment_sheet_sample
    WHERE experiment_sheet_id = experiment_sheet.id) 
    AND (
        SELECT COUNT(*)
        FROM `experiment_sheet` `experiment_sheet_2`
        WHERE `experiment_sheet_2`.id = `experiment_sheet_addition`.parent_id 
        AND `experiment_sheet_2`.`status` = 9
    )
))


select * from experiment_sheet 
where is_epiboly = 2 
and experiment_engineer_id is not null 
and schedule_end_time < now() 
and (status = 2 or status = 4)


 $wheres = "JSON_CONTAINS(cache," . "'" . '{"id":"' . $v['id'] . '"}' . "'" . ")";
                // var_dump($wheres);die();
                $one = Db::table('flow_progress')->field('cache')->where($wheres)->order('create_time desc')->find();
                // var_dump($one);die();
                //echo Db::table("flow_progress")->getLastSql();
                if ($one) {
                    $one_json = json_decode($one['cache'], true);
                    // var_dump($one_json);die();
                    $v['finish_original_record'] = isset($one_json['finishOriginalRecordfinishOriginalRecord']) ? $one_json['finishOriginalRecord'] : false;
                    $v['finish_detection_conclusion'] = isset($one_json['finishDetectionConclusion']) ? $one_json['finishDetectionConclusion'] : false;
                    $v['used_samples'] = isset($one_json['usedSampleItems']) ? count($one_json['usedSampleItems']) : 0;
                }



实验状态（1：预排期，2：待排期，3：待确认，4：待开始，5：已开始，6：已取消，7：已结束，8：已终止，9：已完成）

DM05-B-01-05








=============================================================================
(http://t.csdn.cn/3Mq4S  http://t.csdn.cn/vKsuA   http://t.csdn.cn/2nuaV)
自己编译
1.进入php ext文件夹下

执行 php ./ext_skel.php --ext testfunc （如果没有ext_skel.php文件则执行 如下./ext_skel --extname=testfunc）

2.修改 config.m4
删掉8~11行 【del】注释符

PHP_ARG_WITH(testfunc,
  for testfunc support,
  [--with-testfunc,
    Include testfunc support])

3.进入生成的文件夹下执行 phpize

4.修改php_testfunc.h
在文件后尾添加 PHP_FUNCTION(testfunc_gavin);

5.编辑testfunc.c
zend_function_entry hello_functions[] = {
    PHP_FE(testfunc_gavin,   NULL)       /* 此处添加要实现的方法 */
    {NULL, NULL, NULL}  /* Must be the last line in hello_functions[] */
};
 
//文末,添加具体实现
PHP_FUNCTION(testfunc_gavin)
{
    longint a, b;
    longint result;
    if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "ll", &a, &b) == FAILURE) {
        return;
    }
    result = testfunc_gavin(a, b);
    RETURN_LONG(result);
}

6.执行
phpizels
./configure  --with-php-config=/www/server/php/74/bin/php-config
make clean   # 清除编译
make LDFLAGS=-lhello     #-lhello 表示加载 动态库  testfunc.so 文件
sudo make install




==============================================================================
(https://www.cnblogs.com/mrlayfolk/p/15714532.html)
[readme]
1、libdbapi.so安装
将dbapi.zip解压至linux分区；解压密码tprisis。
进入解压后的dbapi目录，使用root权限执行./ins-pro.sh。
2、测试程序源码为test-program-src，库头文件为源码包解压后的dbapi.h，WinDef4Linux.h。


[调用已编译的so]
docker ps -a
docker exec -it 7b86ce150484 /bin/bash

bt 查看宝塔命令

cd dbapi
./ins-pro.sh

查看/usr/lib64/irpt目录下ddpdrv等文件是否存在

执行/dbapi/test-dbapi是否成功

ldd命令查看可执行文件依赖的库是否存在

将test-dbapi文件放在系统public目录下，然后去php代码中找个接口测试
删除禁用函数shell_exec
-- shell_exec('pwd');
shell_exec('./test-dbapi');
$file = fopen("testfile.txt", "w");fwrite($file, shell_exec('./test-dbapi'));fclose($file);


/*        // $data['data']['testfunc_add'] = DBP_GetVersion();
        // $data['data']['testfunc_add'] = testfunc_add("test");
        // $data['data']['testfunc_add'] = shell_exec('pwd');
        // $data['data']['aaa'] = shell_exec('./test-dbapi');
        // halt(phpinfo());
        // halt(get_loaded_extensions());*/



========================================================================
运行命令报错
-bash: warning: setlocale: LC_ALL: cannot change locale (zh_CN.utf-8)

/bin/sh: warning: setlocale: LC_ALL: cannot change locale (zh_CN.utf-8)

解决办法
1.运行以下命令来查看可用的语言环境列表：
locale -a
确保 zh_CN.utf-8 在列表中。

2.如果 zh_CN.utf-8 不在列表中，你需要安装中文语言包。运行以下命令以安装 zh_CN.utf-8 语言包：
yum groupinstall "Chinese Support"
这个命令会安装中文语言包和其他相关的语言支持组件。

3.安装完成后，运行以下命令来重新生成语言环境配置：
localedef -c -f UTF-8 -i zh_CN zh_CN.utf-8
4.编辑 /etc/locale.conf 文件，并添加以下内容：
LC_ALL="zh_CN.utf-8"
LANG="zh_CN.utf-8"


============================================================================

1.1. 负荷（出力）系数：展示百分比，计算公式：负荷（出力）系数=发电量÷（机组容量*运行小时数）
1.2. 发电排放强度：单位：tCO2/MWh，计算公式：发电排放强度=该机组排放量÷供电量
1.3. 供热排放强度：单位：tCO2/GJ，计算公式：供热排放强度=该机组排放量÷供热量


1）${Text_标准年代号}
2）${Table_标准年代号}
3）${Text_方法标准年代号}
4）${Table_方法标准年代号}
5）${Text_方法标准号}
6）${Table_方法标准号}
7）${Text_标准编号}
8）${Table_标准编号}

==============================================================================

恺蔚实验室综合管理系统
沈阳道达
http://192.168.1.3:49000/
岱美实验综合管理系统
http://192.168.1.3:29000/
浩进实验综合管理系统

上汽通用五菱供应商试验管理系统
https://sq.sgmw.com.cn/shiyan/?token=21232f297a57a5a743894a0e4a801fc3admin#/page/ThirdParty/list/list.html


二氧化碳在线监测系统
http://jayeecloud.tpddns.cn:18000/

捷亦云
http://jayeecloud.tpddns.cn:9999/



1、数据挖掘的分析方法——决策树法
2、数据挖掘的分析方法——神经网络法
3、数据挖掘的分析方法——关联规则法
4、数据挖掘的分析方法——遗传算法
5、数据挖掘的分析方法——聚类分析法
6、数据挖掘的分析方法——模糊集法
7、数据挖掘的分析方法——web页挖掘
8、数据挖掘的分析方法——逻辑回归分析
9、数据挖掘的分析方法——粗糙集法
10、数据挖掘的分析方法——连接分析


1、关联分析 (Associations):目的是为了挖掘出隐藏在数据间的相互关系。若设 R={A1，A2，...，AP)为0，1域上的属性集，r为R上的一个关系，关于r 的关联规则表示为X-B，其中XER，BER,且XnB=Q。关联规则的矩阵形式为:矩阵r 中，如果在行 的每一列为 1，则行 B 中各列趋向于为1。在进行关联分析的同时还需要计算两个参数，最小置信度(Confidence)和最小支持度 (Support)。前者用以过滤掉可能性过小的规则，后者则用来表示这种规则发生的概率，即可信度。
2、序列模式分析 (SeuentialPatterns):目的也是为了挖掘出数据之间的联系，但它的侧重点在于分析数据间的前后关系(因果关系)。例如，将序列模式分析运用于商业，经过分析，商家可以根据分析结果发现客户潜在的购物模式，发现顾客在购买一种商品的同时经常购买另一种商品的可能性。在进行序列模式分析时也应计算置信度和支持度。
3、分类分析 (Classifiers):首先为每一个记录赋予一个标记(一组具有不同特征的类别)，即按标记分类记录，然后检查这些标定的记录，描述出这些记录的特征。这些描述可能是显式的，如一组规则定义;也可能是隐式的，如一个数学模型或公式。
4、聚类分析(Clustering):聚类分析法是分类分析法的逆过程，它的输入集是一组未标定的记录，即输入的记录没有作任何处理。目的是根据一定的规则，合理地划分记录集合，并用显式或隐式的方法描述不同的类别。在实际应用的 DM 系统中，上述四种分析方法有着不同的适用范围，因此经常被综合运用







====================================================================================================

N1TL.10HTA10CQ016XQ11_OUT        6(二氧化碳实时浓度)
N1TL.10HTA10CQ016XQ03_OUT        7（烟气流速）
CALC.BT.N1BS.CO2_YY_Tol          27（当年碳排放量）
N1TL.10HTA10CQ016XQ26_OUT        28（去年碳排放量）
N1DCS.100AP111EV01_106495_OUT    11（负荷率）
N2TL.20HTA60CP001XQ01_XQ01       8（烟气压力）
公式:(pointAvg('N1DCS.10HNA10CT567_XQ01','T_Any','N1TS_W_G_500')+pointAvg('N1DCS.10HNA20CT567_XQ01','T_Any','N1TS_W_G_500'))/2       9（温度）
N1FECS.YM0275                    17（发电量，定时任务，D，D+8h，D+16，D+1d），月发电量累计日发电量     
N1DCS.100AP111EV01_106495_OUT    10（发电功率）
CALC.TT.N1TC_CoalRate_G          21(发电煤气耗)
CALC.TT.N1TC_CoalRate_SupplyHeat    22（供热煤气耗）


post                             23（发电设备月运行小时数）
http://172.19.160.120/SmartPortal//mc/portal/getKpi
{"codeList":["金陵燃煤/1号机组指标/运行小时.本月累计"]}
{"data":[{"valueList":[{"time":"2023-10-01 00:00:00","value":"617.9"}],"name":"金陵燃煤/1号机组指标/运行小时.本月累计"}],"success":true,"errorCode":0,"message":""}

CALC.TT.N1TC_Eff_Unit           25（负荷率）
CALC.TT.N1TC_Q_GR               12（供热量）
CALC.BT.N1BC_EFF_Boiler            锅炉效率
N1TL.10HTA10CQ016XQ25_1_OUT        烟气流量
GYTL.YM_01ECT01AW021PI01_XQ01 + GYTL.YM_01ECT01AW011PI01_XQ01 输煤总量




http://127.0.0.1:9003/DataAnalysis/addtestdata
http://192.168.1.61:998/DataAnalysis/addtestdata


28.1105 * (3387.2- 83.74) * 0.001 = 92.86191233


30.2792 * (3196.9- 83.74) * 0.001 = 94.26399427
1h 187.124


取时间
2023-11-18 04:02:20

3847660 - 3847100


self::getMqDataRange('N1FECS.YM0275', date('Y-01-01 00:00:00'), date('Y-01-01 00:00:00', strtotime("1 minute")))


$list = M('order')
        ->where('id>10')
        ->field('id,IF(finished="0000-00-00 00:00:00","",finished) as r_finished,CASE status WHEN 0 THEN "未派送" WHEN 1 THEN "派送中" WHEN 2 "已收货" ELSE "未知状态" END as r_status')
        ->select();




SELECT `equipment`.`internal_id`,`equipment`.`device_status`,`equipment`.`approval`,`equipment`.`equipment`,`equipment`.`calibration_interval`,`equipment`.`id`,`equipment`.`period_cycle`,`equipment`.`device_title`,`equipment`.`device_type`,`equipment`.`asset_class`,`equipment`.`department`,`equipment`.`or_shared`,`equipment`.`responsible_staff`,`equipment`.`responsible_deputy`,`equipment`.`equipment_remark`,`equipment`.`revised_date`,`more`.`status`,`equipment`.`create_time`,`equipment`.`type`,`equipment`.`test_type`,`equipment`.`subcategory`,`equipment`.`is_multichannel`,`equipment`.`channel_no`,`more`.`allow_error`,`more`.`fixed_asset_no`,`more`.`range`,`more`.`arrival_date`,`more`.`model`,`more`.`serial_no`,`more`.`manufacturer`,`more`.`software`,`more`.`equipment_require`,`more`.`firmware`,`more`.`characteristics`,`equipment`.`period_verification`,`equipment`.`equipment_appendix`,`equipment`.`experimental_entry`,`equipment`.`calibration_due_date` FROM `system_equipment` `equipment` INNER JOIN `system_equipment_more_info` `more` ON `equipment`.`id`=`more`.`equipment_id` WHERE (  (  equipment.create_by in ('0314434775ebd28d238b9966a5d0d0a7','0cd86e0b5a21d50e2d8737da6b85d192','0e2ba870316be8e852a6cb948d6c74a8','1','10f01a7e97a152a193cf21acfc7fbed0','165f27b240f4a7a13b921e7b98a436b7','26d7ea646ffe52c658647842cfd12969','2b1d7454745350df12c7bdaa052ac4df','3065f7c9edb3606f19c280847ea3c6f1','326de1a540e6823a17858265e64c541d','33a11fae4f433f772b57a7cb24f81cb9','38bf531b8d130955408df0c400a92752','3b9055b6efd4f7dcedec801b70bb5976','413a383a3363ca88d8f6088a29bebf70','45dc442e015eac680bdcbeba01561d09','4826b18fd96877f5abd6e3c180ba8a93','4f4191a609f19099ab9471dfdd9ac34d','606062d65171b9129f7fdbdc228f24ff','63a4e9c20ae25f64c6780807f5cc9dc2','79dd3cfd281db347341668bd7ba1ff9e','7c46f981c88122c1861ece8d203aa379','7c609244a8c619a232133c0cef24879c','85457ca2ddc34ffc8b23d2db3d15d5d6','893de04fe1c41c076c9f442f254da3ba','8d2a55fcaa30c89c00fd464e67dcdb00','8fbc1c9639458a22b145798769d6a061','95cd78fecdcb4a8b00f47d7f764d6c4f','97a9383146c3bd633443aee1a7923dbf','97deea4b354f6ecb3ded8e63fd898bea','9a46c1d1c48bb1f9f792c8d912ccc54d','a23c0a1baa2856f0c0a0113ea0829498','a4d127f56a76409a21bb436d1c66b4e3','b2fa38b431f7268adbeca059b1d34893','b889ed379c5c1dc469b2c74cfebdb3c0','b8e5953685821a9a881390d1492924e0','bc6eeff5bd3ac2bc7be2b66382425236','c0ece9befc04f769e1f871c7eb049fc9','c2f0ef38206e2fb86c4660b35b79c3ba','c47bae516e90c12ad09590b2ce5bb025','c74578e14b4638625864ccd2b729fd43','c781f800d1912bfa183e83554c8d3356','ce5cc398bc20925fd5d9a7aa2bad0aee','cf45ae79cceaeb0912d53624b501eec6','d8d425eaf882136bdd90bb0ecbc49772','d8fe9d77450f4e28d7978b90f0eb285d','e1b36af04c41da184dc99242b25e3707','e534649878d23f0b8ade809d5c3fbc68','ec09d971a26e23d9d3c0fd5711b2ac9f','f30e2f1ce15c40f8000732daa9f84000','f537d7dd19f56e5edb36f3cf0c612e2f','f6daec11c989c7b0900ed563df05fcb0','f9757cdefeeeb5d5e3e462a7a1550b3c','1','1') or equipment.flow_handler='1' )  AND `internal_id` LIKE '%CC%'  AND `device_status` NOT IN (4,5)  AND `equipment`.`approval` = '1'  AND `equipment`.`create_by` IN ('0314434775ebd28d238b9966a5d0d0a7','0cd86e0b5a21d50e2d8737da6b85d192','0e2ba870316be8e852a6cb948d6c74a8','1','10f01a7e97a152a193cf21acfc7fbed0','165f27b240f4a7a13b921e7b98a436b7','26d7ea646ffe52c658647842cfd12969','2b1d7454745350df12c7bdaa052ac4df','3065f7c9edb3606f19c280847ea3c6f1','326de1a540e6823a17858265e64c541d','33a11fae4f433f772b57a7cb24f81cb9','38bf531b8d130955408df0c400a92752','3b9055b6efd4f7dcedec801b70bb5976','413a383a3363ca88d8f6088a29bebf70','45dc442e015eac680bdcbeba01561d09','4826b18fd96877f5abd6e3c180ba8a93','4f4191a609f19099ab9471dfdd9ac34d','606062d65171b9129f7fdbdc228f24ff','63a4e9c20ae25f64c6780807f5cc9dc2','79dd3cfd281db347341668bd7ba1ff9e','7c46f981c88122c1861ece8d203aa379','7c609244a8c619a232133c0cef24879c','85457ca2ddc34ffc8b23d2db3d15d5d6','893de04fe1c41c076c9f442f254da3ba','8d2a55fcaa30c89c00fd464e67dcdb00','8fbc1c9639458a22b145798769d6a061','95cd78fecdcb4a8b00f47d7f764d6c4f','97a9383146c3bd633443aee1a7923dbf','97deea4b354f6ecb3ded8e63fd898bea','9a46c1d1c48bb1f9f792c8d912ccc54d','a23c0a1baa2856f0c0a0113ea0829498','a4d127f56a76409a21bb436d1c66b4e3','b2fa38b431f7268adbeca059b1d34893','b889ed379c5c1dc469b2c74cfebdb3c0','b8e5953685821a9a881390d1492924e0','bc6eeff5bd3ac2bc7be2b66382425236','c0ece9befc04f769e1f871c7eb049fc9','c2f0ef38206e2fb86c4660b35b79c3ba','c47bae516e90c12ad09590b2ce5bb025','c74578e14b4638625864ccd2b729fd43','c781f800d1912bfa183e83554c8d3356','ce5cc398bc20925fd5d9a7aa2bad0aee','cf45ae79cceaeb0912d53624b501eec6','d8d425eaf882136bdd90bb0ecbc49772','d8fe9d77450f4e28d7978b90f0eb285d','e1b36af04c41da184dc99242b25e3707','e534649878d23f0b8ade809d5c3fbc68','ec09d971a26e23d9d3c0fd5711b2ac9f','f30e2f1ce15c40f8000732daa9f84000','f537d7dd19f56e5edb36f3cf0c612e2f','f6daec11c989c7b0900ed563df05fcb0','f9757cdefeeeb5d5e3e462a7a1550b3c')  AND ( 1=1 ) ) AND `equipment`.`delete_time` IS NULL ORDER BY `calibration_due_date` ASC,`equipment`.`create_time` DESC



php think make:controller app\Consumables\controller\ConsumablesController
php think make:model app\Consumables\model\ConsumablesModel

php think make:validate app\Consumables\validate\ConsumablesValidate



审批状态 
1、草稿 
2、申请单待审核
3、申请单退回
4、待反馈
5、完成
6、取消

1 正常 2草稿 3 审核中



草稿    4  2
待审核  2  3  退回  3  3
待反馈  4  3

已完成 7  1


订单管理审核记录log

type_table【需要显示的字段】
1. 订单创建       【create_time title create_by】
2. 报价单审核退回  【create_time title create_by verify_end back】
3. 报价单审核通过  【create_time title create_by verify_end】
4. 更新报价       【create_time title create_by】
5. 取消订单       【create_time title create_by】
6. 报价单生成     【create_time title create_by file_id】
7. 报价单下载     【create_time title create_by】
8. 上传合同       【create_time title create_by】
9. 编辑信息       【create_time title create_by】
10. 创建项目      【create_time title create_by project_no file_id】
11. 项目审核退回   【create_time title create_by verify_end back project_no】
12. 重新提交项目   【create_time title create_by project_no file_id】
13. 变更申请提交   【create_time title create_by】
14. 变更申请退回   【create_time title create_by verify_end back】
15. 变更申请通过   【create_time title create_by verify_end】
16. 项目全部创建   【create_time title create_by】

##### 返回参数说明 

|节点名称|传参|说明|
|:-----  |:-----|----- |
|销售经理 |order_2   |   |
|销售总监 | order_3 |   |
|总经理 | order_3 |   |


Gavin测试001-报价库（认证-标准）

Gavin测试001-认证客户


状态 1待收样 2已收样 3已领样 4已还样 5已出库 6已报废

说明
查看，编辑时【state】【test_engineer】非必传
收样时，获取待收样列表 【state】为1
领样时，获取已收样列表 【state】为2，【test_engineer】为所选枚举值
出库时，获取已收样、已还样列表 【state】为2,4
还样时，获取已领样列表 【state】为3，【test_engineer】为所选枚举值
报废时，获取已收样、已还样列表【state】为2,4，且处理方式是测试方处理样品清单【sample_processing】为2，项目结案后才可以操作报废

【state】 状态1=>已拟稿2=>待审核3=>待批准4=>待回传5=>已退回6=>已完成7=>已作废8=>已取消
|节点名称 |原状态| 传参说明|
|:-----  |:-----|:---------- |
|草稿确认通过 | 1           | 【state】传1                   ❤PS:   【state】变更为2 |
|取消        | 1、5         | 【state】传2                   ❤PS:   【state】变更为8 |
|作废        | 1、2、3、4、5、6| 【state】传3、【scrap_content】   ❤PS:   【state】变更为7 |
|审核通过    | 2           | 【state】传4                    ❤PS:   【state】变更为3 |
|审核退回    | 2           | 【state】传5、【back_content】     ❤PS:   【state】变更为5，同时【last_state】变更为2 |
|审批通过    | 3           | 【state】传6                    ❤PS:   判断是否盖章 是【state】变更为4，否【state】变更为6 |
|审批退回    | 3           | 【state】传7、【back_content】     ❤PS:   【state】变更为5，同时【last_state】变更为3 |
|回传报告     | 4          | 【state】传8、【file_id】          ❤PS:   【state】变更为6 |
|重新提交     | 5          | 【state】传9                    ❤PS:   根据【last_state】是2还是3 【state】变更为2还是3 |
|撤销作废     | 7          | 【state】传10                   ❤PS:    根据【last_state】变更为【state】 |
|修改         | 6          | 见修改接口                    ❤PS:    只能修改已完成状态的报告 |


数据权限已加接口
->where('o.create_by','in',DataRangeUserIds()) // 数据权限


订单管理列表
项目管理列表
报告管理列表
样品管理列表



-- 清空流程相关数据库
-- 订单
TRUNCATE `order`;
TRUNCATE `order_change_data`;
TRUNCATE `order_change_price`;
TRUNCATE `order_details`;
TRUNCATE `order_examine`;
-- 项目
TRUNCATE `project`;
TRUNCATE `project_details`;
TRUNCATE `project_details_pack`;
TRUNCATE `project_equipment`;
TRUNCATE `project_examine`;
-- 分包
TRUNCATE `project_invoice`;
TRUNCATE `project_pack`;
TRUNCATE `project_pack_examine`;
TRUNCATE `project_report`;
-- 项目样品
TRUNCATE `project_sample`;
TRUNCATE `project_sample_attachment`;
-- 工时计算
TRUNCATE `project_working`;
TRUNCATE `project_working_details`;
TRUNCATE `project_sample`;
-- 报告管理
TRUNCATE `report`;
TRUNCATE `report_examine`;
TRUNCATE `report_project_details`;
-- 样品管理操作记录
TRUNCATE `sample_cache_edit_examine`;
TRUNCATE `sample_cache_examine`;
TRUNCATE `sample_cache_examine_enclosure`;
TRUNCATE `sample_edit_examine`;
TRUNCATE `sample_examine_enclosure`;
-- 样品管理
TRUNCATE `system_sample`;
TRUNCATE `system_sample_attachment`;
TRUNCATE `system_sample_cache`;
TRUNCATE `system_sample_cache_attachment`;
TRUNCATE `system_sample_cache_detail`;
TRUNCATE `system_sample_cache_quit`;
TRUNCATE `system_sample_detail`;
TRUNCATE `system_sample_quit`;
TRUNCATE `system_sample_wifi`;
TRUNCATE `system_storage_rack`;
-- 测试管理
TRUNCATE `test_exception_handle`;
TRUNCATE `test_original_records`;
-- 合同管理
TRUNCATE `contract`;


            $project_data = $this->project->find($data['id']);
            $sdata = [
                'id'          => create_uuid(),
                'create_time' => date('Y-m-d H:i:s'),
                'create_by'   => $request->user_id,
                'order_id'    => $data['id'],
                'title'       => '重新提交项目',
                'project_no'  => $project_data['data']['project_no']
            ];

            // 添加项目审核记录
            $this->projectExamine->insert($sdata);



    /**
     * 生成测试单号
     * 需项目排期后生成，审核?
     * 项目号+序号
     */
    public function setTestNo($id)
    {
        $project_data = Db::name('project')->where(['id'=>$id])->find();
        $list = $this->field('project_details.project_id,project_details.test_no,p.project_no')
        ->Join('project p', 'project_details.project_id = p.id')
        ->whereNull('project_details.delete_time')
        ->where(['project_details.project_id'=>$id])
        ->whereNotNull('project_details.test_no')
        ->select()->toArray();

        $numStr = count($list) + 1; 
        return $project_data['project_no'].'-'.$numStr;
    }




已完成
1. 订单管理数据权限
2. 订单管理流程权限
3. 项目管理数据权限
4. 项目管理流程权限
5. 订单管理、样品管理bug

待完成
1. 订单管理变更申请
2. 项目管理结案操作
3. 项目管理排期
4. 样品管理出库重新绑定对应关系（待定）

Mail error: SMTP Error: data not accepted.SMTP server error: DATA END command failed Detail: The recipient may contain a non-existent account, please check the recipient address. SMTP code: 550

安可捷检测(常州)有限公司
沈阳道达汽车饰件有限公司
上海灏境汽车检测技术有限公司
上海岱美汽车内饰件股份有限公司


/www/wwwroot/report.com/public/storage/report/min_report240312001-2.docx


样品状态（1：待收样， 2：待领样， 3：待还样， 4：已还样）

status >=2  实收数量
status >=3  领样数量

status  =2  已收样数量
status  =3  已领样数量
status  =4  已还样数量





状态 1待收样 2已收样 3已领样 4已还样 5已出库 6已报废

884bf7ca483c50deb8fabac3aa2a296d


            $cache_no_str = 'LS' . date('Ymd');
            $cache_no_str_ls = 'LS-' . date('Ymd');
            $sample_cache_mables = Db::table('system_sample_cache')->where('sample_cache_no', 'like', $cache_no_str . '%')->count();

            $sample_mables = Db::table('system_sample_cache_detail')->where('sample_cache_no', 'like', $cache_no_str_ls . '-%')->count();
            $sample_cache_no = generate_number_sample('LS-' . date('Ymd') . "-", 3, $sample_mables + 1);

            $attachment_mables = Db::table('system_sample_cache_attachment')->where('attachment_no', 'like', $cache_no_str_ls . '-P%')->count();
            $attachment_no = generate_number_sample('LS-' . date('Ymd') . "-", 2, $attachment_mables + 1, 'P'),