// RDBPConnTestDlg.h : 头文件
//

#pragma once
#include "afxwin.h"

#include "dbapi.h"

// CRDBPConnTestDlg 对话框
class CRDBPConnTestDlg : public CDialog
{
// 构造
public:
	CRDBPConnTestDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_RDBPCONNTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	DBPH m_hHandle;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonDisconnect();
	CString m_szIP;
	UINT m_nPort;
	CString m_szUser;
	CString m_szPassword;
	CListBox m_ctrlLog;
	afx_msg void OnBnClickedButtonSnapshot();
	afx_msg void OnBnClickedButtonHistory();
	CString m_szTagName;
	CString m_szTime;
	CString m_szValue;
	CString m_szQuality;
};

class CTimeCount   
{   
public:   
	CTimeCount(){};   
	~CTimeCount(){};   
	void	start()
	{
		QueryPerformanceFrequency(&litmp);   
		dfFreq   =   (double)litmp.QuadPart;//   获得计数器的时钟频率   
		QueryPerformanceCounter(&litmp);   
		QPart1   =   litmp.QuadPart;//   获得初始值   
	};     //开始计时   
	void	end()
	{
		QueryPerformanceCounter(&litmp);   
		QPart2   =   litmp.QuadPart;//获得中止值   
		dfMinus   =   (double)(QPart2-QPart1);   
		dfTim   =   dfMinus   /   dfFreq;//   获得对应的时间值，单位为秒 
	};         //停止计时   

	double time_milli(){
		return dfTim * 1000.0;
	};       //以毫秒精度输出所用时间   

private:   
	LARGE_INTEGER   litmp;     
	LONGLONG   QPart1,QPart2;   
	double   dfMinus,   dfFreq,   dfTim;     
};