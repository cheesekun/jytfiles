// RDBPConnTestDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "RDBPConnTest.h"
#include "RDBPConnTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CRDBPConnTestDlg 对话框




CRDBPConnTestDlg::CRDBPConnTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRDBPConnTestDlg::IDD, pParent)
	, m_szIP(_T("127.0.0.1"))
	, m_nPort(12084)
	, m_szUser(_T("admin"))
	, m_szPassword(_T("admin"))
	, m_hHandle(NULL)
	, m_szTagName(_T(""))
	, m_szTime(_T(""))
	, m_szValue(_T(""))
	, m_szQuality(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRDBPConnTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_IP, m_szIP);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_PASSWORD, m_szPassword);
	DDX_Control(pDX, IDC_LIST_LOG, m_ctrlLog);
	DDX_Text(pDX, IDC_EDIT_TAG_NAME, m_szTagName);
	DDX_Text(pDX, IDC_EDIT_TIME, m_szTime);
	DDX_Text(pDX, IDC_EDIT_VALUE, m_szValue);
	DDX_Text(pDX, IDC_EDIT_QUALITY, m_szQuality);
}

BEGIN_MESSAGE_MAP(CRDBPConnTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CRDBPConnTestDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CRDBPConnTestDlg::OnBnClickedButtonDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_SNAPSHOT, &CRDBPConnTestDlg::OnBnClickedButtonSnapshot)
	ON_BN_CLICKED(IDC_BUTTON_HISTORY, &CRDBPConnTestDlg::OnBnClickedButtonHistory)
END_MESSAGE_MAP()


// CRDBPConnTestDlg 消息处理程序

BOOL CRDBPConnTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	GetDlgItem(IDC_BUTTON_DISCONNECT)->EnableWindow(FALSE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CRDBPConnTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CRDBPConnTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CRDBPConnTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CRDBPConnTestDlg::OnBnClickedButtonConnect()
{
	UpdateData();

	TDBPARG arg;
	memset(&arg, 0, sizeof(TDBPARG));

	strcpy(arg.sip, m_szIP);
	strcpy(arg.suser, m_szUser);
	strcpy(arg.spass, m_szPassword);
	arg.wport = m_nPort;

	/*
	TDBPARG args[4];
	memset(args, 0, sizeof(TDBPARG));

	strcpy(args[0].sip, "192.168.1.100");
	strcpy(args[0].suser, "admin");
	strcpy(args[0].spass, "admin");
	args[0].wport = 12084;

	strcpy(args[0].sip, "127.0.0.1");
	strcpy(args[0].suser, "admin");
	strcpy(args[0].spass, "admin");
	args[0].wport = 12084;

	strcpy(args[2].sip, m_szIP);
	strcpy(args[2].suser, m_szUser);
	strcpy(args[2].spass, m_szPassword);
	args[2].wport = m_nPort;
*/

	CString szLog;
	CTimeCount timer;

	timer.start();

	m_hHandle = ::DBP_Open(&arg, 1);
	//m_hHandle = ::DBP_Open(args, 3);
	if (m_hHandle == NULL)
		return;
	timer.end();

	double dbtime = timer.time_milli();
	timer.start();

	DWORD dwret = DBP_Connect(m_hHandle);

	timer.end();
	if (dwret == 0)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_DISCONNECT)->EnableWindow(TRUE);

		szLog.Format(_T("连接成功，Open耗时%fms，Connect耗时%fms，总耗时%fms"), dbtime, timer.time_milli(), dbtime + timer.time_milli());
	}
	else {
		DBP_Close(m_hHandle);
		m_hHandle = NULL;
		szLog.Format(_T("连接失败，Open耗时%fms，Connect耗时%fms，总耗时%fms"), dbtime, timer.time_milli(), dbtime + timer.time_milli());
	}

	m_ctrlLog.AddString(szLog);
	m_ctrlLog.SetCurSel( m_ctrlLog.GetCount() - 1 );
}

void CRDBPConnTestDlg::OnBnClickedButtonDisconnect()
{
	if (m_hHandle)
	{
		CTimeCount timer;
		timer.start();

		DBP_DisConnect(m_hHandle);
		DBP_Close(m_hHandle);
		m_hHandle = NULL;

		timer.end();

		CString szLog;
		szLog.Format(_T("断开连接，耗时%f ms"), timer.time_milli());
	

		m_ctrlLog.AddString(szLog);
		m_ctrlLog.SetCurSel( m_ctrlLog.GetCount() - 1 );



		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_DISCONNECT)->EnableWindow(FALSE);
	}
}

void CRDBPConnTestDlg::OnBnClickedButtonSnapshot()
{
	if (m_hHandle == NULL)
		return;
	UpdateData();
	
	m_szTagName.Trim();
	if (m_szTagName.IsEmpty())
		return;

	if (m_hHandle == NULL)
		return;

	/*char szTagName[1][80];
	strcpy(szTagName[0], m_szTagName);*/
	char tagname[100] ="";
	strcpy(tagname, m_szTagName);

	TVVAL val;
	memset(&val, 0, sizeof(TVVAL));

	CTimeCount timer;

	timer.start();

	DWORD dwret = DBP_GetSnapshotOne(m_hHandle, tagname, &val);
	//DWORD dwret = DBP_GetSnapshot(m_hHandle, (char(*)[80])&tagname, &val, 1);

	timer.end();

	CString szLog;
	if (dwret == 0)
	{
		CTime tt(val.ltime);
		m_szTime = tt.Format(_T("%Y-%m-%d %H:%M:%S"));
		switch(val.vt)
		{
		case DT_DIGITAL:
		case DT_INT32:
			m_szValue.Format(_T("%d"), val.lval);
			break;
		case DT_INT64:
			m_szValue.Format(_T("%I64"), val.llval);
			break;
		case DT_FLOAT32:
			m_szValue.Format(_T("%g"), val.fval);
			break;
		case DT_FLOAT64:
			m_szValue.Format(_T("%g"), val.dblval);
			break;
		case DT_STRING:
			m_szValue = (val.pstr != NULL) ? val.pstr : _T("");
			break;
		default:
			m_szValue = _T("N/A");
		}
		m_szQuality.Format(_T("%d"), (int)val.snqa);

		if (val.snerr == 0)
			szLog.Format(_T("读<%s>的快照，耗时%f ms"), m_szTagName, timer.time_milli());
		else
			szLog.Format(_T("读<%s>的快照，耗时%f ms(error=%d)"), m_szTagName, timer.time_milli(), (int)val.snerr);
	}
	else {
		m_szTime.Empty();
		m_szValue.Empty();
		m_szQuality.Empty();

		szLog.Format(_T("读<%s>的快照，耗时%f ms，错误码：%u"), m_szTagName, timer.time_milli(), dwret);
	}
	UpdateData(FALSE);

	m_ctrlLog.AddString(szLog);
	m_ctrlLog.SetCurSel( m_ctrlLog.GetCount() - 1 );
}

void CRDBPConnTestDlg::OnBnClickedButtonHistory()
{
	UpdateData();

	m_szTagName.Trim();
	if (m_szTagName.IsEmpty())
		return;

	if (m_hHandle == NULL)
		return;

	long lEndTime = (long)::time(NULL);
	long lStartTime = lEndTime - 3600;

	TVVAL pValues[3600];
	int nSize = 3600;
	memset(pValues, 0, sizeof(TVVAL) * nSize);

	CTimeCount timer;

	timer.start();

	DWORD dwret = DBP_GetHisVal(m_hHandle, m_szTagName, lStartTime, lEndTime, 0, 0, pValues, nSize, &nSize);

	timer.end();

	CString szLog;
	if (dwret == 0)
	{
		szLog.Format(_T("读<%s>最近一小时历史数据，耗时%f ms，读取%d个数据"), m_szTagName, timer.time_milli(), nSize);
	}
	else {
		szLog.Format(_T("读<%s>最近一小时历史数据，耗时%f ms，错误码：%u"), m_szTagName, timer.time_milli(), dwret);
	}

	m_ctrlLog.AddString(szLog);
	m_ctrlLog.SetCurSel( m_ctrlLog.GetCount() - 1 );
}
