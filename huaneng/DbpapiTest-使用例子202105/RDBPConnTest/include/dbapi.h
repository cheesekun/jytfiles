#if !defined _DPAPI2_H_I
#define _DPAPI2_H_I

//数据类型
#define DT_VOID		0	//空类型
#define DT_DIGITAL	1	//开关量	,也是4字节整数		
#define DT_INT32	2	//4字节整数
#define DT_FLOAT32	3	//4字节浮点数
#define DT_INT64	4	//8字节整数
#define DT_FLOAT64	5	//8字节浮点数
#define DT_STRING	6	//字符串
#define DT_BLOB		7	//二进制数据块

// 代理服务器质量码
#define DBP_QUALITY_GOOD					0x00	// GOOD
#define DBP_QUALITY_BAD						0x01	// 坏值，但没有特定原因
#define DBP_QUALITY_BAD_CONFIG_ERROR		0x02	// 服务器配置错误。例如，标签从运行的服务器配置中删除了
#define DBP_QUALITY_BAD_NOT_CONNECTED		0x03	// 输入要求连接到其他目标，但无法成功连接。该质量说明在该时间点上的值不可信，因为有一个数据源未提供
#define DBP_QUALITY_BAD_DEVICE_FAIL			0x04	// 检测到设备故障
#define DBP_QUALITY_BAD_SENSOR_FAIL			0x05	// 检测到传感器故障
#define DBP_QUALITY_BAD_LAST				0x06	// 设备与服务器的通讯失败，但是，最后有效值是可用的。请注意，时标标记的是最后一个有效值的时标，而不是质量变更的时标
#define DBP_QUALITY_BAD_COMM_FAIL			0x07	// 设备与服务器的通讯失败，并且没有最后可用的有效值
#define DBP_QUALITY_BAD_OUT_OF_SERVICE		0x08	// 该标签或组被设置为非活动，表示该标签由于某种原因不被服务器更新

#define DBP_QUALITY_UNCERTAIN				0xE0	// 值有效性不确定，但没有特定原因
#define DBP_QUALITY_UNCERTAIN_LAST			0xE1	// 已经停止写入最新值，该值应被视为“过去值”。请注意，该状态表示为在规定时间内标签未被更新。而Bad/Last Usable Value表示为检测到通讯错误发生
#define DBP_QUALITY_UNCERTAIN_SENSOR		0xE2	// 值超出传感器量程范围或者传感器内部诊断为测量不准确
#define DBP_QUALITY_UNCERTAIN_EU_EXCEEDED	0xE3	// 返回值超出了该值定义的范围。请注意，该状态并不意味着该值是超限的
#define DBP_QUALITY_UNCERTAIN_SUB_NORMAL	0xE4	// 该值来自多个源，但其中状态为GOOD的数据源个数少于规定值

#define DBP_QUALITY_CALC_OFF				0xF0	// 计算停止

#define DBP_QUALITY_LOLO_LIMIT				0xF9	// 低低报
#define DBP_QUALITY_LO_LIMIT				0xFA	// 低报
#define DBP_QUALITY_HI_LIMIT				0xFB	// 高报
#define DBP_QUALITY_HIHI_LIMIT				0xFC	// 高高报
#define DBP_QUALITY_NOT_INIT				0xFD	// 才被创建，还未被初始化
#define DBP_QUALITY_SHUTDOWN				0xFE	// 服务器关闭
#define DBP_QUALITY_UNKOWN					0xFF	// 未知错误

// 读历史str,历史blob,SOE是时标的意义
#define  RST_CUR			1		//当前时标处
#define  RST_AFTER			2		//当前之后一条
#define  RST_DEFAULT		(RST_CUR | RST_AFTER) //当前或之后一条

// DBP_GetMultiPointHisVal使用，表示如果指定时标无值时，如何取值
#define RHV_BEFORE			1		// 取前一值
#define RHV_AFTER			2		// 取后一值
#define RHV_INTERP			3		// 取插值

//权限
#define DS_POWERMAN		0x00000001 //账号管理
#define DS_SYSMAN		0x00000002 //系统管理,数据库管理，标签管理
#define DS_WRITE		0x00000004 //写权限
#define DS_READ			0x00000008 //读权限

//标签类型
#define TAG_FLAG_DDC	0 //现场数据标签(实时库标签) 导入名"ddctag"
#define TAG_FLAG_DEF	1 //定义标签(手工标签)       导入名"deftag"
#define TAG_FLAG_PRE	2 //预处理标签(预处理标签)   导入名"pretag"
#define TAG_FLAG_CUR	3 //曲线标签                 导入名"curtag"	
#define TAG_FLAG_CALC	4 //计算标签				 导入名"calctag"

/// 标签名数组变量声明 
#ifndef MallocTagNameArray
typedef char(* TagNameArray)[80];
#define MallocTagNameArray(n) (TagNameArray)malloc((n) * 80)
#define FreeTagNameArray(p)	if(p) free(p)
#endif

typedef void* DBPH; //句柄

typedef void* HDBPRPCSTREAM; // RPC流句柄

typedef struct t_dbparg  //代理服务器连接参数
{
	char	sip[16];    	//服务器IP
	char	suser[16];		//登录账号
	char	spass[16];  	//登录密码
	WORD	wport;	//端口
	short	snres;	//保留
	char	sres[4];		//保留
}TDBPARG,*LPDBPARG;	//sizeof() = 56

#ifndef _DEFINE_RDBPROXY_STRUCT
#define _DEFINE_RDBPROXY_STRUCT

typedef struct t_ServerInfo
{
	DWORD dwVersion;			// 代理服务器版本号，版本号V2.5.4.0表示为0x02050400
	long lNowTime;				// 当前时间
	long lStartupTime;			// 启动时间
	DWORD dwConnectCount;		// 当前连接数
	DWORD dwTagCount;			// 所有标签数
	float fCPUUsage;			// CPU占用
	DWORD dwHandleCount;		// 句柄数
	DWORD dwThreadsCount;		// 线程数
	DWORD dwMemoryBytes;		// 内存使用
	DWORD dwMemoryBytesPeak;	// 高峰内存使用
	DWORD dwVirtualBytes;		// 虚拟内存大小
	DWORD dwVirtualBytesPeak;	// 高峰虚拟内存大小
	DWORD dwReserves[52];
}SERVERINFO, *LPSERVERINFO; // sizeof()=256


typedef struct t_tagitem		//代理服务器标签定义
{
	char	m_sname[80];		//标签名
	char	m_sdbname[16];		//数据库实例名
	char	m_sdbtagname[80];	//数据库标签名
	char	m_sdes[80];			//描述
	char	m_sunit[16];		//单位
	DWORD   m_dwid;				//标签ID
	WORD	m_wtype;			//数据类型
	short   m_errcode;			//错误代码
	long	m_ltagflag;			//标签标志,
	DWORD   m_dwres;			//保留，填写0
}TTAGITEM,*LPTAGITEM; //sizeof() = 288

// 代理服务器标签扩展属性
typedef struct t_tagitemex
{
	float dblLowLimit;			// 量程下限
	float dblHighLimit;			// 量程上限
	char szKKS[64];				// 设备KKS码
	char szDeviceSource[128];	// 设备源：1#机组/空预器、2#机组/送风机

	BOOL bScan;                 // 是否激活扫描标签点标志, 1 激活, 0 不激活
	long nScanCycle;            // 扫描周期，单位为毫秒
	char szTagSource[32];		// 逻辑块的名称
	char szSourceAddress[128];  // 标签点的点源:真实的物理名称
}TAGITEMEX, *LPTAGITEMEX;

// 用于建点的标签信息定义结构体
typedef struct tagTAGITEMINFO
{
	char	szTagName[80];		// 标签名
	char	szDBName[16];		// 数据库实例名
	char	szDBTagName[80];	// 数据库标签名
	char	szTagDescr[80];		// 标签描述信息
	char	szTagUnit[16];		// 标签单位
	WORD	wDataType;			// 标签数据类型
	long	nTagFlag;			// 标签标志
	int		bSave;				// 是否存盘,0不存，1存盘
	int 	bCompression;		// 压缩方式，0-不压缩，1-百分比精度压缩，2-绝对值精度压缩
	double	dblCompPrecision;	// 压缩精度
	short	shErrorCode;		// 错误码
}TAGITEMINFO, *LPTAGITEMINFO;

typedef struct  t_vval		//变体结构
{
	DWORD	vt;		//类型
	long	lvlen;	//可变部分长度,对pstr,pblob,psoe有效,其他已知长度的基本数据类型填0
					//字符串长度不包含结尾的0字符
	union 	
	{
		long  	lval;		//存储didital、int32
		float  	fval;		//存储float32
		__int64 llval;		//存储int64
		double 	dblval;		//存储float64
		char*	pstr;		//字符串首指针
		void*	pblob;		//存储pblob的存储区首地址
	};
	long  ltime;			//时标
	short snqa;				//质量
	short snerr;			//错误代码
}TVVAL,*LPVVAL; //sizeof() = 24 


typedef struct  t_vval2		//变体结构
{
	DWORD	vt;		//类型
	long	lvlen;	//可变部分长度,对pstr,pblob,psoe有效,其他已知长度的基本数据类型填0
	//字符串长度不包含结尾的0字符
	union 	
	{
		long  	lval;		//存储didital、int32
		float  	fval;		//存储float32
		__int64 llval;		//存储int64
		double 	dblval;		//存储float64
		char*	pstr;		//字符串首指针
		void*	pblob;		//存储pblob的存储区首地址
	};
	long  ltime;			//时标
	short snqa;				//质量
	short snerr;			//错误代码
	short ms;				//毫秒
}TVVAL2,*LPVVAL2; //sizeof() = 28 

typedef struct t_tagval		//标签值
{
	char	m_sname[80];	//标签名
	TVVAL	m_val;			//值
}TTAGVAL,*LPTAGVAL; //sizeof() = 104

#endif // end of _DEFINE_RDBPROXY_STRUCT

typedef struct  t_soe		//事件结构
{
	long	m_ltime;			//时标
	long	m_lmsec;			//毫秒
	char	m_ssrc[64];			//来源
	char	m_sdec[64];			//工程设备
	char	m_sdes[256];		//事件描述
	char	m_stxtargs[256];	//文本参数
	long	m_lclsid;			//事件分类
	char	m_sres[4];			//保留
	long	m_lbinarglen;		//参数长度
	void*	m_pbinargs;			//事件参数
}TSOE,*LPSOE; //sizof() = 664

// 连接超时，默认为5000ms
#define PROPERTY_CONNECT_TIMEOUT		"connect_timeout"
// 读数据超时，默认为30000ms
#define PROPERTY_READ_TIMEOUT			"read_timeout"
// 读小数据超时，默认为4000ms
#define PROPERTY_READ_SMALL_TIMEOUT		"read_small_timeout"
// 写数据超时，默认为4000ms
#define PROPERTY_WRITE_TIMEOUT			"write_timeout"

//枚举函数一次处理一批标签，原来是处理一个标签，这里多加了一个参数ntags
typedef void (WINAPI *Enum_TagAttr)(LPTAGITEM ptag, int ntags, void* lparam); //枚举标签函数

extern "C"{
	// dbpapi版本号，版本号V2.5.4.0表示为0x02050400
	DWORD  __stdcall DBP_GetVersion();

	DBPH  __stdcall DBP_Open( 
		TDBPARG  conargs[],
		int nsize
		);
	DBPH  __stdcall DBP_Open2( 
		const char *szIP, 
		WORD wPort,
		const char *szUserName,
		const char *szPassword,
		BOOL bUseGroup
		);
	DWORD __stdcall DBP_Close( 
		DBPH h
		);
	DWORD __stdcall DBP_Connect( 
		DBPH h
		);
	DWORD __stdcall DBP_DisConnect( 
		DBPH h
		);

	DWORD __stdcall DBP_IsConnect( 
		DBPH h
		);
	DWORD __stdcall DBP_GetTagAttr(
		DBPH  h, 
		TTAGITEM tags[],
		int nsize
		);
	DWORD __stdcall DBP_GetTagAttrEx(
		DBPH  h, 
		TagNameArray sTagNames,
		TAGITEMEX tags[],
		short	errcode[],
		int nsize
		);
	DWORD __stdcall DBP_GetSnapshot(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_GetSnapshotOne(
		DBPH  h, 
		char* tagname,
		TVVAL tvs[]
		);

	DWORD __stdcall DBP_GetHisVal(
		DBPH  h, 
		const char* lpszTagName,
		long lts,
		long lte,
		long linsertsec,
		long lflag,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);
	DWORD __stdcall DBP_GetHisValFilter(
		DBPH  h, 
		const char* lpszTagName,
		long lts,
		long lte,
		long linsertsec,
		long lflag,
		const char *expression,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);

	/* 2020-10-16新增API
		DBP_GetHisVal的改进版本，可以读包括字符串和Blob类型的历史记录，
		当读字符串和Blob类型时，linsertsec和lflag无效，只可读样本数据

		对于DBP_GetHistory2()函数，如果lflag==1，表示读插值，则linsertsec为毫秒的插值间隔

		如果读字符串和Blob类型的历史记录，则pvals需要使用DBP_FreeValues()来释放内存
	*/
	DWORD __stdcall DBP_GetHistory(DBPH  h,LPCTSTR lpszTagName,long lts,long lte,long linsertsec,long lflag,
		TVVAL pvals[],int nsize,int* pnrecsize);
	/* 支持毫秒数据，当lflag==1读历史插值时，linsertsec单位为毫秒 */
	DWORD __stdcall DBP_GetHistory2(DBPH  h,LPCTSTR lpszTagName,long lts, short lts_ms, long lte, short lte_ms, long linsertsec,long lflag,
		TVVAL2 pvals[],int nsize,int* pnrecsize);
	void __stdcall DBP_FreeValues(TVVAL pvals[], int nsize);
	void __stdcall DBP_FreeValues2(TVVAL2 pvals[], int nsize);

	DWORD __stdcall DBP_WriteReal(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_WriteHis(
		DBPH  h, 
		TagNameArray sTagNames,
		TVVAL tvs[],
		int nsize
		);
	DWORD __stdcall DBP_WriteRealStr(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteRealBlob(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteHisStr(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteHisBlob(
		DBPH  h, 
		const char* sName,
		TVVAL* ptv
		);
	DWORD __stdcall DBP_WriteSoe(
		DBPH  h, 
		TSOE* psoe
		);

	DWORD __stdcall DBP_DelHis(
		DBPH  h, 
		const char* sName,
		long lts,
		long lte
		);
	DWORD __stdcall DBP_DelHis2(
		DBPH  h,
		TagNameArray sTagNames,
		long ltimes[],
		short snerrs[],
		int nsize
		);
	DWORD __stdcall DBP_DelSoe(
		DBPH  h, 
		long ltimes, 
		long lmsecs, 
		long ltimee,  
		long lmsece, 
		const char* ssrc, 
		const char* sdec, 
		long lclsid       
		);
	DWORD __stdcall DBP_GetSumVal(
		DBPH  h, 
		const char*  sName,
		long lts,
		long lte,
		long ltype,
		long lds,
		TVVAL pvals[],
		int nsize,
		int* pnrecsize
		);
	DWORD __stdcall DBP_GetRealStr(
		DBPH  h, 
		const char*  sName,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetRealBlob(
		DBPH  h, 
		const char*  sName,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetHisStr(
		DBPH  h, 
		const char*  sName,
		long ltime,
		unsigned long dwstyle,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetHisBlob(
		DBPH  h, 
		const char*  sName,
		long ltime,
		unsigned long dwstyle,
		TVVAL** ppval
		);
	DWORD __stdcall DBP_GetSoe(
		DBPH  h, 
		long lts, 
		long lms, 
		const char* ssrc, 
		const char* sdec, 
		long lclsid,       
		DWORD dwStyle,		
		TSOE** ppsoe
		);

	DWORD __stdcall DBP_QueryTag(
		DBPH  h,
		LPCTSTR sfltname,
		LPCTSTR sfltdes,
		int ntype,
		Enum_TagAttr pfEnum,
		void* lparam
		); //查询标签

	DWORD __stdcall DBP_QueryTag2(
		DBPH  h,
		LPCTSTR sfltname,
		LPCTSTR sfltdes,
		int ntype,
		int ntagtype,
		Enum_TagAttr pfEnum,
		void* lparam); //查询标签

	DWORD __stdcall DBP_GetSvrConInfo(
		DBPH  h,
		LPDBPARG pconarg,
		int* pnNum);//取服务器客户个数

	DWORD __stdcall DBP_SendIdel(
		DBPH  h,
		long* plsrvoder);//发送空闲，看服务器是否有命令

	DWORD __stdcall DBP_InportTags(
		DBPH  h,
		const char* lpsztags,
		int nsize,
		BOOL bEnd); //导入标签到dbp服务器

	DWORD __stdcall DBP_GetServerInfo(
		DBPH h, 
		LPSERVERINFO lpServerInfo
		);

	DWORD __stdcall DBP_GetMultiPointHisVal(
		DBPH h, 
		long	lMode,				//取值模式，RHV_BEFORE、RHV_AFTER、RHV_INTERP
		TagNameArray sTagNames, 
		long lTimes[],
		TVVAL tvs[], 
		int nsize
		);

	DWORD __stdcall DBP_GetDiSumVal(
		DBPH  h, 
		LPCTSTR lpszTagName, 
		long lts, 
		long lte, 
		long lds, 
		long lval,
		long lpSumCount[], 
		long lpSumTimes[], 
		int nsize, 
		int* pnrecsize
		);

	DWORD __stdcall DBP_GetGroups(
		DBPH  h,
		TDBPARG  conargs[],
		int *pnsize); //获取DBP群组配置

	DWORD __stdcall DBP_SetGroups(
		DBPH  h,
		TDBPARG  conargs[],
		int nsize); //设置DBP群组配置

	DWORD __stdcall DBP_TagSearch(DBPH h,
		char* szTagNameMask,		// 标签名掩码，*表示全部，?表示任意一个字符
		char* szTagDescrMask,		// 标签描述掩码，*表示全部，?表示任意一个字符
		char* szDBNameMask,			// 数据库名掩码，*表示全部，?表示任意一个字符
		int nDataType,				// 数据类型掩码，-1表示全部
		int nTagType,				// 标签类型掩码，-1表示全部
		LPTAGITEM pTagItems,		// 返回标签搜索结果的标签属性数组
		int* pnSize,				// 传递pTagItems数组大小，并返回标签搜索结果个数，如果该数组放满，则可能需要继续搜索，否则表示搜索完成
		DWORD* pdwSearchSession,	// 初次传入0，返回标签搜索Session
		float* pfltPerProgress		// 返回搜索完成百分率，有效值0.0-100.0
		);

	DWORD __stdcall DBP_InsertTags(DBPH h,						// 插入标签
		LPTAGITEMINFO pTagItemInfos,	// 需要插入的测点信息
		int nTagCount,				// 测点个数
		BOOL bUpdate				// 如果已存在，是否更新
		);

	DWORD __stdcall DBP_DeleteTags(DBPH h,						// 删除标签
		TagNameArray szTagNames,		// 需要删除的测点名称数组
		int nTagCount,				// 需要删除的测点名称数组个数
		short *pErrors				// 对应测点删除的执行结果，个数应为nTagCount			
		);

	//////////////////////////////////////////////////////////////////////////
	// 配置属性读写接口
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_get_property(
		DBPH h,							// 代理连接句柄
		const char* szPropertyName,		// 配置属性名称
		void* pPropertyData,			// 待设置的配置属性值存储内存块
		int nSize						// 待设置的配置属性值存储内存块大小
		);

	DWORD __stdcall dbp_set_property(
		DBPH h,							// 代理连接句柄
		const char* szPropertyName,		// 配置属性名称 
		void* pPropertyData,			// 用于接受返回配置属性值的存储内存块 
		int nSize						// 用于接受返回配置属性值的存储内存块大小
		);

	//////////////////////////////////////////////////////////////////////////
	// 支持标签路由的新API接口
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_get_snapshot(DBPH h, TagNameArray szTagNames, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_write_snapshot(DBPH h, TagNameArray szTagNames, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_get_history(DBPH  h, const char* lpszTagName, long lStartTime, long lEndTime, 
		long lInsertSec, long lFlag, TVVAL* pTagVals, int* pnSize);

	DWORD __stdcall dbp_write_history(DBPH  h, TagNameArray sTagNames, TVVAL* pTagVals,int nSize);

	DWORD __stdcall dbp_write_single_history(DBPH  h, const char* lpszTagName, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_get_history_section(DBPH h, long lMode, TagNameArray szTagNames, long* lTimes, TVVAL* pTagVals, int nSize);

	DWORD __stdcall dbp_delete_history(DBPH  h, const char* lpszTagName, long lStartTime, long lEndTime);

	DWORD __stdcall dbp_delete_history2(DBPH  h, TagNameArray szTagNames, long* pTimes, short* pErrors, int nSize);

	//////////////////////////////////////////////////////////////////////////
	// RPC接口
	//////////////////////////////////////////////////////////////////////////
	DWORD __stdcall dbp_rpc_create(DBPH h, HDBPRPCSTREAM* pHRPCStream);

	DWORD __stdcall dbp_rpc_destroy(HDBPRPCSTREAM hRPCStream);

	DWORD __stdcall dbp_rpc_call(HDBPRPCSTREAM hRPCStream, const char* lpszRPCFuncName);

	DWORD __stdcall dbp_rpc_clear(HDBPRPCSTREAM hRPCStream);

	DWORD __stdcall dbp_rpc_size(HDBPRPCSTREAM hRPCStream, DWORD* pSize);

	DWORD __stdcall dbp_rpc_position(HDBPRPCSTREAM hRPCStream, DWORD* pPosition);

	DWORD __stdcall dbp_rpc_seek(HDBPRPCSTREAM hRPCStream, DWORD dwOffsetPosition, DWORD dwOriginFlag);

	DWORD __stdcall dbp_rpc_write(HDBPRPCSTREAM hRPCStream, void* pBuffer, DWORD dwSize);

	DWORD __stdcall dbp_rpc_read(HDBPRPCSTREAM hRPCStream, void* pBuffer, DWORD* pSize);
}
#endif //_DPAPI2_H_I