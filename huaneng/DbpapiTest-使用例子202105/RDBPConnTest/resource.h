//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RDBPConnTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_RDBPCONNTEST_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_IP                     1000
#define IDC_EDIT_PORT                   1001
#define IDC_EDIT_USER                   1002
#define IDC_EDIT_PASSWORD               1003
#define IDC_BUTTON_CONNECT              1004
#define IDC_BUTTON_DISCONNECT           1005
#define IDC_LIST1                       1006
#define IDC_LIST_LOG                    1006
#define IDC_EDIT_TAG_NAME               1007
#define IDC_EDIT_TIME                   1008
#define IDC_EDIT_VALUE                  1009
#define IDC_EDIT_QUALITY                1010
#define IDC_BUTTON_SNAPSHOT             1011
#define IDC_BUTTON2                     1012
#define IDC_BUTTON_HISTORY              1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
