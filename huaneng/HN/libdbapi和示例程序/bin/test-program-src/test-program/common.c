#include <assert.h>
#include <string.h>
#include <time.h>
#include "common.h"

int g_caseCount = 0;
int g_caseCountFailure = 0;
int g_caseCountSuccess = 0;

const char* GetErrorMsg(DWORD dwErrorCode)
{
	const char *szMsg = NULL;
	
	switch(dwErrorCode)
	{
	case 0x00: szMsg = _T("");									break;
	case 0x01: szMsg = _T("失败");									break;
		// 数据库级别错误代�?
	case 0x02: szMsg = _T("链接失败");								break;
	case 0x03: szMsg = _T("无此接口");								break;
	case 0x04: szMsg = _T("驱动句柄错误");							break;
	case 0x05: szMsg = _T("错误的记�?");							break;
	case 0x06: szMsg = _T("驱动发生的内存错�?");					break;
	case 0x07: szMsg = _T("不支持，比如把SOE写入实时�?");			break;
	case 0x08: szMsg = _T("没有记录");								break;
	case 0x09: szMsg = _T("无此标签");								break;
	case 0x0A: szMsg = _T("数据类型错误或无法转�?");				break;
	case 0x0B: szMsg = _T("时标错误");								break;
	case 0x0C: szMsg = _T("没有数据");								break;
	case 0x1E: szMsg = _T("无此驱动");								break;
		// 代理服务器级别错误代�?
	case 0xA1: szMsg = _T("标签不存在，指代理服务器中的标签");		break;
	case 0xA2: szMsg = _T("标签ID不存�?");							break;
	case 0xA3: szMsg = _T("标签名已经存�?");						break;
	case 0xA4: szMsg = _T("标签ID和标签名存在");					break;
	case 0xA5: szMsg = _T("数据类型错误");							break;
	case 0xA6: szMsg = _T("不能修改数据类型");						break;
	case 0xA7: szMsg = _T("代理服务器发生的内存错误");				break;
	case 0xA8: szMsg = _T("无此数据�?");							break;
	case 0xA9: szMsg = _T("数据库忙");								break;
	case 0xAA: szMsg = _T("数据库管理中");							break;
	case 0xAB: szMsg = _T("数据库连接ID错误");						break;
	case 0xAC: szMsg = _T("超时");									break;
	case 0xAD: szMsg = _T("无数�?");								break;
		// API级别错误代码
	case 0xC0: szMsg = _T("错误句柄");								break;
	case 0xC1: szMsg = _T("没有连接");								break;
	case 0xC2: szMsg = _T("API发生的内存错�?");						break;
		// 安全级别错误代码
	case 0x23: szMsg = _T("无此角色");								break;
	case 0x24: szMsg = _T("无此账号");								break;
	case 0x25: szMsg = _T("无此IP");								break;
	case 0x29: szMsg = _T("无此权限");								break;
	case 0x2A: szMsg = _T("密码错误");								break;
	case 0x2B: szMsg = _T("账号禁用");								break;
	default: szMsg = _T("未知错误");								break;
	}
	
	return szMsg;
}

void CaseCountSuccessSelfIncrease()
{
	g_caseCountSuccess++;
}

void CaseCountFailureSelfIncrease()
{
	g_caseCountFailure++;
}

void CaseCountAllSelfIncrease()
{
	g_caseCount++;
}

void TestSummaryOutput()
{
	double caseCountSuccess = (double)g_caseCountSuccess;
	printf("test case :%d,\tsuccess :%d,\tfailed :%d,\t success rate :%.2lf%%\n\n",
		g_caseCount, g_caseCountSuccess, g_caseCountFailure,
		caseCountSuccess / g_caseCount * 100
		);
}

void SecondTime2Str(OUT     char *outStr, IN const int strLen, IN const time_t second)
{
	struct tm stm = {0};
	//printf("time :%d", (int)second);
	localtime_r(&second, &stm);
	strftime(outStr, strLen, "%Y-%m-%d %X", &stm);
}

static void Tvval2Str(const LPVVAL tv, char *szTagType, char *szTagValue,char *szTime)
{
	assert(szTagType != NULL);
	assert(szTagValue != NULL);
	assert(szTime != NULL);
	switch(tv->vt)
	{
	case DT_VOID:
		strcpy(szTagType, "DT_VOID");
		strcpy(szTagValue, "void");
		break;
	case DT_DIGITAL:
		strcpy(szTagType, "DIGITAL");
		sprintf(szTagValue,"%d", tv->lval);
		break;
	case DT_INT32:
		strcpy(szTagType, "INT32");
		sprintf(szTagValue,"%d", tv->lval);	
		break;
	case DT_FLOAT32:
		strcpy(szTagType, "FLOAT32");
		sprintf(szTagValue,"%f", tv->fval);	
		break;
	case DT_INT64:
		strcpy(szTagType, "INT64");
		sprintf(szTagValue,"%lld", tv->llval);		
		break;
	case DT_FLOAT64:
		strcpy(szTagType, "DT_FLOAT64");
		sprintf(szTagValue,"%f", tv->dblval);
		break;
	case DT_STRING:
		strcpy(szTagType, "STRING");
		if (tv->pstr)
			strcpy(szTagValue, tv->pstr);
		else
			strcpy(szTagValue, "NULL");
		break;
	case DT_BLOB:
		strcpy(szTagType, "BLOB");
		break;
	}

	/*time_t itimes = tv->ltime;
	struct tm stm;
	localtime_r((time_t*)&(itimes), &stm);
 	strftime(szTime, TMPLEN, "%Y-%m-%d %X", &stm)   */                   
	SecondTime2Str(szTime, TMPLEN, tv->ltime);
}


void OutputTvval(LPVVAL tv)
{
	char szTime[TMPLEN] = "";
	char szTye[TMPLEN] = "";
	char szTagValue[TMPLEN*5] = "";
	Tvval2Str(tv, szTye, szTagValue, szTime);
	printf("time:%s, quality:%d, type:%s, value:%s, err:%d\n", 
		szTime, tv->snqa, szTye, szTagValue, tv->snerr);
}

void OutputTvvals(LPVVAL tv, int n)
{
	int i = 0;
	for (; i<n; i++)
	{
		OutputTvval(&tv[i]);
	}
	//putchar('\n');
}
