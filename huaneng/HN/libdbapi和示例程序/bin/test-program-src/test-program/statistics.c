#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "history.h"
#include "common.h"

void StatisticsTest(const DBPH hDdp)
{
	const char *tagName = "FLA0235997";	
	TVVAL pValues[3600] = {0};
	int recvSize = 0;
	int i = 0;
	INT4 interval = 0;
	INT4 endTime = (INT4)::time(NULL);
	INT4 startTime = endTime - 3600*24;
	DWORD dwret  = 0;
	int nsizeOut = 0;
	int nsize = 0;
	TIMING_INIT;

	assert(hDdp != NULL);
	
	for (i=0; i<7; i++ ) //7 statistics type
	{
		for (interval=0; interval<20000; interval+=10000)
		{
			CaseCountAllSelfIncrease();
		    nsize = (0 == interval) ? 1 : (endTime - startTime) / interval;
			
			TIMING_BEGINS;
			dwret = DBP_GetSumVal(hDdp, tagName, startTime, endTime,
				i, interval, pValues, nsize, &nsizeOut);
			TIMING_END;
			printf("DBP_GetSumVal type:%d, interval:%d, nsize(input size):%d, sizeOut:%d\n",
				i, interval, nsize, nsizeOut);
			if (dwret != 0)
			{
				CaseCountFailureSelfIncrease();
				printf("DBP_GetSumVal: failed, dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
					);
			}
			else
			{
				CaseCountSuccessSelfIncrease();
				printf("DBP_GetSumVal read %d value(s)\n", nsizeOut);
				OutputTvvals(pValues, nsizeOut);	
			}
		}
		
	}
	TestSummaryOutput();
}
