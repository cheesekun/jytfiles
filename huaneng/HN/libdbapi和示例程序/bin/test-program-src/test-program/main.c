#include <stdlib.h>
#include <time.h>
#include "main.h"
#include "common.h"
#include "snapshot.h"
#include "history.h"
#include "statistics.h"

BOOL g_queryTagsNum = 0;

void __stdcall EnumTag(LPTAGITEM ptag, int ntags, void* lparam)
{
	
	for(int i = 0; i < ntags; i++)
	{
		printf("tagName:%s\tid:%u\tm_sdbname:%s\tm_wtype:%u\terrCode:%d\n", 
			ptag[i].m_sname, ptag[i].m_dwid, ptag[i].m_sdbname, 
			ptag[i].m_wtype, ptag[i].m_errcode);
	}
	g_queryTagsNum += ntags;
	//g_enumTagFinish = TRUE;
}


int main(int argc, char *argv[])
{
	int i = 0;
	MODULE_START_PRINT(mainfuction);	
	puts("testCase 1:call DBP_Open2...");
	CaseCountAllSelfIncrease();
	DBPH hDdp = DBP_Open2("10.220.96.139", 12084, "admin", "admin", FALSE);
	if (hDdp == NULL)
	{
		puts("DBP_Open2 failed");
		CaseCountFailureSelfIncrease();
		TestSummaryOutput();
		return -1;
	}
	CaseCountSuccessSelfIncrease();

	puts("testCase 2:call DBP_Connect...");
	CaseCountAllSelfIncrease();
	DWORD dwret = DBP_Connect(hDdp);
	if (dwret)
	{
		perror("DBP_Connect failed.\t");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
		TestSummaryOutput();
		return -1;
	}
	CaseCountSuccessSelfIncrease();

	CaseCountAllSelfIncrease();
	DWORD ver = DBP_GetVersion();
	if (ver <= 0)
	{
		printf("ver=%u\n", ver);
		CaseCountFailureSelfIncrease();
	}
	else
	{
		TRACE(_T("DBP_GetVersion: V%d.%d.%d.%d\n"), ((BYTE*)&ver)[3], ((BYTE*)&ver)[2], ((BYTE*)&ver)[1], ((BYTE*)&ver)[0]);
		CaseCountSuccessSelfIncrease();
	}
	
	CaseCountAllSelfIncrease(); 
	dwret = DBP_IsConnect(hDdp); 
	CaseCountSuccessSelfIncrease();
	if (dwret == 0)
	{
		puts("DBP_IsConnect:Connected !");
	}
	else 
	{
		perror("DBP_IsConnect,UnConnected!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
	}
	TestSummaryOutput();

	TIMING_INIT;
	
	{//Module QueryTag
		g_queryTagsNum = 0;	
		MODULE_START_PRINT(QueryTag);
		CaseCountAllSelfIncrease();
		TIMING_BEGINS;
		//dwret = DBP_QueryTag(hDdp, NULL, NULL, -1, EnumTag,	NULL); 
		dwret = DBP_QueryTag(hDdp, "FLA023599*", NULL, -1, EnumTag,	NULL); 
		TIMING_END;
		if (dwret == 0)
		{
			puts("DBP_QueryTag,Sync FLA023599* Tag success!");
			printf("Sync %d tags.\n", g_queryTagsNum);
			CaseCountSuccessSelfIncrease();
		}
		else 
		{
			perror("DBP_QueryTag,Sync all Tag failed!");
			printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
			CaseCountFailureSelfIncrease();
		}

		CaseCountAllSelfIncrease();  //Testcase 6
		g_queryTagsNum = 0;
		TIMING_BEGINS;
		dwret = DBP_QueryTag2(hDdp, "N5DCS.D7*", NULL, -1, -1, EnumTag,	NULL); 
		TIMING_END;
		if (dwret == 0)
		{
			puts("DBP_QueryTag2,Query N5DCS.D7* Tag success!");
			printf("Query %d tags.\n", g_queryTagsNum);
			CaseCountSuccessSelfIncrease();
		}
		else 
		{
			perror("DBP_QueryTag2,Query N5DCS.D* Tag failed!");
			printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
			CaseCountFailureSelfIncrease();
		}
		TestSummaryOutput();
	}

	MODULE_START_PRINT(SnapShot);
	SnapShotTest(hDdp);
	
	MODULE_START_PRINT(History);
	HistoryTest(hDdp);

	MODULE_START_PRINT(statistics);
	StatisticsTest(hDdp);

	{//Module MultPointHis
		char tagNames[2][80] = {"FLA0235994", "FLA0235995"}; 
		INT4 i4time = (INT4)::time(NULL) - 600;
		TVVAL tvs[2] = {0};
		MODULE_START_PRINT(MultPointHis);	
		for (i=0; i<3; i++)
		{
			CaseCountAllSelfIncrease();
			TIMING_BEGINS;
			dwret = DBP_GetMultiPointHisVal(hDdp, (INT4)i+1,
			tagNames, &i4time, tvs, 2);
			TIMING_END;
			if (dwret)
			{
				perror("DBP_GetMultiPointHisVal failed.\t");
				printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
				CaseCountFailureSelfIncrease();
			}
			else
			{
				printf("DBP_GetMultiPointHisVal type:%d(type 1,2,3 means before, after, insert) success\n", i+1);
				CaseCountSuccessSelfIncrease();
				OutputTvvals(tvs, 2);
			}
		}
		TestSummaryOutput();
	}


	{//Module ServerInfo
		TDBPARG dbpargs[TMPLEN] = {0};
		int conNum = 0;
		int i = 0;
		int srvOrder = 0;
		SERVERINFO srvInfo = {0};
		MODULE_START_PRINT(ServerInfo);
		CaseCountAllSelfIncrease();
		TIMING_BEGINS;
		dwret = DBP_GetSvrConInfo(hDdp, dbpargs, &conNum);
		TIMING_END;
		if (dwret)
		{
			perror("DBP_GetSvrConInfo failed.\t");
			printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
			CaseCountFailureSelfIncrease();
		}
		else
		{
			printf("DBP_GetSvrConInfo success\n");
			CaseCountSuccessSelfIncrease();
			for ( ; i<conNum; i++)
			{
				printf("Connection %d:ip:%s\tuser:%s\tpass:%s\tport:%d\tnres:%d\tsres:%s\n",
					i, dbpargs[i].sip, dbpargs[i].suser, dbpargs[i].spass,
					dbpargs[i].wport, dbpargs[i].snres, dbpargs[i].sres);
			}
		}

		CaseCountAllSelfIncrease();
		TIMING_BEGINS;
		dwret = DBP_SendIdel(hDdp, &srvOrder);
		TIMING_END;
		if (dwret)
		{
			perror("DBP_SendIdel failed.\t");
			printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
			CaseCountFailureSelfIncrease();
		}
		else
		{
			printf("DBP_SendIdel success,srvOrder:%d\n", srvOrder);
			CaseCountSuccessSelfIncrease();
		}
		
		CaseCountAllSelfIncrease();
		TIMING_BEGINS;
		dwret = DBP_GetServerInfo(hDdp, &srvInfo);
		TIMING_END;
		if (dwret)
		{
			perror("DBP_GetServerInfo failed.\t");
			printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
			CaseCountFailureSelfIncrease();
		}
		else
		{
			char nowTime[TMPLEN] = "";
			char startTime[TMPLEN] = "";
			printf("DBP_GetServerInfo success.\n");
			CaseCountSuccessSelfIncrease();
			
			SecondTime2Str(nowTime, TMPLEN, srvInfo.lNowTime);
			SecondTime2Str(startTime, TMPLEN, srvInfo.lStartupTime);
			printf("ServerInfo: dwVersion:0x%X\tnowTime:%s\tstartTime:%s\t"
				   "ConnectCount:%u\tTagCount:%u\tCPUUsage:%%%f\t"
				   "HandleCount:%u\tThreadsCount:%u\tMemoryBytes:%u\t"
				   "MemoryBytesPeak:%u\tVirtualBytes:%u\tVirtualBytesPeak:%u\n", 
				   srvInfo.dwVersion, nowTime, startTime,
				   srvInfo.dwConnectCount, srvInfo.dwTagCount, srvInfo.fCPUUsage*100,
				   srvInfo.dwHandleCount, srvInfo.dwThreadsCount, srvInfo.dwMemoryBytes,
				   srvInfo.dwMemoryBytesPeak, srvInfo.dwVirtualBytes, srvInfo.dwVirtualBytesPeak
				   );
		}
		TestSummaryOutput();
	}
	
	MODULE_START_PRINT(mainfuction);	
	CaseCountAllSelfIncrease();
	dwret = DBP_DisConnect(hDdp);
	if (dwret)
	{
		perror("DBP_DisConnect failed.\t");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}
	else
	{
		puts("DBP_DisConnect success");
		CaseCountSuccessSelfIncrease();
	}

	CaseCountAllSelfIncrease();  
	dwret = DBP_IsConnect(hDdp); 
	CaseCountSuccessSelfIncrease();
	if (dwret == 0)
	{
		puts("DBP_IsConnect:Connected !");
	}
	else 
	{
		perror("DBP_IsConnect,UnConnected!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
	}

	CaseCountAllSelfIncrease(); 
	dwret = DBP_Close(hDdp);
	if (dwret)
	{
		perror("DBP_Close failed.\t");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}
	else
	{
		puts("DBP_Close success");
		CaseCountSuccessSelfIncrease();
	}
	TestSummaryOutput();
	
	return 0;
}
