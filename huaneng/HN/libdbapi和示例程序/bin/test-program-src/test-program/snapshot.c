#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "snapshot.h"
#include "common.h"

void SnapShotTest(const DBPH hDdp)
{
	assert(hDdp != NULL);
	CaseCountAllSelfIncrease();
	char sTagNames[3][80] = {"FLA0235999", "FLA0235998", "FLA0235997"};
	TVVAL tvs[5] = {0};
		
	TIMING_INIT;
	TIMING_BEGINS;
	DWORD dwret = DBP_GetSnapshot(hDdp, sTagNames, tvs, 3); 
	TIMING_END;
	if (dwret == 0)
	{
		puts("DBP_GetSnapshot, success!");
		for (int i=0; i<3; i++)
		{
			printf("TagName:%s:", sTagNames[i]);
			OutputTvval(&tvs[i]);
		}
		CaseCountSuccessSelfIncrease();
	}
	else 
	{
		perror("DBP_GetSnapshot, failed!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}

	CaseCountAllSelfIncrease();
	tvs[0].ltime = tvs[1].ltime =tvs[2].ltime = (INT4)time(NULL); 
	tvs[0].lval = 32;
	tvs[1].llval = 64;
	tvs[2].dblval =64.64;
	
	TIMING_BEGINS;
	dwret = DBP_WriteReal(hDdp, sTagNames,  tvs, 3);
	TIMING_END;
	if (dwret == 0)
	{
		puts("DBP_WriteReal, success!");
		CaseCountSuccessSelfIncrease();
	}
	else 
	{
		perror("DBP_WriteReal, failed!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}

	const char *tagString = "FLA0235996";
	CaseCountAllSelfIncrease();
	tvs[0].ltime = (INT4)time(NULL); 
	tvs[0].pstr = (char*)malloc(128);
	if (tvs[0].pstr == NULL)
	{
		perror("DBP_WriteRealStr malloc error!");
		return;
	}
	tvs[0].vt = DT_STRING;
	tvs[0].lvlen = strlen("teststring")+1;
	strncpy_s(tvs[0].pstr, 128, "teststring", strlen("teststring")+1);
	TIMING_BEGINS;
	dwret = DBP_WriteRealStr(hDdp, tagString, tvs);
	TIMING_END;
	free(tvs[0].pstr);
	if (dwret == 0)
	{
		puts("DBP_WriteRealStr, success!");
		CaseCountSuccessSelfIncrease();
	}
	else 
	{
		perror("DBP_WriteRealStr, failed!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}

	CaseCountAllSelfIncrease();
	TVVAL* pval = NULL;
	TIMING_BEGINS;
	dwret = DBP_GetRealStr(hDdp, tagString, &pval);
	TIMING_END;
	if (dwret == 0)
	{
		puts("DBP_GetRealStr, success!");
		CaseCountSuccessSelfIncrease();
		OutputTvval(pval);
	}
	else 
	{
		perror("DBP_GetRealStr, failed!");
		printf("dwret:%d, errMsg:%s\n", (int)dwret, GetErrorMsg(dwret));
		CaseCountFailureSelfIncrease();
	}
	TestSummaryOutput();
	
}

