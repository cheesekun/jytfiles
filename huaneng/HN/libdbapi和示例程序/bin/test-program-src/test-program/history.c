#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "history.h"
#include "common.h"


void HistoryTest(const DBPH hDdp)
{
	assert(hDdp != NULL);
	INT4 endTime = (INT4)::time(NULL);
	INT4 startTime = endTime - 3600;
	TVVAL pValues[3600] = {0};
	int recvSize = 0;
	int i = 0;

	//1 hour sample
	CaseCountAllSelfIncrease();
	puts("DBP_GetHisVal type double,  1  hour");
	TIMING_INIT;
	TIMING_BEGINS;
	DWORD dwret = DBP_GetHisVal(hDdp, "FLA0235997", startTime, endTime,
		0, 0, pValues, 3600, &recvSize); //read sample value
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_GetHisVal:read 1 hour sample value failed,value type float64."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_GetHisVal read 1hour sample get %d values\n", recvSize);
		OutputTvvals(pValues, recvSize);
		
	}

	//100 hours sample
	startTime = endTime - 3600*100;
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_GetHisVal(hDdp, "FLA0235997", startTime, endTime,
		0, 0, pValues, 3600, &recvSize); //read sample value
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_GetHisVal:read 100 hours sample value failed,value type float64."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_GetHisVal read 100 hours sample get %d values\n", recvSize);
		OutputTvvals(pValues, recvSize);	
	}

	//100 hours Interpolation
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_GetHisVal(hDdp, "FLA0235997", startTime, endTime,
		105, 1, pValues, 3600, &recvSize); 
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_GetHisVal:read 100 hours Interpolation failed,value type float64."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_GetHisVal read 100 hours Interpolation get %d values\n", recvSize);
		OutputTvvals(pValues, recvSize);	
	}

	//1 hour trend
	startTime = endTime - 3600;
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_GetHisVal(hDdp, "FLA0235997", startTime, endTime,
		0, 2, pValues, 3600, &recvSize); 
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_GetHisVal:read 1 hour trend failed,value type float64."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_GetHisVal read 1 hour trend get %d values\n", recvSize);
		OutputTvvals(pValues, recvSize);	
	}

	//DBP_WriteHis write 1 hour trend of "FLA0235997" to "FLA0235994"
	for (i=0; i<recvSize; i++)
	{
		pValues[i].vt = DT_FLOAT32;
		pValues[i].fval = (float)pValues[i].dblval;
	}

	char (*sTagNames)[80]  =  (TagNameArray) malloc(80* recvSize);
	for (i=0; i<recvSize; i++)
	{
		strcpy(sTagNames[i], "FLA0235994");
	}
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_WriteHis(hDdp, sTagNames, pValues, recvSize);
	TIMING_END;
	free(sTagNames);
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_WriteHis:write 1 hour trend of FLA0235997 to FLA0235994 failed,value type float32."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_WriteHis  write %d values to tag FLA0235994 success\n", recvSize);
		OutputTvvals(pValues, recvSize);	
	}


	//DBP_DelHis del 1 hour history 
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_DelHis(hDdp, "FLA0235994", startTime, endTime);
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_DelHis:del FLA0235994 1 hour history failed,value type float32."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_DelHis :del FLA0235994 1 hour history success\n");
	}

	TVVAL* pval = NULL;
	const char *stringTag = "FLA0235996";
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_GetHisStr(hDdp, stringTag, startTime, RST_AFTER, &pval);
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_GetHisStr: FLA0235996 1 hour history failed."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_GetHisStr:1 hour str success\n");
		OutputTvval(pval);
	}


	const char *writeStr = "writeHistoryStr";
	memset(&pValues[0], 0, sizeof(TVVAL));
	pValues[0].ltime = time(NULL) - 7200;
	pValues[0].vt = DT_STRING;
	pValues[0].lvlen = strlen(writeStr);
	pValues[0].pstr = (char*)malloc(pValues[0].lvlen+1);
	if (pValues[0].pstr == NULL)
	{
		perror("DBP_WriteHisStr malloc error!");
		return;
	}
	strcpy(pValues[0].pstr, writeStr);
	pValues[0].pstr[strlen(writeStr)] = 0;
	
	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_WriteHisStr(hDdp, stringTag, pValues);
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_WriteHisStr: FLA0235996 before 2 hours failed."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_WriteHisStr:before 2 hours success\n");
		//OutputTvval(pval);
	}

	char tagNames[3][80] = {"FLA0235997", "FLA0235994", "FLA0235995"};
	INT4 times[3] = {0};
	short snerrors[3] = {0};
	int size = 3;
	struct tm stm = {0};
	strptime("20200521141959", "%Y%m%d%H%M%S", &stm); 
	times[0] = (INT4)mktime(&stm); 
	strptime("20200521174223", "%Y%m%d%H%M%S", &stm); 
	times[1] = (INT4)mktime(&stm); 
	strptime("20200520171219", "%Y%m%d%H%M%S", &stm); 
	times[2] = (INT4)mktime(&stm);

	CaseCountAllSelfIncrease();
	TIMING_BEGINS;
	dwret = DBP_DelHis2(hDdp, tagNames, times, snerrors, size);
	TIMING_END;
	if (dwret != 0)
	{
		CaseCountFailureSelfIncrease();
		printf("DBP_DelHis2:del 3 records failed."
			"dwret :%d,errMsg:%s\n", dwret, GetErrorMsg(dwret)
			);
	}
	else
	{
		CaseCountSuccessSelfIncrease();
		printf("DBP_DelHis2:del 3 records success\n");
		//OutputTvval(pval);
	}
	TestSummaryOutput();
}


