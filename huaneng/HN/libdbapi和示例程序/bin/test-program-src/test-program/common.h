#ifndef __COMMON_H__
#define __COMMON_H__
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include "dbapi.h"

#ifndef TMPLEN
#define TMPLEN 128
#endif


#define TIMING_INIT						\
struct  timeval  tvBegin;				\
struct  timeval  tvEnd;

#define TIMING_BEGINS 					                 \
do                                                       \
{                                                        \
	gettimeofday(&tvBegin, NULL);                        \
} while(0)                                               
	
#define TIMING_END                                       \
do                                                       \
{                                                        \
														 \
	gettimeofday(&tvEnd, NULL);                          \
	int diffMs = (tvEnd.tv_sec-tvBegin.tv_sec)*1000      \
		+ (tvEnd.tv_usec-tvBegin.tv_usec)/1000;          \
	printf("Timing use %d Ms.\n", diffMs);                \
} while(0)

void CaseCountSuccessSelfIncrease();
void CaseCountFailureSelfIncrease();
void CaseCountAllSelfIncrease();
void TestSummaryOutput();

const char* GetErrorMsg(DWORD dwErrorCode);

void SecondTime2Str(OUT     char *outStr, IN const int strLen, IN const time_t second);
void OutputTvval(LPVVAL tv);
void OutputTvvals(LPVVAL tv, int n);
#define MODULE_START_PRINT(m) printf("\n---Module %s test start---\n", #m)


#endif
