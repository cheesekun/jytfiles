#!/bin/sh
LIBDIR=/usr/lib64/irpt
if [ ! -d "${LIBDIR}" ];then
  mkdir -p $LIBDIR
fi
cp -rf ddpdrv $LIBDIR
cp -f lib*.so* $LIBDIR
#ln -sf /usr/lib64/irpt/libstdc++.so.6.0.21 /usr/lib64/irpt/libstdc++.so.6
#ln -sf /usr/lib64/irpt/
ldconfig
